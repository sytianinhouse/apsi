<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('address')->nullable();
            $table->string('cp_first_name')->nullable();
            $table->string('cp_last_name')->nullable();
            $table->string('cp_number')->nullable();
            $table->string('cp_number_2')->nullable();
            $table->text('additional_contacts')->nullable();
            $table->string('type')->nullable();
            $table->boolean('is_master')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
