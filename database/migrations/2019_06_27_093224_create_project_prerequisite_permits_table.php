<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectPrerequisitePermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_prerequisite_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_permits_id');
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->text('notes')->nullable();
            $table->timestamp('date_filed')->nullable();
            $table->timestamp('date_acquired')->nullable();
            $table->timestamp('date_expiry')->nullable();
            $table->timestamp('date_due')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_prerequisite_permits');
    }
}
