<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'Permit',
            'email' => 'admin@email.com',
            'username' => 'admin',
            'password' => Hash::make('password123'),
            'active' => 1,
        ]);
    }
}
