<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@manageRedirect');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth.admin']], function() {

    Route::get('', 'Dashboard\DashboardController@dashboard');
    Route::get('dashboard', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::get('settings', 'Setting\SettingsController@index')->name('settings');
    Route::post('settings', 'Setting\SettingsController@store')->name('store_settings');
    Route::post('settings/{siteSetting}', 'Setting\SettingsController@update')->name('update_settings');

    Route::group(['prefix' => 'clients', 'as' => 'clients.', 'namespace' => 'Client'], function() {

        Route::get('/', 'ClientsController@index')->name('index');

        Route::get('create', 'ClientsController@create')->name('create');
        Route::post('create', 'ClientsController@store')->name('store');

        Route::get('edit/{client}', 'ClientsController@edit')->name('edit');
        Route::post('edit/{client}', 'ClientsController@update')->name('update');

        Route::get('delete/{client}', 'ClientsController@destroy')->name('destroy');

        Route::post('client/ajax', 'ClientsController@clientAjax')->name('client-ajax');

    });

    Route::group(['prefix' => 'cities', 'as' => 'cities.', 'namespace' => 'City'], function() {

        Route::get('/', 'CitiesController@index')->name('index');

        Route::get('create', 'CitiesController@create')->name('create');
        Route::post('create', 'CitiesController@store')->name('store');

        Route::get('edit/{city}', 'CitiesController@edit')->name('edit');
        Route::post('edit/{city}', 'CitiesController@update')->name('update');

        Route::get('delete/{city}', 'CitiesController@destroy')->name('destroy');

    });

    Route::group(['prefix' => 'permits', 'as' => 'permits.', 'namespace' => 'Permit'], function() {

        Route::get('/', 'PermitsController@index')->name('index');

        Route::get('create', 'PermitsController@create')->name('create');
        Route::post('create', 'PermitsController@store')->name('store');

        Route::get('edit/{permit}', 'PermitsController@edit')->name('edit');
        Route::post('edit/{permit}', 'PermitsController@update')->name('update');

        Route::get('delete/{permit}', 'PermitsController@destroy')->name('destroy');

    });

    Route::group(['prefix' => 'permit-categories', 'as' => 'permit_categories.', 'namespace' => 'Permit'], function() {

        Route::get('/', 'PermitCategoriesController@index')->name('index');

        Route::post('quick-create', 'PermitCategoriesController@quickCreate')->name('quick-create');

        Route::get('create', 'PermitCategoriesController@create')->name('create');
        Route::post('create', 'PermitCategoriesController@store')->name('store');

        Route::get('edit/{permitCategory}', 'PermitCategoriesController@edit')->name('edit');
        Route::post('edit/{permitCategory}', 'PermitCategoriesController@update')->name('update');

        Route::get('delete/{permitCategory}', 'PermitCategoriesController@destroy')->name('destroy');

    });

    Route::group(['prefix' => 'users', 'as' => 'users.', 'namespace' => 'User'], function() {

        Route::get('/', 'UsersController@index')->name('index');

        Route::get('create', 'UsersController@create')->name('create');
        Route::post('create', 'UsersController@store')->name('store');

        Route::get('edit/{user}', 'UsersController@edit')->name('edit');
        Route::post('edit/{user}', 'UsersController@update')->name('update');

        Route::get('delete/{user}', 'UsersController@destroy')->name('destroy');

    });

    Route::group(['prefix' => 'projects', 'as' => 'projects.', 'namespace' => 'Project'], function() {

        Route::get('/', 'ProjectsController@index')->name('index');

        Route::get('client-projects/{client}', 'ProjectsController@viewByClient')->name('byClient');

        Route::get('{client}/create', 'ProjectsController@createByClient')->name('createByClient');
        Route::post('{client}/create', 'ProjectsController@storeByClient')->name('createByClient');

        Route::get('create', 'ProjectsController@create')->name('create');
        Route::post('create', 'ProjectsController@store')->name('store');

        Route::get('edit/{project}', 'ProjectsController@edit')->name('edit');
        Route::post('edit/{project}', 'ProjectsController@update')->name('update');

        Route::get('delete/{project}', 'ProjectsController@destroy')->name('destroy');

        Route::post('ajax', 'ProjectsController@permitAjax')->name('permit-ajax');

        Route::post('send-notif', 'ProjectsController@ajaxSendNotification')->name('sendNotif');

        Route::post('delete/file', 'ProjectsController@ajaxDeleteFile')->name('delete-file');

    });

    Route::group(['prefix' => 'reports', 'as' => 'reports.', 'namespace' => 'Report'], function() {

        Route::get('projects-created-per-permit-category', 'ReportsController@projectsPerCategory')->name('projPerCategory');

        Route::get('projects-created', 'ReportsController@viewProjectsCreated')->name('projCreated');

        Route::get('projects-per-city', 'ReportsController@projectsPerCity')->name('projPerCity');

        Route::get('projects-per-status', 'ReportsController@projectsPerStatus')->name('projPerStatus');

    });

});

Route::group(['prefix' => 'client', 'as' => 'client.', 'namespace' => 'Client', 'middleware' => ['auth.client']], function() {

    Route::get('', 'Project\ProjectsController@dashboard')->name('dashboard');

    Route::get('profile', 'User\ProfileController@edit')->name('profile');
    Route::post('profile/{client}', 'User\ProfileController@update')->name('update');

    Route::group(['prefix' => 'projects', 'as' => 'projects.', 'namespace' => 'Project'], function() {

        Route::get('/', 'ProjectsController@index')->name('index');

        Route:: get('view/{project}', 'ProjectsController@viewProject')->name('view');
    });

});

Route::group(['prefix' => 'master-client', 'as' => 'master_client.', 'namespace' => 'MasterClient', 'middleware' => ['auth.master_client']], function() {

    Route::get('', 'Dashboard\DashboardController@dashRedirect')->name('dash-redirect');

    Route::get('dashboard', 'Dashboard\DashboardController@dashboard')->name('dashboard');

    Route::get('profile', 'User\ProfileController@edit')->name('profile');
    Route::post('profile/{client}', 'User\ProfileController@update')->name('update');

    Route::get('clients', 'Dashboard\DashboardController@index')->name('clients');

});



