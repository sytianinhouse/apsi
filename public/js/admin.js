/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/admin.js":
/*!**************************************!*\
  !*** ./resources/assets/js/admin.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  initSwitchery();
  stickySidebar();
  selectPermitAjax();
  getClientProjects();
  sendPermitEmail();
  deletePermit();
  dateRangePicker();
  customFileUpload();
  $('#send-sms').on('click', function (e) {
    sendSMS();
  });
  $('#master_date_range').on('change', function (e) {
    if ($(this).val() == "") {
      $('#dateRange').removeAttr('required');
      $('#master-dr-wrap').addClass('d-none');
      $('#master-main-dr').removeClass('col-md-5');
      $('#master-main-dr').addClass('col-md-2');
      $('#master-dr-left').removeClass('show');
    } else {
      $('#dateRange').attr('required', '');
      $('#master-dr-wrap').removeClass('d-none');
      $('#master-main-dr').removeClass('col-md-2');
      $('#master-main-dr').addClass('col-md-5');
      $('#master-dr-left').addClass('show');
    }
  });
  $('#display-permits').on('click', '.btn-add-file', function (e) {
    e.preventDefault();
    $('#permit-hidden-file-' + $(this).data('id')).click(); //$('#permit-file-'+$(this).data('id')).hide();
  });
  $('#display-permits').on('click', '.btn-add-prefile', function (e) {
    e.preventDefault();
    $('#pre-hidden-file-' + $(this).data('id')).click();
  });
  $('#display-permits').on('click', '.btn-permit-notes', function (e) {
    e.preventDefault();

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('#pnote-' + $(this).data('id')).addClass('d-none');
      $('#pnote-' + $(this).data('id')).removeClass('d-block');
      $(this).html('<i class="fa fa-plus"></i> ADD NOTES');
      $('#chcknote-' + $(this).data('id')).val(0);
    } else {
      $(this).addClass('active');
      $('#pnote-' + $(this).data('id')).addClass('d-block');
      $('#pnote-' + $(this).data('id')).removeClass('d-none');
      $(this).html('<i class="fa fa-minus"></i> REMOVE NOTES');
      $('#chcknote-' + $(this).data('id')).val(1);
    }
  });
  $('#display-permits').on('click', '.btn-prereq-notes', function (e) {
    e.preventDefault();

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('#prenote-' + $(this).data('id')).addClass('d-none');
      $('#prenote-' + $(this).data('id')).removeClass('d-block');
      $(this).html('<i class="fa fa-plus"></i> ADD NOTES');
      $('#prechcknote-' + $(this).data('id')).val(0);
    } else {
      $(this).addClass('active');
      $('#prenote-' + $(this).data('id')).addClass('d-block');
      $('#prenote-' + $(this).data('id')).removeClass('d-none');
      $(this).html('<i class="fa fa-minus"></i> REMOVE NOTES');
      $('#prechcknote-' + $(this).data('id')).val(1);
    }

    console.log($(this).data('id'));
  }); //REMOVE ERROR NOTIF WHEN CHANGE | START

  $('#select-permit').on('change', function () {
    $('.proj-perm').hide();
    $('ul.chosen-choices').removeClass('error');
  }); //REMOVE ERROR NOTIF WHEN CHANGE | END
  //REMOVE PROJECT PERMIT PRE-REQUISITE | START

  $('#display-permits').on('click', '.x-child', function (e) {
    e.preventDefault();
    deletePreReq($(this));
  }); //REMOVE PROJECT PERMIT PRE-REQUISITE | END
  //ADD PRE-REQUISITE PERMIT | START

  $('#display-permits').on('click', '.add-prereq-btn .btn', function (e) {
    e.preventDefault();
    var permit_count = $('#display-permits .permit-content-wrapper').length - 1,
        current_count = $(this).data('count'),
        pre_permit_count = $('#pre-permit-' + current_count + ' .prereq-wrap').length,
        content = '';
    content += '<div class="prereq-wrap">';
    content += '<a href="#" class="close x-child" data-pkey="' + current_count + '" data-prekey="' + pre_permit_count + '"><i class="fa fa-times"></i></a>';
    content += '<div class="upper-fields">';
    content += '<div class="uf-wrap left">';
    content += '<h5>Title</h5>';
    content += '<input type="text" class="form-control" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][name]" value="" />';
    content += '</div>';
    content += '<div class="uf-wrap right">';
    content += '<ul class="btn-wrap">';
    content += '<li id="pre-view-file-' + current_count + pre_permit_count + '" class="middle">';
    content += '<a href="#" class="btn btn-primary btn-vfile" data-toggle="modal" data-target="#prereq-file-modal-' + current_count + pre_permit_count + '"><i class="fa fa-paperclip"></i> FILE ATTACHMENTS</a>';
    content += '</li>';
    content += '<li class="right">';
    content += '<h5>Status</h5>';
    content += '<input type="checkbox" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][status]" class="make-switch-radio" data-on-text="OPEN" data-off-text="CLOSE" data-on-color="primary" data-off-color="danger" checked />';
    content += '</li>';
    content += '</ul>';
    content += '</div>';
    content += '</div>';
    content += '<ul class="date-fields">';
    content += '<li>';
    content += '<h5>Due Date</h5>';
    content += '<input type="text" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][date_due]" class="form-control datepicker" placeholder="mm/dd/yyyy" data-date-format="mm/dd/yyyy" autocomplete="off">';
    content += '</li>';
    content += '<li>';
    content += '<h5>Date Filed</h5>';
    content += '<input type="text" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][date_filed]" class="form-control datepicker" placeholder="mm/dd/yyyy" data-date-format="mm/dd/yyyy" autocomplete="off">';
    content += '</li>';
    content += '<li>';
    content += '<h5>Date Acquired</h5>';
    content += '<input type="text" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][date_acquired]" class="form-control datepicker" placeholder="mm/dd/yyyy" data-date-format="mm/dd/yyyy" autocomplete="off">';
    content += '</li>';
    content += '<li>';
    content += '<h5>Date Expiry</h5>';
    content += '<input type="text" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][date_expiry]" class="form-control datepicker" placeholder="mm/dd/yyyy" data-date-format="mm/dd/yyyy" autocomplete="off">';
    content += '</li>';
    content += '</ul>';
    content += '<div id="prereq-file-modal-' + current_count + pre_permit_count + '" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">';
    content += '<div class="modal-dialog">';
    content += '<div class="modal-content">';
    content += '<div class="modal-header">';
    content += '<ul class="mh-wrap">';
    content += '<li class="left">';
    content += '<h4 class="modal-title">File Attachments</h4>';
    content += '</li>';
    content += '<li class="right">';
    content += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    content += '<span aria-hidden="true">×</span>';
    content += '</button>';
    content += '</li>';
    content += '</ul>';
    content += '</div>';
    content += '<div class="modal-body">';
    content += '<div class="permit-file-wrapper" id="permit-file-' + current_count + pre_permit_count + '">';
    content += '<a href="#" class="file-close d-none"><i class="fa fa-times"></i></a>';
    content += '<input  name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][uploads][]" type="file" multiple class="file-loading mult-permit-file" data-id="prereq-' + current_count + pre_permit_count + '">';
    content += '<div class="existing-file-wrapper d-none" id="existing-prereq-' + current_count + pre_permit_count + '"></div>';
    content += '</div>';
    content += '</div>';
    content += '<div class="modal-footer">';
    content += '<button type="button" class="btn btn-success" data-dismiss="modal">CONFIRM</button>';
    content += '<button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CANCEL</button>';
    content += '</div>';
    content += '</div>';
    content += '</div>';
    content += '</div>';
    content += '<div class="add-notes-wrap padding-top">';
    content += '<div class="notes-btn-wrap">';
    content += '<input type="hidden" id="prechcknote-' + current_count + pre_permit_count + '" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][check_note]" value="0">';
    content += '<a href="#" class="btn-prereq-notes" data-id="' + current_count + pre_permit_count + '"><i class="fa fa-plus"></i> ADD NOTES</a>';
    content += '</div>';
    content += '<div class="notes-wrap d-none" id="prenote-' + current_count + pre_permit_count + '">';
    content += '<h5>Notes</h5>';
    content += '<textarea class="form-control" name="Permit[' + current_count + '][Pre][' + pre_permit_count + '][notes]" cols="50" rows="1"></textarea>';
    content += '</div>';
    content += '</div>';
    content += '</div>';
    $('#pre-permit-' + current_count).append(content);
    initSwitchery();
    initDatePicker();
    customFileUpload();
  }); //ADD PRE-REQUISITE PERMIT | END
  //ADD PRE-REQUISITE | START

  $('#add-req').on('click', function (e) {
    e.preventDefault();
    var ctr = $('.req-main-wrapper .row .input_field_sections').length,
        output = '';
    output += '<div class="col-12 input_field_sections">';
    output += '<h5>Name</h5>';
    output += '<div class="req-wrap">';
    output += '<div class="wrap left">';
    output += '<input type="text" class="form-control" name="pre_permits[' + ctr + '][name]" />';
    output += '</div>';
    output += '<div class="wrap right">';
    output += '<a href="#" class="btn btn-danger form-control" data-id="' + ctr + '">';
    output += '<i class="fa fa-trash"></i>';
    output += '</a>';
    output += '</div>';
    output += '</div>';
    output += '</div>';
    $('.req-main-wrapper .row').append(output);
  }); //ADD PRE-REQUISITE | END
  //REMOVE PRE-REQUISITE | START

  $('.req-main-wrapper').on('click', '.btn-danger', function (e) {
    e.preventDefault();
    var deleteField = '<input type="hidden" class="form-control" name="pre_permits[' + $(this).data('id') + '][delete]" value="1" />';
    $(this).parent().parent().parent().append(deleteField);
    $(this).parent().parent().parent().hide();
  }); //REMOVE PRE-REQUISITE | END
  //ADD ADDITIONAL CONTACTS | START

  $('#ac-button').on('click', function (e) {
    e.preventDefault();
    var ctr = $('#additional-contacts .ac-wrap').length,
        output = '';
    output += '<div class="ac-wrap">';
    output += '<ul>';
    output += '<li class="left">';
    output += '<input type="text" class="form-control" name="additional_contacts[]" value="" placeholder="Contact Number" required/>';
    output += '</li>';
    output += '<li class="right">';
    output += '<a href="#" class="btn btn-danger form-control">';
    output += '<i class="fa fa-trash"></i>';
    output += '</a>';
    output += '</li>';
    output += '</ul>';
    output += '</div>';
    $('#additional-contacts').append(output);
  }); //ADD ADDITIONAL CONTACTS | END
  //REMOVE ADDITIONAL CONTACTS | START

  $('#additional-contacts').on('click', 'a', function (e) {
    e.preventDefault();
    var btnThis = $(this);
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00c0ef',
      cancelButtonColor: '#ff8080',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      btnThis.parent().parent().parent().remove();
      swal({
        title: 'Deleted!',
        text: 'Contact number has been deleted!',
        type: 'success',
        confirmButtonColor: '#00c0ef'
      }).done();
    });
  }); //REMOVE ADDITIONAL CONTACTS | END

  $('#master').on('click', function () {
    if ($(this).prop("checked") == true) {
      $('#all-clients').removeClass('d-none');
      $('#select-clients').attr('required', '');

      if ($('#select-clients').val().length > 0) {
        $('#all-projects').removeClass('d-none');
      }
    } else if ($(this).prop("checked") == false) {
      $('#all-clients').removeClass('d-block');
      $('#all-clients').addClass('d-none');
      $('#all-projects').addClass('d-none');
      $('#select-clients').removeAttr('required');
    }
  });
  $('#select-clients').on('change', function () {
    if ($(this).val().length > 0) {
      $('#all-projects').removeClass('d-none');
    } else {
      $('#all-projects').addClass('d-none');
    }
  });
  $('#display-permits').on('change', 'input[type="file"]', function () {//readFileURL(this, $(this).data('fileid'), $(this).data('id'), $(this).data('where'));
  });
  $('#display-permits').on('click', '.btn-vfile', function (e) {
    e.preventDefault(); // if($(this).attr('href') != "javascript:void(0);"){
    // 	debugBase64($(this).attr('href'));
    // }
  });
  $('#index-table').on('click', 'a.btn-delete', function (e) {
    e.preventDefault();
    var projCount = 0;

    if ($(this).data('projcount')) {
      projCount = $(this).data('projcount');
    }

    confirmDelete($(this).attr('href'), projCount);
  }); // *************************
  // Token setup for AJAX call, Laravel requirement
  // *************************

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('body').on('change', '.custom-control-input', function () {
    if ($('.date-time-wrapper').hasClass('active')) {
      $('.date-time-wrapper').removeClass('active');
      $('.date-time-wrapper').slideUp();
    } else {
      $('.date-time-wrapper').addClass('active');
      $('.date-time-wrapper').slideDown();
    }
  });

  if ($('.body-dropdown').length == 1) {
    var container = document.querySelector('.body-dropdown');
    var ps = new PerfectScrollbar(container);
    var baseUrl = $('meta[name="base-url"]').attr('content', {
      wheelSpeed: 5,
      wheelPropagation: true,
      minScrollbarLength: 20,
      suppressScrollX: true
    });
  }

  $('.dropify').each(function () {
    var defaultImg = $(this).data('default-file-cs');
    $(this).dropify();
  });
  $('body').on('click', '.dropify-clear', function () {
    var has_upload = $(this).closest('.profile-pic').find('.has-upload');
    $(has_upload).val('false');
    $(has_upload).attr('value', 'false');
  }); //$('.card-same-height').matchHeight();

  $('li :checkbox').on('click', function () {
    var $chk = $(this),
        $li = $chk.closest('li'),
        $ul,
        $parent;

    if ($li.has('ul')) {
      $li.find(':checkbox').not(this).prop('checked', this.checked);
    }

    do {
      $ul = $li.parent();
      $parent = $ul.siblings(':checkbox');

      if ($chk.is(':checked')) {
        $parent.prop('checked', $ul.has(':checkbox:not(:checked)').length == 0);
      } else {
        $parent.prop('checked', false);
      }

      $chk = $parent;
      $li = $chk.closest('li');
    } while ($ul.is(':not(.someclass)'));
  });
  $(document).mouseup(function (e) {
    var container = $("#cs_trigger");

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $('#cs_trigger').removeClass('active');
      $(".cs-dropdown").fadeOut();
    }
  });
  $('body').on('click', '#cs_trigger', function () {
    var _self = $(this);

    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).next('.cs-dropdown').fadeOut();
    } else {
      $(this).addClass('active');
      $(this).next('.cs-dropdown').fadeIn();
    }
  });
  $('.switch-wrapper').on('click', '.selected-wrapper', function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).next('.options-wrapper').removeClass('active');
    } else {
      $(this).addClass('active');
      $(this).next('.options-wrapper').addClass('active');
    }
  }); // $('body').on('click','#sub-account-modal-submit', function(e) {
  //        e.preventDefault();
  //    	var _selfValue = $('#sub-accoount-modal-form #subAccounts').val();
  // 	var _selfUrl = $('#sub-accoount-modal-form #subAccounts').data('url');
  // 	var _newUrl = _selfUrl.replace("###", _selfValue);
  //        if (_selfValue) {
  // 		window.location.href = _newUrl ;
  // 	} else {
  // 		swal({
  // 			title: 'Error!',
  // 			text: 'Select a Sub Account.',
  // 			type: "warning",
  // 			confirmButtonColor: "#DD6B55",
  // 			confirmButtonText: "Ok!",
  // 			closeOnConfirm: true
  // 		});
  // 	}
  //    });

  $('body').on('click', '#sub-account-modal-submit', function () {
    var _selfValue = $('#sub-accoount-modal-form #subAccounts').val();

    var _selfUrl = $('#sub-accoount-modal-form #subAccounts').data('url');

    if (_selfValue) {
      _selfUrl += '?subaccount_id=' + _selfValue;
      window.location.href = _selfUrl;
    } else {
      swal({
        title: 'Error!',
        text: 'Select a Sub Account.',
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok!",
        closeOnConfirm: true
      });
    } // window.location.replace( _selfUrl );

  });
  $('body').on('change', '#account_type', function () {
    if ($(this).val() == 'account_user') {
      $('#sub_account').prop('required', true);
      $('#sub_account').attr('required', true);
      $('#roles').prop('required', true);
      $('#roles').attr('required', true);
    } else {
      $('#sub_account').prop('required', false);
      $('#sub_account').attr('required', false);
      $('#roles').prop('required', false);
      $('#roles').attr('required', false);
    }
  }); // *************************
  // Form Plugins
  // *************************

  var selector = '#select-filter-option > .option-hide';
  $('.select-option-filter').on('change', function () {
    var $setValue = $(this).val();
    $('.number-execution').hide();
    $('.customer').val('').trigger('chosen:updated');

    if ($setValue.length > 0) {
      var $getValue = [];
      $getValue.push($setValue);
      $.each($getValue, function (i, val) {
        if (val == $setValue) {
          $(selector).removeClass('show');
          $('.' + val).addClass('show');
        } else {
          $('.' + val).removeClass('show'); // $("#customer-group option:selected").prop("selected", false)
        }
      });
    } else {
      $('.option-hide').removeClass('show');
    }
    /* if multiple select */

    /*if ( $(this).val().length > 0 ){
    	// if multiple
    	var $getValue = [];
    	$getValue.push($(this).val());
    	$.each($getValue, function(i, val){
    		$(selector).removeClass('show');
    		$.each(val, function (e, arg) {
       			$('.'+arg).addClass('show');
    		});
    	});
    } else {
    	$(selector).removeClass('show');
    }*/

  });
  $('.shortcode-click').click(function () {
    $('.ul-listing').toggleClass('active');
    setTimeout(function () {
      $('.ul-listing').removeClass('active');
    }, 3000);
  });
  $('.day_filter').datepicker({
    format: 'dd',
    todayHighlight: true,
    autoclose: true,
    orientation: "bottom"
  });
  $('.month_filter').datepicker({
    showOtherMonths: true,
    selectOtherMonths: true,
    todayHighlight: true,
    autoclose: true,
    orientation: "bottom"
  }); // loop for year

  for (i = new Date().getFullYear(); i > 1900; i--) {
    $('#yearpicker').append($('<option />').val(i).html(i));
  }

  for (i = 1; i < 31; i++) {
    $('#dayfilter').append($('<option />').val(i).html(i));
  }

  $('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    todayHighlight: true,
    autoclose: true,
    orientation: "bottom",
    popup: {
      position: "top right" // origin: "top right"

    }
  });
  var date = new Date();
  date.setDate(date.getDate());
  $('#datepicker3').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    autoclose: true,
    orientation: "top",
    startDate: date,
    popup: {
      position: "top" // origin: "top left"

    }
  });
  $('#customer_listing_wrapper').DataTable({
    "dom": "<'row'<'col-md-5 col-12'l><'col-md-7 col-12'f>r><'table-responsive't><'row'<'col-md-5 col-12'i><'col-md-7 col-12'p>>",
    "order": [[3, "desc"]],
    "bLengthChange": false // "bFilter": false,
    // "bInfo": false,

  }); //$(".chzn-select").chosen({allow_single_deselect: true, height:"80%"});

  $(".chzn-select").chosen({
    allow_single_deselect: true
  }); //$(".chzn-select-deselect,#select2_sample").chosen();
  // $(".chosen-container").css({width:'90%'});

  $(".phone").inputmask({
    "mask": "(02) 999-9999"
  });
  $(".mobile-phone").inputmask({
    "mask": "+63 (999)-999-9999"
  }); // *************************
  // Tooltips Plugins
  // *************************
  // *************************
  // DataTable Plugin
  // use 'data-order="[[0, 'ASC']]"' @ table element to set the default sorting column
  // use 'data-orderable="false"' @ th element to set if column is sortable
  // use 'data-type="currency"' @ td element to set the data type of the cell eg: date, currency, number
  // *************************

  $(".dataTables_paginate .pagination").addClass("float-right");

  if ($('.table-data').length) {
    $('.table-data').each(function () {
      var aoColumns = [],
          $this = $(this);
      var $doIndex,
          $doOrdering,
          options = {
        responsive: true,
        paging: false,
        filter: false,
        "lengthMenu": [[50, 150, 250, 300, 450, 500, -1], [50, 150, 250, 300, 450, 500, "All"] // change per page values here
        ],
        "pageLength": 50,
        "bInfo": false
      };

      if ($(this).data('default-order')) {
        $doIndex = i;
        $doOrdering = $(this).attr('data-default-order');
      } else {
        $doIndex = 0;
        $doOrdering = 'desc';
      }

      if ($(this).data('no-default-order')) {
        options.order = [];
      } else {
        options.order = [[$doIndex, $doOrdering]];
      }

      if ($this.find('thead > tr').length == 1) {
        $this.find('thead th').each(function (i) {
          var sType = $(this).data("type"),
              oFalse = $(this).data("orderable");
          var lengthMenu = [[5, 15, 20, -1], [5, 15, 20, "All"] // change per page values here
          ];

          if (sType != '' || sType != undefined) {
            aoColumns.push({
              "type": sType,
              "targets": i,
              "lengthMenu": lengthMenu
            }); // if (sType != 'status-sort') {
            // 	aoColumns.push({ "type" : sType, "targets" : i });
            // } else {
            // 	aoColumns.push({ "targets" : i, "render" : 	function ( data, sType, full, meta ) {
            // alert(sType);
            //        if (stype == 'status-sort') {
            //            return $(data).find('span').hasClass('Move Up') ? 1 : 0;
            //        }  else {
            //            return data;
            //        }
            //   	}
            // 	});
            // }
          }

          if (oFalse != undefined) {
            aoColumns.push({
              "orderable": oFalse,
              "targets": i
            });
          }

          options.columnDefs = aoColumns;
        });
      }

      $.fn.dataTable.moment('MMM D, YYYY');
      $this.DataTable(options);
    });
    $('.table-data').parent().parent().css('width', '100%');
  } // *************************
  // Sweetalert 2 prompts
  // *************************


  $(".confirm").click(function (e) {
    var msg = $(this).data('confirm') || 'Are you sure?';
    var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
    e.preventDefault();

    var _self = $(this);

    swal({
      title: msg,
      text: msgText,
      type: "info",
      showCancelButton: true,
      confirmButtonText: "Yes, Continue!",
      closeOnConfirm: false
    }).then(function (isConfirm) {
      if (isConfirm) {
        window.location = _self.attr("href");
      }
    });
  });
  $(".confirmWarning").click(function (e) {
    var msg = $(this).data('confirm') || 'Are you sure?';
    var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
    e.preventDefault();

    var _self = $(this);

    swal({
      title: msg,
      text: msgText,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, continue!",
      confirmButtonColor: "#DD6B55",
      closeOnConfirm: false
    }).then(function (isConfirm) {
      if (isConfirm) {
        window.location = _self.attr("href");
      }
    });
  });
  $(".confirmDelete").click(function (e) {
    var msg = $(this).data('confirm') || 'Are you sure?';
    var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
    e.preventDefault();

    var _self = $(this);

    swal({
      title: msg,
      text: msgText,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    }).then(function (isConfirm) {
      if (isConfirm) {
        // _self.closest("form").submit();
        window.location.href = _self.attr('href');
      }
    });
  });
  $(".confirmCancelSubmit").click(function (e) {
    var msg = $(this).data('confirm') || 'Are you sure?';
    var msgText = $(this).data('confirm-text') || 'You can try this again later.';
    e.preventDefault();

    var _self = $(this);

    swal({
      title: msg,
      text: msgText,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#449d44",
      confirmButtonText: "Yes, Continue!",
      closeOnConfirm: false
    }).then(function (isConfirm) {
      if (isConfirm) {
        _self.closest("form").submit();
      }
    });
  });
  $(".confirmSubmit").click(function (e) {
    var msg = $(this).data('confirm') || 'Are you sure?';
    var msgText = $(this).data('confirm-text') || '';
    e.preventDefault();

    var _self = $(this);

    swal({
      title: msg,
      text: msgText,
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#449d44",
      confirmButtonText: "Yes, Continue!",
      closeOnConfirm: false
    }).then(function (isConfirm) {
      if (isConfirm) {
        _self.closest("form").submit();
      }
    });
  });
  $(".campaignCreationCheckSubaccounts").click(function (e) {
    e.preventDefault();

    var _self = $(this);

    var msg = $(this).data('confirm') || 'Create a Sub Account';
    var msgText = $(this).data('confirm-text') || '';
    var validateSubaccounts = $(this).data('validate-subaccounts');

    if (validateSubaccounts > 0) {
      window.location.href = _self.attr('href');
    } else {
      swal({
        title: msg,
        text: msgText,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#449d44",
        confirmButtonText: "Yes, Create One!",
        closeOnConfirm: false
      }).then(function (isConfirm) {
        if (isConfirm) {
          window.location.href = _self.data('subaccount-url');
        }
      });
    }
  }); //FOR DATERANGE PICKER

  $('.input-daterange').daterangepicker({
    autoApply: true,
    autoUpdateInput: false,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  }, function (start, end, label) {
    var selectedStartDate = start.format('YYYY-MM-DD');
    var selectedEndDate = end.format('YYYY-MM-DD');
    $checkinInput = $('#from_date');
    $checkoutInput = $('#to_date');
    $checkinInput.val(selectedStartDate);
    $checkoutInput.val(selectedEndDate); // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
    // var checkOutPicker = $checkoutInput.data('daterangepicker');
    // checkOutPicker.setStartDate(selectedStartDate);
    // checkOutPicker.setEndDate(selectedEndDate);
    // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
    // var checkInPicker = $checkinInput.data('daterangepicker');
    // checkInPicker.setStartDate(selectedStartDate);
    // checkInPicker.setEndDate(selectedEndDate);
  }); // SUMMERNOTE WYSIWYG
  // $('.summernote-editor').summernote({
  // 	placeholder: 'Your message here...'
  // });
  // SELECT2

  $('.select2-select').select2({
    placeholder: "Select Sub Account"
  });
  var tblTargets = $('#index-table').attr('data-disableCol'),
      tblSortIndex = $('#index-table').attr('data-sortindex'),
      tblSortBy = $('#index-table').attr('data-sortby');
  $('#index-table').DataTable({
    "dom": "<'row'<'col-md-5 col-12'l><'col-md-7 col-12'f>r><'table-responsive't><'row'<'col-md-5 col-12'i><'col-md-7 col-12'p>>",
    "order": [[parseInt(tblSortIndex), tblSortBy]],
    "paging": false,
    "info": false,
    "bFilter": false,
    'columnDefs': [{
      'targets': [parseInt(tblTargets)],

      /* column index */
      'orderable': false
      /* true or false */

    }]
  }); //new Switchery(document.querySelector('.js-switch'), { color: '#38c172', jackColor: '#fff' });
}); //MY CUSTOM FUNCTIONS, NOT FROM THEME TEMPLATE

function initSwitchery() {
  // var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch:not([data-switchery=true])'));
  // elems.forEach(function(html) {
  //   var switchery = new Switchery(html);
  // });
  $.each($('.make-switch-radio'), function () {
    $(this).bootstrapSwitch({
      onText: $(this).data('onText'),
      offText: $(this).data('offText'),
      onColor: $(this).data('onColor'),
      offColor: $(this).data('offColor'),
      size: $(this).data('size'),
      labelText: $(this).data('labelText')
    });
  });
}

function initDatePicker() {
  $('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    todayHighlight: true,
    autoclose: true,
    orientation: "bottom",
    popup: {
      position: "top right" // origin: "top right"

    }
  });
}

function arr_diff(a1, a2) {
  var a = [],
      diff = [];

  for (var i = 0; i < a1.length; i++) {
    a[a1[i]] = true;
  }

  for (var i = 0; i < a2.length; i++) {
    if (a[a2[i]]) delete a[a2[i]];else a[a2[i]] = true;
  }

  for (var k in a) {
    diff.push(k);
  }

  return diff;
}

function reinitChzn() {
  $(".chzn-select").chosen("destroy");
  $(".chzn-select").chosen({
    allow_single_deselect: true
  });
}

function stickySidebar() {
  $("#sticky-sidebar").sticky({
    topSpacing: 75,
    bottomSpacing: $(".footer").outerHeight() + 20
  });
}

function deletePreReq(btnThis) {
  swal({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#00c0ef',
    cancelButtonColor: '#ff8080',
    confirmButtonText: 'Yes, delete it!'
  }).then(function () {
    var deleteField = '<input type="hidden" class="form-control" name="Permit[' + btnThis.data('pkey') + '][Pre][' + btnThis.data('prekey') + '][delete]" value="1" />';
    btnThis.parent().parent().append(deleteField);
    btnThis.parent().hide();
    swal({
      title: 'Deleted!',
      text: 'Permit Pre-Requisite has been deleted!',
      type: 'success',
      confirmButtonColor: '#00c0ef'
    }).done();
  });
  return false;
}

function confirmDelete(locUrl, projCount) {
  swal({
    title: 'Are you sure?',
    text: 'You won\'t be able to revert this!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#00c0ef',
    cancelButtonColor: '#ff8080',
    confirmButtonText: 'Yes, delete it!'
  }).then(function () {
    if (projCount == 0) {
      location = locUrl;
    } else {
      swal({
        title: 'Oops...',
        text: 'Cannot delete the client with projects',
        type: 'error',
        confirmButtonColor: '#00c0ef'
      }).done();
    }
  });
  return false;
}

function readFileURL(input, data_id, view_id, where_val) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      if (where_val == "permit") {
        $('#permit-file-' + data_id).attr('href', e.target.result);
      } else {
        $('#file-' + data_id).attr('href', e.target.result);
      }
    };

    reader.readAsDataURL(input.files[0]);

    if (where_val == "permit") {
      $('#permit-view-file-' + view_id).removeClass('d-none');
    } else {
      $('#pre-view-file-' + view_id).removeClass('d-none');
    }
  }
}

function debugBase64(base64URL) {
  var win = window.open();
  win.document.write('<iframe src="' + base64URL + '" frameborder="0" style="border:0; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%;" allowfullscreen></iframe>');
}

function selectPermitAjax() {
  //SELECT PERMIT WITH AJAX | START
  $('#add-permit').on('click', function (e) {
    e.preventDefault();
    var btnThis = $(this);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    var selected_permit = $('#select-permit').val();
    var permitCount = 0;
    permitCount = $('#display-permits .permit-content-wrapper').length;

    if (selected_permit.length > 0) {
      $('#select_permit_chosen').removeClass('error');
      $('#slctError').addClass('d-none'); //AJAX TO GET THE PERMIT

      $.ajax({
        url: $(this).data('url'),
        method: 'post',
        data: {
          permit_ids: selected_permit,
          permitCount: permitCount
        },
        success: function success(result) {
          $('#permit-modal').modal('hide');
          $('#display-permits').append(result.html);
          var title = 'PERMIT HAS BEEN ADDED!';

          if (selected_permit.length > 1) {
            title = 'PERMITS HAS BEEN ADDED!';
          }

          swal({
            title: title,
            type: 'success',
            confirmButtonColor: '#00c0ef'
          }).done();
          return false;
        },
        error: function error(result) {
          $('#permit-modal').modal('hide');
          swal({
            title: 'Oops...',
            text: 'THERE IS AN ERROR IN YOUR CODE!',
            type: 'error',
            confirmButtonColor: '#00c0ef'
          }).done();
        },
        complete: function complete(result) {
          $('#select-permit option').prop('selected', false);
          initSwitchery();
          initDatePicker();
          reinitChzn();
          customFileUpload();
        }
      });
    } else {
      $('#select_permit_chosen').addClass('error');
      $('#slctError').removeClass('d-none');
    }
  }); //SELECT PERMIT WITH AJAX | END
}

function getClientProjects() {
  $('#select-clients').on('change', function () {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    var selectedClients = $(this).val(),
        clientArray = [],
        dataUrl = $(this).data('url'); //UNSELECT

    if ($('#client-projects optgroup').length > selectedClients.length) {
      var temp_id = [],
          temp_selected = [],
          must_remove_id = [];
      $('#client-projects optgroup').each(function () {
        temp_id.push($(this).data('id'));
      });
      $(selectedClients).each(function (key2, val2) {
        temp_selected.push(val2);
      });
      must_remove_id = arr_diff(temp_id, temp_selected);
      $(must_remove_id).each(function (key3, val3) {
        $('#client-projects optgroup').each(function () {
          if ($(this).data('id') == val3) {
            $(this).remove();
          }
        });
      });
    }

    if (selectedClients.length > 0) {
      var tempId = [],
          tempSelected = [],
          mustAddId = [];
      $(selectedClients).each(function (key, val) {
        tempSelected.push(val);
      });

      if ($('#client-projects optgroup').length > 0) {
        $('#client-projects optgroup').each(function () {
          tempId.push($(this).data('id'));
        });
      }

      mustAddId = arr_diff(tempId, tempSelected);
      clientArray = mustAddId;
    }

    $.ajax({
      url: dataUrl,
      method: 'post',
      data: {
        client_ids: clientArray
      },
      success: function success(result) {
        $('#client-projects').append(result.html);
      },
      complete: function complete(result) {
        reinitChzn();
      }
    });
  });
}

function sendPermitEmail() {
  $('#display-permits').on('click', '.btn-sendnotif', function (e) {
    e.preventDefault();
    var permitID = $(this).data('permit'),
        clientID = $(this).data('client'),
        btn = $(this);
    $.ajax({
      url: '../send-notif',
      method: 'POST',
      data: {
        clientID: clientID,
        permitID: permitID
      },
      beforeSend: function beforeSend(result) {
        btn.addClass('disabled');
        btn.text('SENDING ...');
      },
      success: function success(result) {
        btn.removeClass('disabled');
        btn.text('SEND NOTIFICATION');

        if (result) {
          swal({
            title: 'NOTIFICATION SENT!',
            type: 'success',
            confirmButtonColor: '#00c0ef'
          }).done();
          return false;
        } else {
          swal({
            title: 'Oops...',
            text: 'SENDING FAILED!',
            type: 'error',
            confirmButtonColor: '#00c0ef'
          }).done();
        }
      },
      complete: function complete(result) {}
    });
  });
}

function deletePermit() {
  $('#display-permits').on('click', 'a.x-parent', function (e) {
    e.preventDefault();
    var btnThis = $(this);
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00c0ef',
      cancelButtonColor: '#ff8080',
      confirmButtonText: 'Yes, delete it!'
    }).then(function () {
      var permCount = $('#perm-' + btnThis.data('key')).data('count'),
          appendField = '<input type="hidden" name="Permit[' + permCount + '][delete]" value="1">';
      $('#display-permits').append(appendField);
      $('#perm-' + btnThis.data('key')).remove();
      swal({
        title: 'Deleted!',
        text: 'Permit Pre-Requisite has been deleted!',
        type: 'success',
        confirmButtonColor: '#00c0ef'
      }).done();
    });
    return false;
  });
}

function dateRangePicker() {
  $('#dateRange').daterangepicker({
    autoUpdateInput: false,
    locale: {
      cancelLabel: 'Clear'
    }
  });
  $('#dateRange').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    return false;
  });
  $('#dateRange').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
    return false;
  });
}

function customFileUpload() {
  // $(".custom-file").fileinput({
  //     theme: "fa",
  //     uploadUrl: "/file-upload-batch/2",
  //     showUpload: false,
  //     showCancel: false,
  //     fileActionSettings: {
  //     	showUpload: false,
  //     	showDrag: false,
  //     	showZoom: false,
  //     }
  // });
  $('#display-permits .mult-permit-file').each(function (key, val) {
    var permitKey = $(this).data('id'),
        initialPreview = [];
    initialPreviewConfig = [];
    $('#existing-' + permitKey + ' .exists-path').each(function (key2, val2) {
      var key2 = key2 + 1;
      tmpUrl = $(this).val(), fileName = fileNameFromUrl(tmpUrl), fileExtension = fileName.split('.').pop(), fileID = $(this).data('id');
      initialPreview.push(tmpUrl);

      if (fileExtension == 'pdf') {
        initialPreviewConfig.push({
          type: "pdf",
          caption: fileName,
          url: '../delete/file',
          key: key2,
          extra: {
            id: fileID
          },
          downloadUrl: false
        });
      }

      if (fileExtension == 'png' || fileExtension == 'jpeg' || fileExtension == 'jpg') {
        initialPreviewConfig.push({
          caption: fileName,
          url: '../delete/file',
          key: key2,
          extra: {
            id: fileID
          },
          downloadUrl: false
        });
      }

      if (fileExtension == 'docx' || fileExtension == 'ppt' || fileExtension == 'pptx' || fileExtension == 'xls' || fileExtension == 'xlsx') {
        initialPreviewConfig.push({
          type: "office",
          caption: fileName,
          url: '../delete/file',
          key: key2,
          extra: {
            id: fileID
          },
          downloadUrl: false
        });
      }

      if (fileExtension == 'txt') {
        initialPreviewConfig.push({
          type: "html",
          caption: fileName,
          url: '../delete/file',
          key: key2,
          extra: {
            id: fileID
          },
          downloadUrl: false
        });
      }

      if (fileExtension == 'zip') {
        initialPreviewConfig.push({
          type: "html",
          caption: fileName,
          url: '../delete/file',
          key: key2,
          extra: {
            id: fileID
          },
          downloadUrl: false
        });
      }
    });
    $($(this)).fileinput({
      theme: "fa",
      uploadUrl: "/file-upload-batch/1",
      uploadAsync: false,
      allowedFileExtensions: ['jpg', 'jpeg', 'png', 'txt', 'word', 'docx', 'doc', 'pdf', 'ppt', 'pptx', 'xls', 'xlsx', 'zip'],
      minFileCount: 2,
      maxFileCount: 5,
      overwriteInitial: false,
      showUpload: false,
      showCancel: false,
      dropZoneEnabled: false,
      fileActionSettings: {
        showUpload: false,
        showDrag: false,
        showZoom: false
      },
      initialPreview: initialPreview,
      initialPreviewAsData: true,
      // identify if you are sending preview data only and not the raw markup
      initialPreviewFileType: 'image',
      // image is the default and can be overridden in config below
      initialPreviewConfig: initialPreviewConfig
    }).on('filebeforedelete', function () {
      return swal({
        title: 'Are you sure?',
        text: 'This file will be deleted without clicking the \'Save Changes\' Button and you won\'t be able to revert this!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#00c0ef',
        cancelButtonColor: '#ff8080',
        confirmButtonText: 'Yes, delete it!'
      }).then(function () {//Do something here...
      });
    }).on('filedeleted', function () {
      setTimeout(function () {
        swal({
          title: 'Deleted!',
          text: 'File has been deleted!',
          type: 'success',
          confirmButtonColor: '#00c0ef'
        }).done();
      }, 900);
    });
  }); // $('#display-permits').on('click','.permit-modal-cancel', function() {
  // 	$(this).parent().parent().find('.fileinput-remove').click();
  // });
} // returns a string on success, null in failure


function fileNameFromUrl(url) {
  var matches = url.match(/\/([^\/?#]+)[^\/]*$/);

  if (matches.length > 1) {
    return matches[1];
  }

  return null;
}

function sendSMS() {
  var security_key = $('#security_key').val(),
      app_key = $('#app_key').val(),
      number = $('#number').val(),
      name = $('#name').val(),
      message = $('#message').val(),
      error = 0,
      errorMessage = "";

  if (number.length < 11) {
    error++;
    errorMessage = errorMessage + "<span>Client's mobile number is required.</span>";
  }

  if (message.length == 0) {
    error++;
    errorMessage = errorMessage + "<span>Text message is required.</span>";
  }

  if (error > 0) {
    $('#sms-alert').html(errorMessage);
    $('#sms-alert').show();
  }

  if (error <= 0) {
    $('#sms-alert').html('');
    $('#sms-alert').hide();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: 'https://app.textify.ph/api/immediate-sms',
      type: 'GET',
      dataType: 'json',
      data: {
        security_key: security_key,
        app_key: app_key,
        number: number,
        name: name,
        message: message
      },
      beforeSend: function beforeSend(result) {
        $('#send-cancel').attr('disabled', true);
        $('#send-sms').attr('disabled', true);
        $('#send-sms').text('SENDING ...');
      },
      success: function success(result) {
        $('#send-cancel').removeAttr('disabled');
        $('#send-sms').removeAttr('disabled');
        $('#send-sms').text('SEND');
        $('#sms-modal').modal('hide');

        if (result.success) {
          swal({
            title: 'Success!',
            text: result.message,
            type: 'success',
            confirmButtonColor: '#00c0ef'
          }).done();
        } else {
          swal({
            title: 'Failed!',
            text: result.message,
            type: 'error',
            confirmButtonColor: '#00c0ef'
          }).done();
        }
      },
      error: function error(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    });
  }

  return;
}

/***/ }),

/***/ "./resources/assets/sass/app.scss":
/*!****************************************!*\
  !*** ./resources/assets/sass/app.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*****************************************************************************!*\
  !*** multi ./resources/assets/js/admin.js ./resources/assets/sass/app.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /var/www/html/permit-express/resources/assets/js/admin.js */"./resources/assets/js/admin.js");
module.exports = __webpack_require__(/*! /var/www/html/permit-express/resources/assets/sass/app.scss */"./resources/assets/sass/app.scss");


/***/ })

/******/ });