<?php

namespace PermitExpress\City;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PermitExpress\BaseRepository;

class CityRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        City $city
    )
    {
        $this->model = $city;
    }

    public function displayCities(Request $request, $with = [])
    {
        $query = $this->model->with($with);

        if($request->filled('search_key'))
        {
          $query = $query->where(function($q) use($request) {
            $q->where('name', 'LIKE', "%{$request->search_key}%");
          });
        }

        if($request->sort_by_name)
        {
            $query = $query->orderBy('name');
        }

        return $query->get();
    }

    public function getAll() {
        return $this->model->all();
    }

    public function createCity(Request $request)
    {
        $city = new City;

        $this->validateCityData($request, $city);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $city = $this->saveCity($request, $city);

        return $city;
    }

    public function updateCity(Request $request, $city)
    {
        $this->validateCityData($request, $city);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->saveCity($request, $city);

        return;
    }

    public function deleteCity($city)
    {
        $city->delete();
        return;
    }

    private function saveCity($request, $city)
    {
        $city->name = $request->name;

        $city->save();

        return $city;
    }

    public function validateCityData(Request $request, $model = NULL)
    {
        $rules = [];

        if($model->exists)
        {
            $rules['name'] = 'required|unique:cities,name,' . $model->id;
        }
        else
        {
            $rules['name'] = 'required|unique:cities,name';
        }

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }
}