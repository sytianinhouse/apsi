<?php

namespace PermitExpress\City;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\BaseModel;
use PermitExpress\ProjectPermit\ProjectPermit;
use PermitExpress\Project\Project;

class City extends BaseModel
{




    /**
     * Relations
     */

    public function projects()
    {
        return $this->hasMany(ProjectPermit::class, 'city_id');
    }

    public function projectPermits()
    {
        return $this->hasMany(ProjectPermit::class, 'city_id');
    }
}