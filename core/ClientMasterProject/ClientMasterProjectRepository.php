<?php

namespace PermitExpress\ClientMasterProject;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\BaseRepository;

class ClientMasterProjectRepository extends BaseRepository
{
    protected $model;

   public function __construct(
        ClientMasterProject $clientMasterProj
   )
   {
        $this->model = $clientMasterProj;
   }

    public function displayMasterProj(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if( $request->filled('search_key') )
        {
            $query = $query->where(function($q) use($request) {
                $q->where('name', 'LIKE', "%{$request->search_key}%");
            });
        }

        return $query->get();
    }

    public function getByMaster($client_id)
    {
        return $this->model->where('master_id', $client_id)->get();
    }

    public function getByProjectID($project_id)
    {
        return $this->model->where('project_id', $project_id)->get();
    }

    public function getByMasterProject($project_id, $master_id)
    {
        return $this->model->where('master_id', $master_id)->where('project_id', $project_id)->get();
    }

    public function getMastersProject($master_id)
    {
        return $this->model->where('master_id', $master_id)->pluck('project_id')->toArray();
    }

    public function saveFromProject($request, $project, $fromUpdate)
    {
        if($fromUpdate)
        {
            $this->deleteFromProject($project);
        }

        if($request)
        {
            foreach($request->master as $master)
            {
                $masterProject = new ClientMasterProject;

                $masterProject->master_id = $master;
                $masterProject->client_id = $project->client_id;
                $masterProject->project_id = $project->id;

                $masterProject->save();
            }
        }
    }

    public function deleteFromProject($project)
    {
        return $this->model->where('project_id', $project->id)->delete();
    }

    public function saveFromClient(Request $request, $client, $fromUpdate)
    {
        if($fromUpdate)
        {
            $this->deleteFromClient($client->id);
        }

        $tmpClientProj = (object) $this->rebuildRequestData($request);

        if( isset($tmpClientProj->clients) )
        {
            foreach($tmpClientProj->clients as $keys => $clientProj)
            {
                $clientProj = (object) $clientProj;

                if($clientProj->projects)
                {
                    foreach($clientProj->projects as $tmpProj)
                    {
                        $masterProject = new ClientMasterProject;

                        $masterProject->master_id = $client->id;
                        $masterProject->client_id = (int)$clientProj->id;
                        $masterProject->project_id = $tmpProj;

                        $masterProject->save();
                    }
                }
                else
                {
                    $masterProject = new ClientMasterProject;

                    $masterProject->master_id = $client->id;
                    $masterProject->client_id = (int)$clientProj->id;
                    $masterProject->project_id = 0;

                    $masterProject->save();
                }
            }
        }

        return;
    }

    public function deleteFromClient($client_id)
    {
        return $this->model->where('master_id', $client_id)->delete();
    }

    public function rebuildRequestData(Request $request)
    {
        $tmpClientProj = array();

        if( $request->clients )
        {
            foreach($request->clients as $ckey => $client)
            {

                $tmpClientProj['clients'][$ckey]['id'] = $client;
                $tmpClientProj['clients'][$ckey]['projects'] = array();

                if( $request->projects ) {
                    foreach( $request->projects as $pkey => $project )
                    {
                        $tmpBool = false;
                        foreach(json_decode($project) as $key => $tmpProj)
                        {
                            if($key == "client_id")
                            {
                                if( $client == $tmpProj )
                                {
                                    $tmpBool = true;
                                }
                            }

                            if($tmpBool)
                            {
                                if($key == 'project_id')
                                {
                                    $tmpClientProj['clients'][$ckey]['projects'][$pkey] = $tmpProj;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $tmpClientProj;
    }
}