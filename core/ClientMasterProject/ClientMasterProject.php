<?php

namespace PermitExpress\ClientMasterProject;

use PermitExpress\Client\Client;
use PermitExpress\Project\Project;
use PermitExpress\BaseModel;

class ClientMasterProject extends BaseModel
{

    /**
     * Relations
     */

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}