<?php

namespace PermitExpress\Client;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use PermitExpress\User\User;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\Project\Project;
use PermitExpress\BaseModel;


class Client extends BaseModel
{
    use SoftDeletes;

    protected $casts = [
        'additional_contacts' => 'array',
    ];


    /**
     * Relations
     */

    public function user()
    {
        return $this->hasOne(User::class, 'client_id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'client_id');
    }

    public function clientMasterProjects()
    {
        return $this->hasMany(ClientMasterProject::class, 'master_id');
    }

}