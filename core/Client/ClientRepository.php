<?php

namespace PermitExpress\Client;

use App\Mail\AdminNewClient;
use App\Mail\WelcomeClient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use PermitExpress\BaseRepository;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\ClientMasterProject\ClientMasterProjectRepository;
use PermitExpress\User\User;
use PermitExpress\User\UserRepository;


class ClientRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        Client $client,
        UserRepository $userRepo,
        ClientMasterProjectRepository $masterProjRepo
    )
    {
        $this->model = $client;
        $this->userRepo = $userRepo;
        $this->masterProjRepo = $masterProjRepo;
    }

    public function displayClients(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if($request->filled('search_key'))
        {
          $query = $query->where(function($q) use($request) {
            $q->where('name', 'LIKE', "%{$request->search_key}%");
          });
        }

        return $query->get();
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getAllExceptMaster()
    {
        return $this->model->where('is_master', '<>', 1)->get();
    }

    public function getMasters()
    {
        return $this->model->where('is_master', 1)->get();
    }

    public function getAllExcept($id)
    {
        return $this->model->where('id', '<>', $id)->get();
    }

    public function getAllExceptAndNoMaster($id)
    {
        return $this->model->where('id', '<>', $id)->where('is_master', '<>', 1)->get();
    }

    public function getByID($ids)
    {
        return $this->model->find($ids);
    }

    public function hasProjects(Client $client)
    {
        return $client->projects->count();
    }

    public function selectedOldClient($oldClient, $client)
    {
        $oldBool = false;

        foreach( $oldClient as $oldData )
        {
            if( $client->id == $oldData )
            {
                $oldBool = true;
            }
        }

        return $oldBool;
    }

    public function createClient(Request $request)
    {
        $client = new Client;

        $this->validateClientData($request, $client);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $client = $this->saveClient($request, $client);

        //if($client->email && $client->created_at->isToday())
        //{
            //$this->makeWelcomeEmail($client);
        //}

        return $client;
    }

    public function updateClient(Request $request, $client)
    {
        $this->validateClientData($request, $client);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->saveClient($request, $client, true);

        return;
    }

    public function updateProfile(Request $request, $client)
    {
        $this->validateClientData($request, $client);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->saveProfile($request, $client);

        return;
    }

    public function deleteClient($client)
    {
        if( $client->user->exists )
        {
            $this->masterProjRepo->deleteFromClient($client->id);
            $user = $client->user;
            $user->forceDelete();
            $client->delete();
        }
        return;
    }

    private function saveClient($request, $client, $fromUpdate = false)
    {
        $client->name = $request->name;
        $client->email = $request->email;
        $client->second_email = $request->second_email;
        $client->address = $request->address;
        $client->cp_first_name = $request->cp_first_name;
        $client->cp_last_name = $request->cp_last_name;
        $client->cp_number = $request->cp_number;
        $client->cp_number_2 = $request->cp_number_2;
        $client->additional_contacts = $request->additional_contacts;
        $client->type = $request->type;
        $client->is_master = ($request->is_master) ? $request->is_master : 0;

        $client->save();

        $userData = array();

        // $randomPass = sha1(time());
        // $randomUser = $this->randomClientUsername($randomPass);

        if( isset($client->user) )
        {
            $userData['id'] = $client->user->id;
        }

        if( $request->password )
        {
            $userData['password'] = Hash::make($request->password);
        }

        $userData['first_name'] = $client->cp_first_name;
        $userData['last_name'] = $client->cp_last_name;
        $userData['email'] = $client->email;
        $userData['client_id'] = $client->id;
        $userData['username'] = $request->username;
        $userData['active'] = ($request->active) ? $request->active : 0;

        $user = $this->userRepo->saveFromClient($userData);

        if($client->is_master == 1)
        {
            $masterClient = $this->masterProjRepo->saveFromClient($request, $client, $fromUpdate);
        }
        else
        {
            if($fromUpdate)
            {
                $this->masterProjRepo->deleteFromClient($client->id);
            }
        }

        return $client;
    }

    private function saveProfile($request, $client, $fromUpdate = false)
    {
        $client->name = $request->name;
        $client->address = $request->address;
        $client->cp_first_name = $request->cp_first_name;
        $client->cp_last_name = $request->cp_last_name;
        $client->cp_number = $request->cp_number;
        $client->cp_number_2 = $request->cp_number_2;
        $client->additional_contacts = $request->additional_contacts;
        $client->type = $request->type;

        $client->save();

        $userData = array();

        if( $request->password )
        {
            $userData['password'] = Hash::make($request->password);
        }

        $userData['id'] = $client->user->id;
        $userData['first_name'] = $client->cp_first_name;
        $userData['last_name'] = $client->cp_last_name;
        $userData['client_id'] = $client->id;

        $user = $this->userRepo->saveFromClient($userData);

        return $client;
    }

    public function validateClientData(Request $request, $model = NULL)
    {
        $rules = [
            'name'  =>  'required',
        ];

        if( isset($model->user) )
        {
            if( Auth::user()->isSuperAdmin() )
            {
                $rules['email'] = 'required|unique:users,email,' . $model->user->id;
                //$rules['second_email'] = 'required';
                //$rules['type'] = 'required';
            }
        }
        else
        {
            $rules['email'] = 'required|unique:users,email';
            //$rules['second_email'] = 'required';
        }

        if( isset($model->user) )
        {
            if( Auth::user()->isSuperAdmin() )
            {
                $rules['username'] = 'required|unique:users,username,' . $model->user->id;
            }
        }
        else
        {
            $rules['username'] = 'required|unique:users,username';
        }

        if( !$model->exists || $request->filled('password') )
        {
            $rules['password'] = 'required|max:15|min:6|confirmed';
            $rules['password_confirmation'] = 'required';
        }

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    private function randomClientUsername($value)
    {
        $random = str_shuffle($value);
        $password = substr($random, 0, 8);
        return $password;
    }

    private function makeWelcomeEmail(Client $client)
    {
        // Sends a welcome email to new created/registered user
        Mail::to($client->email)->send(new WelcomeClient($client));

        // Sends a notification email to admin that new user created/registered
        //$siteEmail = SiteSettings::first()->site_email;
        Mail::to('jerome.sytian@gmail.com')->send(new AdminNewClient($client));

        if (Mail::failures()) {
            Log::error('A welcome email was failed to send ' . Carbon::now()->toDateTimeString());
        }

        return;
    }
}