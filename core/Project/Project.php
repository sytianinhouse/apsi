<?php

namespace PermitExpress\Project;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PermitExpress\Client\Client;
use PermitExpress\City\City;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\ProjectPermit\ProjectPermit;
use PermitExpress\ProjectPrerequisitePermit\ProjectPrerequisitePermit;
use PermitExpress\BaseModel;

class Project extends BaseModel
{

    use SoftDeletes;

    /**
     * Relations
     */
    public function projectPermits()
    {
        return $this->hasMany(ProjectPermit::class, 'project_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function clientMasterProjects()
    {
        return $this->hasMany(ClientMasterProject::class, 'project_id');
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($project) {
            $projectRepo = resolve(\PermitExpress\Uploadable\UploadableRepository::class);
            $projPermIds = array();

            foreach($project->projectPermits as $projPerms)
            {
                array_push($projPermIds, $projPerms->id);

                foreach ($projPerms->uploads as $ups) {
                    $projectRepo->deleteThisUploadable($ups);
                }

                foreach($projPerms->projectPrerequisitePermits as $preReqs)
                {
                    foreach ($preReqs->uploads as $ups2) {
                        $projectRepo->deleteThisUploadable($ups2);
                    }
                }
            }

            DB::table('project_prerequisite_permits')->whereIn('project_permits_id', $projPermIds)->delete();
            $project->projectPermits()->delete();
        });
    }

    /**
     * mutators
     */

    public function getPermitCountsAttribute()
    {
        $count = 0;

        foreach($this->projectPermits as $permits)
        {
            $count++;

            $count = $count + $permits->projectPrerequisitePermits->count();
        }

        return $count;
    }
}