<?php

namespace PermitExpress\Project;

use App\Mail\ProjectPermitNotif;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use PermitExpress\Client\Client;
use PermitExpress\Client\ClientRepository;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\ClientMasterProject\ClientMasterProjectRepository;
use PermitExpress\ProjectPermit\ProjectPermit;
use PermitExpress\ProjectPermit\ProjectPermitRepository;
use PermitExpress\BaseRepository;

class ProjectRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        Project $project,
        ProjectPermitRepository $projectPermitRepo,
        ClientRepository $clientRepo,
        ClientMasterProjectRepository $masterProjRepo
    )
    {
        $this->model = $project;
        $this->projectPermitRepo = $projectPermitRepo;
        $this->clientRepo = $clientRepo;
        $this->masterProjRepo = $masterProjRepo;
    }

    public function displayProjects(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if($request->filled('date_range'))
        {
            $dateRange =  explode(' - ', $request->date_range);

            $newDate = [0 => Carbon::parse($dateRange[0])->copy()->toDateString(),
                        1 => Carbon::parse($dateRange[1])->copy()->toDateString()];

            $query = $query->whereBetween('created_at', $newDate);
        }


        if($request->filled('search_key'))
        {
            $query = $query->where(function($q) use($request) {
                $q->where('name', 'LIKE', "%{$request->search_key}%");
                $q->orWhereHas('client', function ($query) use($request) {
                    $query->where('name', 'LIKE', "%{$request->search_key}%");
                });
            });
        }

        if($request->filled('client_id'))
        {
            $query = $query->where('client_id', $request->client_id);
        }

        if($request->filled('show_to_client'))
        {
            $query = $query->where('show_to_client', 1);
        }

        if($request->filled('limit'))
        {
            $query = $query->limit($request->limit);
        }

        return $query->get();
    }

    public function getByClient($ids)
    {
        return $this->model->where('client_id', $ids)->get();
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function hasPermits(Project $project)
    {
        return $project->projectPermits()->count();
    }

    public function createProject(Request $request)
    {
        $project = new Project;

        $this->validateProjectData($request, $project);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $project = $this->saveProject($request, $project);

        return $project;
    }

    public function updateProject(Request $request, $project)
    {
        $this->validateProjectData($request, $project);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->saveProject($request, $project, true);

        return;
    }

    private function saveProject($request, $project, $fromUpdate = false)
    {

        $project->name = $request->name;
        $project->client_id = $request->client;
        $project->city_id = $request->project_city;
        $project->show_to_client = ($request->show_to_client) ? $request->show_to_client : 0;
        $project->type = $request->project_type;
        $project->barangay = $request->barangay;

        $project->save();

        $projPermit = $this->projectPermitRepo->saveFromProject($request, $project);

        if($request->master)
        {
            $this->masterProjRepo->saveFromProject($request, $project, $fromUpdate);
        }
        else
        {
            if($fromUpdate)
            {
                $this->masterProjRepo->deleteFromProject($project);
            }
        }

        return $project;
    }

    public function deleteProject($project)
    {
        $project->delete();
        return;
    }

    public function makePermitEmail($request)
    {
        $client = $this->clientRepo->getByID($request->clientID);
        $projPermit = $this->projectPermitRepo->getByID($request->permitID);

        $tmpEmails = array();

        if($client->email)
        {
            array_push($tmpEmails, $client->email);
        }

        if($client->second_email)
        {
            array_push($tmpEmails, $client->second_email);
        }

        Mail::to($tmpEmails)->send(new ProjectPermitNotif($projPermit->load('city'), $client));
        if( Mail::failures() )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function validateProjectData(Request $request, $model = NULL)
    {
        $rules = [
            'name'  =>  'required',
            'client' => 'required',
        ];

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }
}