<?php

namespace PermitExpress\ProjectPermit;

use Carbon\Carbon;
use Illuminate\Http\Request;
use PermitExpress\Uploadable\Uploadable;
use PermitExpress\Uploadable\UploadableRepository;
use PermitExpress\ProjectPrerequisitePermit\ProjectPrerequisitePermit;
use PermitExpress\ProjectPrerequisitePermit\ProjectPrerequisitePermitRepository;
use PermitExpress\BaseRepository;

class ProjectPermitRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        ProjectPermit $projectPermit,
        UploadableRepository $uploadableRepo,
        ProjectPrerequisitePermitRepository $projPrereqPermitRepo
    )
    {
        $this->model = $projectPermit;
        $this->projPrereqPermitRepo = $projPrereqPermitRepo;
        $this->uploadableRepo = $uploadableRepo;
    }

    public function displayProjectPermits(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if($request->filled('search_key'))
        {
            $query = $query->where(function($q) use($request) {
                $q->where('name', 'LIKE', "%{$request->search_key}%");
            });
        }

        return $query->get();
    }

    public function beforeExpiration()
    {

        $betweenDate = [0 => Carbon::now()->toDateTimeString(),
                        1 => Carbon::now()->addMonth(1)->toDateTimeString()
                        ];

        $query = $this->model->whereBetween('date_expiry', $betweenDate)->get();

        return $query;
    }

    public function getExpiredByMaster($projects)
    {
        $betweenDate = [0 => Carbon::now()->toDateTimeString(),
                        1 => Carbon::now()->addMonth(1)->toDateTimeString()
                        ];

        $query = $this->model->whereBetween('date_expiry', $betweenDate)->where('project_id', $projects)->get();

        return $query;
    }

    public function beforeDueDate()
    {
        $betweenDate = [0 => Carbon::now()->toDateTimeString(),
                        1 => Carbon::now()->addWeeks(2)->toDateTimeString()
                        ];

        $query = $this->model->whereBetween('date_due', $betweenDate)->get();

        return $query;
    }

    public function temporaryStatus()
    {
        return $this->model->where('status', 'temporary')->get();
    }

    public function dateChecker($date)
    {
        $betweenDate = [0 => Carbon::now()->toDateTimeString(),
                        1 => Carbon::now()->addMonth(1)->toDateTimeString()
                        ];

        $date = Carbon::parse($date)->copy()->toDateString();

        if($date > $betweenDate[0] && $date < $betweenDate[1])
        {
            return true;
        }
        return false;
    }

    public function getAll()
    {
      return $this->model->get();
    }

    public function getByID($ids)
    {
        return $this->model->find($ids);
    }

    public function getByProjID($id)
    {
        return $this->model->where('project_id', $id)->get();
    }

    public function saveFromProject($request, $project)
    {
        if($request->Permit)
        {
            foreach($request->Permit as $key => $permit)
            {
                if( isset($permit['id']) )
                {
                    $projPermit = $this->model->find($permit['id']);
                }
                else
                {
                    $projPermit = new ProjectPermit;
                }

                if( isset($permit['delete']) )
                {
                    if( isset($permit['id']) )
                    {
                        foreach ($projPermit->uploads as $ups) {
                            $this->uploadableRepo->deleteThisUploadable($ups);
                            $ups->delete();
                        }
                        $projPermit->delete();
                    }
                }
                else
                {
                    //$permit = (object) $permit;
                    $projPermit->project_id = $project->id;
                    $projPermit->permit_id = $permit['permit_id'];
                    $projPermit->city_id = $permit['city'];
                    $projPermit->status = $permit['status'];
                    $projPermit->name = $permit['name'];
                    $projPermit->branch_name = $permit['branch_name'];

                    if($permit['check_note'])
                    {
                        $projPermit->notes = $permit['notes'];
                    }
                    else
                    {
                        $projPermit->notes = null;
                    }

                    if($permit['date_filed'])
                    {
                        $projPermit->date_filed = Carbon::parse($permit['date_filed'])->copy()->toDateString();
                    }

                    if($permit['date_acquired'])
                    {
                        $projPermit->date_acquired = Carbon::parse($permit['date_acquired'])->copy()->toDateString();
                    }

                    if($permit['date_expiry'])
                    {
                        $projPermit->date_expiry = Carbon::parse($permit['date_expiry'])->copy()->toDateString();
                    }

                    if($permit['date_due'])
                    {
                        $projPermit->date_due = Carbon::parse($permit['date_due'])->copy()->toDateString();
                    }

                    $projPermit->save();

                    if(isset($permit['uploads']))
                    {
                        foreach($permit['uploads'] as $dataUpload)
                        {
                            $fileType = $dataUpload->getClientOriginalExtension();

                            if($this->idType($fileType))
                            {
                                $this->uploadableRepo->customUpload(
                                    $projPermit,
                                    $dataUpload,
                                    $this->idType($fileType),
                                    [
                                        'key' => ProjectPermit::ATTACHMENT,
                                        'field_key' => null,
                                        'path' => 'uploads/project-permits',
                                        'filename' => $projPermit->name.$projPermit->id.'.'.$fileType,
                                        'multiple' => true
                                    ]
                                );
                            }
                        }
                    }

                    // if( isset($permit['upload']) )
                    // {
                    //     $fileType = $permit['upload']->getClientOriginalExtension();

                    //     $this->uploadableRepo->customUpload(
                    //         $projPermit,
                    //         $permit['upload'],
                    //         $this->idType($fileType),
                    //         [
                    //             'key' => ProjectPermit::ATTACHMENT,
                    //             'field_key' => null,
                    //             'path' => 'uploads/project-permits',
                    //             'filename' => $projPermit->name.'-'.$projPermit->id
                    //         ]
                    //     );
                    // }

                    if(isset($permit['Pre']))
                    {
                        $projPreReq = $this->projPrereqPermitRepo->saveFromProjectPermit($permit['Pre'], $projPermit);
                    }
                }
            }
            return;
        }
    }

    public function idType($extension)
    {
        $images = ['jpg', 'jpeg', 'png'];
        $files = ['txt', 'word', 'docx', 'doc', 'pdf', 'ppt', 'pptx', 'xls', 'xlsx', 'zip'];

        if (in_array($extension, $images)) {
            return 'image';
        } else if(in_array($extension, $files)) {
            return 'file';
        }
    }
}