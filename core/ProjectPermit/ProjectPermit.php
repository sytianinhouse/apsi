<?php

namespace PermitExpress\ProjectPermit;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\Project\Project;
use PermitExpress\Permit\Permit;
use PermitExpress\Uploadable\Uploadable;
use PermitExpress\ProjectPrerequisitePermit\ProjectPrerequisitePermit;
use PermitExpress\BaseModel;
use PermitExpress\City\City;
use Intervention\Image\Facades\Image;

class ProjectPermit extends BaseModel
{

    const ATTACHMENT = 'permit-attachment';


    /**
     * Relations
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function permit()
    {
        return $this->belongsTo(Permit::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function projectPrerequisitePermits()
    {
        return $this->hasMany(ProjectPrerequisitePermit::class, 'project_permits_id');
    }

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    /**
     * mutators
     */

    public function getDpAttribute()
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::ATTACHMENT ? true : false;
        })->first();
        return $uploads;
    }

    public function getFilePathAttribute()
    {
        return $this->dp ? $this->dp->path : NULL;
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($projPermit) {
            $projPermit->projectPrerequisitePermits()->delete();
        });
    }
}