<?php

namespace PermitExpress\Setting;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PermitExpress\BaseRepository;

class SettingRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        SiteSetting $siteSetting
    )
    {
        $this->model = $siteSetting;
    }

    public function getSiteName()
    {
        return $this->model->pluck('site_name');
    }

    public function getSettings()
    {
        return $this->model->first();
    }

    public function isMaintenance()
    {
        return ($this->model->pluck('under_maintenance')[0]) ? true : false;
    }

    public function saveSetting(Request $request, SiteSetting $siteSetting)
    {
        $this->validateSettingData($request, $siteSetting);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $siteSetting->site_email = $request->site_email;
        $siteSetting->site_name = $request->site_name;
        $siteSetting->under_maintenance = ($request->under_maintenance) ? 1 : 0;

        $siteSetting->save();

        return $siteSetting;
    }

    public function validateSettingData(Request $request, $model = NULL)
    {
        $rules = [
            'site_email'  =>  'required',
            'site_name'   =>  'required'
        ];

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

}