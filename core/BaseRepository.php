<?php

namespace PermitExpress;

use Illuminate\Database\Eloquent\Model;
use PermitExpress\Validation\ValidationException;
use PermitExpress\Validation\ValidationInterface;
use Illuminate\Http\Request;

abstract class BaseRepository implements ValidationInterface
{
    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var \Illuminate\Support\MessageBag|array
     */
    protected $errors = [];

    /**
     * @var boolean Whether validation exception will be thrown
     */
    protected $throwValidationException = true;

    public function validate(Model $model, $ruleset = 'saving')
    {
        if (! $model->isValid($ruleset))
        {
            if ( !$this->throwValidationException) {
                $this->errors = $model->getErrors();
                return false;
            }
            throw new ValidationException($model->getErrors());
        }

        return true;
    }

    public function validationPasses(Model $model)
    {
        if ( ! $model->isValid())
        {
            if ( ! $this->throwValidationException) {
                $this->errors = $model->getErrors();
                return false;
            }
            throw new ValidationException($model->getErrors());
        }

        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function abort()
    {
        abort(404);
    }

    public function throwValidationException(array $errors = array())
    {
        $errors = new \Illuminate\Support\MessageBag($errors);

        throw new \PermitExpress\Validation\ValidationException($errors);
    }

    /**
     * Make a unique slug name
     *
     * @param $name
     * @param $ctr
     * @return string
     */
    public function makeSlugName($name, $ctr, $except = null)
    {
        $oldName = $name;
        $name = str_slug($name, '-');
        if ($ctr > 1) {
            $name .= '-'.$ctr;
        }

        $query = $this->model->where('slug', '=', $name);

        if ($this->usesSoftDeletes()) {
            $query = $query->withTrashed();
        }

        if ($this->model->exists) {
            $query = $query->where('id', '<>', $this->model->id);
        }

        if ($except) {
            $query = $query->where('id', '<>', $except);
        }
        $product = $query->get();

        if($product->count() > 0){
            $ctr++;
            return $this->makeSlugName($oldName, $ctr);
        }

        return $name;
    }

    public function getAllBy($key, $value, $with = array())
    {
        $collection = $this->model->with($with)->where($key, $value)->get();

        return $collection;
    }
}
