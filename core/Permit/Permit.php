<?php

namespace PermitExpress\Permit;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\BaseModel;
use PermitExpress\PrerequisitePermit\PrerequisitePermit;
use PermitExpress\PermitCategory\PermitCategory;
use PermitExpress\ProjectPermit\ProjectPermit;

class Permit extends BaseModel
{

    const OPEN = 'open';
    const PAID = 'paid';
    const CLOSED = 'closed';
    const TEMPORARY = 'temporary';

    public static $statuses = [
        self::OPEN => 'Open',
        self::PAID => 'Paid',
        self::CLOSED => 'Closed',
        self::TEMPORARY => 'Temporary',
    ];

    /**
     * Relations
     */
    public function prerequisitePermits()
    {
        return $this->hasMany(PrerequisitePermit::class, 'permit_id');
    }

    public function permitCategory()
    {
        return $this->belongsTo(PermitCategory::class);
    }

    public function projectPermits()
    {
        return $this->hasMany(ProjectPermit::class, 'permit_id');
    }

    /*
     * Static Methods
     */

    public static function boot() {
        parent::boot();
        static::deleting(function($permit) {
            $permit->prerequisitePermits()->delete();
        });
    }

}