<?php

namespace PermitExpress\Permit;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PermitExpress\BaseRepository;
use PermitExpress\PrerequisitePermit\PrerequisitePermit;
use PermitExpress\PrerequisitePermit\PrerequisitePermitRepository;

class PermitRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        Permit $permit,
        PrerequisitePermitRepository $prereqPermitRepo
    )
    {
        $this->model = $permit;
        $this->prereqPermitRepo = $prereqPermitRepo;
    }

    public function displayPermits(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if($request->filled('search_key'))
        {
          $query = $query->where(function($q) use($request) {
            $q->where('name', 'LIKE', "%{$request->search_key}%");
          });
        }

        return $query->get();
    }

    public function getAll() {
        return $this->model->all();
    }

    public function getById($ids)
    {
        return $this->model->find($ids);
    }

    public function checkCategories(Permit $permit)
    {
        return $permit->permitCategory()->count();
    }

    public function hasCategory($permitCategory)
    {
        return $this->model->where('permit_category_id', $permitCategory->id)->count();
    }

    public function createPermit(Request $request)
    {
        $permit = new Permit;

        $this->validatePermitData($request, $permit);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $permit = $this->savePermit($request, $permit);

        return $permit;
    }

    public function updatePermit(Request $request, $permit)
    {
        $this->validatePermitData($request, $permit);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->savePermit($request, $permit);

        return;
    }

    public function deletePermit($permit)
    {
        $permit->delete();
        return;
    }

    private function savePermit($request, $permit)
    {
        $permit->name = $request->name;
        $permit->permit_category_id = $request->permit_category_id;

        $permit->save();

        $this->prereqPermitRepo->savePrereqPermit($request->pre_permits, $permit);

        return $permit;
    }

    public function validatePermitData(Request $request, $model = NULL)
    {
        $rules = [];

        if($model->exists)
        {
            $rules['name'] = 'required|unique:permits,name,' . $model->id;
        }
        else
        {
            $rules['name'] = 'required|unique:permits,name';
        }

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

}