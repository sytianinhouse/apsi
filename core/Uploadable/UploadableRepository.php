<?php

namespace PermitExpress\Uploadable;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use PermitExpress\Traits\Uploadable as UploadableTrait;
use Illuminate\Support\MessageBag;
use PermitExpress\BaseRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadableRepository extends BaseRepository
{
    use UploadableTrait;

    protected $model;

    public function __construct(
        Uploadable $uploadable
    )
    {
        $this->model = $uploadable;
    }

    public function makeUploadFromUrl($model, $key, $path, $filename) 
    {
        $upload = new Uploadable();
        $upload->key = $key;
        $upload->filename = $filename;
        $upload->uploadable_type = get_class($model);
        $upload->uploadable_id = $model->id;
        $upload->path = $path;
        $upload->save();

        return $upload;
    }

    public function createUpload($model, $request, $fileType, $options = [])
    {
        return $this->uploadImage($model, $request, $options, $fileType);
    }

    public function customUpload($model, $file, $fileType, $options = [])
    {
        return $this->uploadImage($model, null, $options, $fileType, true, $file);
    }

    public function uploadImage($model, $request, $options, $fileType, $noRequest = false, $fileObj = null)
    {
        $messageBag = new MessageBag();


        $multiple =  Arr::get($options, 'multiple');

        $key = Arr::get($options, 'key'); // Important part of option
        $fieldKey = Arr::get($options, 'field_key');
        $path = Arr::get($options, 'path');
        $filename = Arr::get($options, 'filename');
        $width = Arr::get($options, 'width');
        $height = Arr::get($options, 'height');

        if (!$key) {
            $this->errors = $messageBag->add('date', 'Key for upload image is not defined.')->merge($this->errors);
            return;
        }

        $modelUpload = null;

        if ($multiple) {
          if (!$noRequest) {
            if ($request->filled('upload_id')) {
                $modelUpload = $model->uploads()->find($request->upload_id);
            }
          }
        } else {
            if ($model->uploads->count() > 0) {
                $modelUpload = $model->uploads->filter(function ($ups) use ($key) {
                    return $ups->key == $key ? true : false;
                })->first();
            }
        }

        $upload = $modelUpload ?: new Uploadable();

        $upload->key = $key;

        if ($upload->exists) {
          if (!$noRequest) {
            $file = $request->file($fieldKey ? $fieldKey : 'image') ?: $request->{$fieldKey};

            if ($file instanceof UploadedFile || $request->hasFile($fieldKey ? $fieldKey : 'file')) {
                $this->deleteUpload($upload);
            }
          } else {
            // para sa call na hindi gumagamit ng normal request
            $file = $fileObj;

            if ($fileObj instanceof UploadedFile) {
              $this->deleteUpload($upload);
            }
          }
        }

        $file = null;
        if (!$noRequest) {
          $file = $request->file($fieldKey ? $fieldKey : ($fileType == 'image' ? 'image' : 'file')) ?: $request->{$fieldKey};
        } else {
          $file = $fileObj;
        }

        if ($fileType == 'image') {
            if ($file instanceof UploadedFile) {
                $data = $this->baseUpload($file, $path ?: 'uploads/etc', $width ?: NULL, $height ?: NULL, false, $filename);
                $upload->path = $data['path'];
                $upload->filename = $data['fileName'];
            }
        } else if ($fileType == 'file') {
            if ($file instanceof UploadedFile) {
                $data = $this->fileUpload($file, $path ?: 'uploads/etc', $filename);
                $upload->path = $data['path'];
                $upload->filename = $data['fileName'];
            }
        }

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $upload->uploadable_type = get_class($model);
        $upload->uploadable_id = $model->id;
        $upload->type = $fileType;
        //$upload->order = !$noRequest ? ($request->filled('order') ? $request->order : null) : 0;
        $upload->save();

        return $upload;
    }

    /**
     * @param $file - Can be filepath, base64 and etc. see image intervention documentation for reference http://image.intervention.io/api/make
     * @param $request
     * @param bool $saveAsUploadObject - either return as Uploadable object or uploaded path
     */
    public function uploadImageIntervention($file, $request, $options = [], $saveAsUploadObject = true)
    {
        $image = Image::make($file);

        if ($image instanceof \Intervention\Image\Image) {
            $path = Arr::get($options, 'path');
            $filename = Arr::get($options, 'filename');
            $width = Arr::get($options, 'width');
            $height = Arr::get($options, 'height');


            $data = $this->interventionUploadImage($image, $path ?: 'uploads/etc', null, $width ?: NULL, $height ?: NULL, false, Str::random(15));

            return $data;
        }

        return false;
    }

    public function deleteThisUploadable($uploadable)
    {
        $this->deleteUpload($uploadable);
    }
}