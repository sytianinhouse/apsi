<?php

namespace PermitExpress\Uploadable;

use PermitExpress\BaseModel;

class Uploadable extends BaseModel
{
    protected $table = 'uploadables';

    const IMG_PLACEHOLDER = 'images/placeholder.png';

    public function uploadable()
    {
        return $this->morphTo();
    }

    public function auditCreate()
    {
        return "New image '{$this->filename}'";
    }
}