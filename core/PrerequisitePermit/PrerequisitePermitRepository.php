<?php

namespace PermitExpress\PrerequisitePermit;

use Carbon\Carbon;
use Illuminate\Http\Request;
use PermitExpress\BaseRepository;
use PermitExpress\Permit\Permit;
use PermitExpress\Permit\PermitRepository;

class PrerequisitePermitRepository extends BaseRepository
{

    protected $model;

    public function __construct(
        PrerequisitePermit $prereqPermit
    )
    {
        $this->model = $prereqPermit;
    }

    public function getAll() {
        return $this->model->all();
    }

    public function savePrereqPermit($request, $permit)
    {
        if($request)
        {
            foreach($request as $preReq)
            {
                if( isset($preReq['id']) )
                {
                    $preReqPermit = PrerequisitePermit::find($preReq['id']);
                }
                else
                {
                    $preReqPermit = new PrerequisitePermit;
                }

                if( isset($preReq['delete']))
                {
                    if( isset($preReq['id']) )
                    {
                        $preReqPermit->delete();
                    }
                }
                else {
                    if($preReq['name'])
                    {
                        $preReqPermit->name = $preReq['name'];
                        $preReqPermit->permit_id = $permit->id;
                        $preReqPermit->save();
                    }
                }
            }
        }
    }
}