<?php

namespace PermitExpress\PrerequisitePermit;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\BaseModel;
use PermitExpress\Permit\Permit;

class PrerequisitePermit extends BaseModel
{




    /**
     * Relations
     */
    public function permit()
    {
        return $this->belongsTo(Permit::class);
    }
}