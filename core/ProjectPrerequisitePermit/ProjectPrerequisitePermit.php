<?php

namespace PermitExpress\ProjectPrerequisitePermit;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\ProjectPermit\ProjectPermit;
use PermitExpress\Uploadable\Uploadable;
use PermitExpress\BaseModel;

class ProjectPrerequisitePermit extends BaseModel
{

    const ATTACHMENT = 'prereq-attachment';


    /**
     * Relations
     */
    public function projectPermit()
    {
        return $this->belongsTo(ProjectPermit::class);
    }

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    /**
     * mutators
     */

    public function getDpAttribute()
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::ATTACHMENT ? true : false;
        })->first();
        return $uploads;
    }

    public function getFilePathAttribute()
    {
        return $this->dp ? $this->dp->path : NULL;
    }
}