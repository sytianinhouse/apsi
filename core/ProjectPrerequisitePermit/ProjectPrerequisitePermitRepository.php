<?php

namespace PermitExpress\ProjectPrerequisitePermit;

use Carbon\Carbon;
use Illuminate\Http\Request;
use PermitExpress\Uploadable\Uploadable;
use PermitExpress\Uploadable\UploadableRepository;
use PermitExpress\BaseRepository;

class ProjectPrerequisitePermitRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        ProjectPrerequisitePermit $projectPrerequisitePermit,
        UploadableRepository $uploadableRepo
    )
    {
        $this->model = $projectPrerequisitePermit;
        $this->uploadableRepo = $uploadableRepo;
    }

    public function displayProjectPrereqPermits(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if($request->filled('search_key'))
        {
            $query = $query->where(function($q) use($request) {
                $q->where('name', 'LIKE', "%{$request->search_key}%");
            });
        }

        return $query->get();
    }

    public function saveFromProjectPermit($request, $projPermit)
    {
        foreach( $request as $key => $preReqPermit )
        {
            if( isset($preReqPermit['id']) )
            {
                $projPreReqPermit = $this->model->find($preReqPermit['id']);
            }
            else
            {
                $projPreReqPermit = new ProjectPrerequisitePermit;
            }

            if( isset($preReqPermit['delete']) )
            {
                if( isset($preReqPermit['id']) )
                {
                    foreach ($projPreReqPermit->uploads as $ups) {
                        $this->uploadableRepo->deleteThisUploadable($ups);
                        $ups->delete();
                    }
                    $projPreReqPermit->delete();
                }
            }
            else {
                $projPreReqPermit->project_permits_id = $projPermit->id;
                $projPreReqPermit->name = $preReqPermit['name'];
                $projPreReqPermit->status = isset($preReqPermit['status']) ? 1 : 0;

                if($preReqPermit['check_note'])
                {
                    $projPreReqPermit->notes = $preReqPermit['notes'];
                }
                else
                {
                    $projPreReqPermit->notes = null;
                }

                if($preReqPermit['date_filed'])
                {
                    $projPreReqPermit->date_filed = Carbon::parse($preReqPermit['date_filed'])->copy()->toDateString();
                }

                if($preReqPermit['date_acquired'])
                {
                    $projPreReqPermit->date_acquired = Carbon::parse($preReqPermit['date_acquired'])->copy()->toDateString();
                }

                if($preReqPermit['date_expiry'])
                {
                    $projPreReqPermit->date_expiry = Carbon::parse($preReqPermit['date_expiry'])->copy()->toDateString();
                }

                if($preReqPermit['date_due'])
                {
                    $projPreReqPermit->date_due = Carbon::parse($preReqPermit['date_due'])->copy()->toDateString();
                }

                $projPreReqPermit->save();

                if(isset($preReqPermit['uploads']))
                {
                    foreach($preReqPermit['uploads'] as $dataUpload)
                    {
                        $fileType = $dataUpload->getClientOriginalExtension();
                        
                        if($this->idType($fileType))
                        {
                            $this->uploadableRepo->customUpload(
                                $projPreReqPermit,
                                $dataUpload,
                                $this->idType($fileType),
                                [
                                    'key' => ProjectPrerequisitePermit::ATTACHMENT,
                                    'field_key' => null,
                                    'path' => 'uploads/project-prerequisite-permits',
                                    'filename' => $projPreReqPermit->name.$projPreReqPermit->id.'.'.$fileType,
                                    'multiple' => true
                                ]
                            );
                        }
                    }
                }

                // if( isset($preReqPermit['upload']) )
                // {
                //     $fileType = $preReqPermit['upload']->extension();
                //     $this->uploadableRepo->customUpload(
                //         $projPreReqPermit,
                //         $preReqPermit['upload'],
                //         $this->idType($fileType),
                //         [
                //             'key' => ProjectPrerequisitePermit::ATTACHMENT,
                //             'field_key' => null,
                //             'path' => 'uploads/project-prerequisite-permits',
                //             'filename' => $projPreReqPermit->name.'-'.$projPreReqPermit->id
                //         ]
                //     );
                // }
            }
        }
        return;
    }

    public function idType($extension)
    {
        $images = ['jpg', 'jpeg', 'png'];
        $files = ['txt', 'word', 'docx', 'doc', 'pdf', 'ppt', 'pptx', 'xls', 'xlsx', 'zip'];

        if (in_array($extension, $images)) {
            return 'image';
        } else if(in_array($extension, $files)) {
            return 'file';
        }
    }
}