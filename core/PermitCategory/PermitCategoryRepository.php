<?php

namespace PermitExpress\PermitCategory;

use Carbon\Carbon;
use Illuminate\Http\Request;
use PermitExpress\PermitCategory\PermitCategoryRepository;
use PermitExpress\BaseRepository;

class PermitCategoryRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        PermitCategory $permitCategory
    )
    {
        $this->model = $permitCategory;
    }

    public function displayPermitCategories(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        if($request->filled('search_key'))
        {
          $query = $query->where(function($q) use($request) {
            $q->where('name', 'LIKE', "%{$request->search_key}%");
          });
        }

        return $query->get();
    }

    public function linkedToPermit()
    {
        $isLinked = $this->model->has('permits')->find(1);

        return $isLinked;
    }

    public function getAll() {
        return $this->model->all();
    }

    public function createPermitCategory(Request $request)
    {
        $permitCategory = new PermitCategory;

        $this->validatePermitCategory($request, $permitCategory);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $permitCategory = $this->savePermitCategory($request, $permitCategory);

        return $permitCategory;
    }

    public function updatePermitCategory(Request $request, $permitCategory)
    {
        $this->validatePermitCategory($request, $permitCategory);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->savePermitCategory($request, $permitCategory);

        return;
    }

    public function deletePermitCategory($permitCategory)
    {
        $permitCategory->delete();
        return;
    }

    private function savePermitCategory($request, $permitCategory)
    {
        $permitCategory->name = $request->name;

        $permitCategory->save();

        return $permitCategory;
    }

    public function validatePermitCategory(Request $request, $model = NULL)
    {
        $rules = [];

        if($model->exists)
        {
            $rules['name'] = 'required|unique:permit_categories,name,' . $model->id;
        }
        else
        {
            $rules['name'] = 'required|unique:permit_categories,name';
        }

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

}