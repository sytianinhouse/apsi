<?php

namespace PermitExpress\PermitCategory;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\BaseModel;
use PermitExpress\Permit\Permit;

class PermitCategory extends BaseModel
{




    /**
     * Relations
     */
    public function permits()
    {
        return $this->hasMany(Permit::class, 'permit_category_id');
    }
}