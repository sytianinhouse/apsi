<?php

namespace PermitExpress\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PermitExpress\Client\Client;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public function isActive()
    {
        return $this->active ? true : false;
    }

    public function isSuperAdmin()
    {
        if( $this->client_id == NULL ) return true;
        return false;
    }

    public function isClient()
    {
        if( $this->client_id != NULL && !$this->client->is_master ) return true;
        return false;
    }

    public function isMasterClient()
    {
        if( $this->client_id != NULL && $this->client->is_master ) return true;
        return false;
    }

    /*
     * Scope Queries
     */
    public function scopeNotAuth($query)
    {
        return $query->where('id', '<>', Auth::id());
    }

    /**
     * mutators
     */
    public function getStatusAttribute()
    {
        return ($this->active) ? 'ACTIVE' : 'INACTIVE';
    }

    public function getStatusColorAttribute()
    {
        return ($this->active) ? 'status-green' : 'status-red';
    }

    /**
     * Relations
     */

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

}