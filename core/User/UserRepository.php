<?php

namespace PermitExpress\User;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PermitExpress\User\User;
use PermitExpress\User\UserRepository;
use PermitExpress\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        User $user
    )
    {
        $this->model = $user;
    }

    public function displayUsers(Request $request, $with = [])
    {
        $query = $this->model->with($with)->latest();

        $query->whereNull('client_id');

        if( $request->filled('except_auth') )
        {
            $query = $query->notAuth();
        }

        if($request->filled('search_key'))
        {
          $query = $query->where(function($q) use($request) {
            $q->where('first_name', 'LIKE', "%{$request->search_key}%")
                ->orWhere('last_name', 'LIKE', "%{$request->search_key}%");
          });
        }

        return $query->get();
    }

    public function createUser(Request $request)
    {
        $user = new User;

        $this->validateUserData($request, $user);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $user = $this->saveUser($request, $user);
    }

    public function updateUser(Request $request, $user)
    {
        $this->validateUserData($request, $user);

        if (count($this->errors) > 0) {
            throw new \PermitExpress\Validation\ValidationException($this->errors);
        }

        $this->saveUser($request, $user);

        return;
    }

    public function deleteUser($user)
    {
        $user->delete();
        return;
    }

    public function saveUser($request, $user)
    {
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->active = ($request->active) ? $request->active : 0;
        $user->client_id = null;

        if( $request->password )
        {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return $user;
    }

    public function saveFromClient( $userData )
    {
        $user = new User;

        if( !empty($userData) )
        {
            if( isset($userData['id']) )
            {
                $user = User::find($userData['id']);
            }

            foreach( $userData as $key => $data )
            {
                $user->$key = $data;
            }
        }

        $user->save();

        return $user;
    }

    public function validateUserData(Request $request, $model = NULL)
    {
        $rules = [];

        if( $model->exists )
        {
            $rules['email'] = 'required|unique:users,email,' . $model->id;
            $rules['username'] = 'required|unique:users,username,' . $model->id;
        }
        else
        {
            $rules['email'] = 'required|unique:users,email';
            $rules['username'] = 'required|unique:users,username';
        }

        if( !$model->exists || $request->filled('password') )
        {
            $rules['password'] = 'required|max:15|min:6|confirmed';
            $rules['password_confirmation'] = 'required';
        }

        $validator = \Validator::make($request->all(), $rules);

        if( $validator->fails() )
        {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }
}