<?php

namespace PermitExpress\StoredProcedure\Report;

use Carbon\Carbon;
use PermitExpress\StoredProcedure\BaseProcedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportProcedure extends BaseProcedure
{
    public function projectByCategory(Request $request)
    {

        $categoryId = null;
        if($request->filled('category'))
        {
            $categoryId = $request->category;
        }

        $dateStart = null;
        $dateEnd = null;
        if($request->filled('dateRange'))
        {
            $dateRange =  explode(' - ', $request->dateRange);

            $dateStart = Carbon::parse($dateRange[0])->copy()->toDateString();
            $dateEnd = Carbon::parse($dateRange[1])->copy()->toDateString();
        }

        $cityId = null;
        if($request->filled('city'))
        {
            $cityId = $request->city;
        }

        $rows = DB::select(
            'call projects_per_category (?, ?, ?, ?)',
            [
                $categoryId,
                $dateStart,
                $dateEnd,
                $cityId
            ]
        );

        $categories = collect($rows);

        return $categories;
    }

    public function projectsCreated(Request $request)
    {

        $categoryId = null;
        if($request->filled('category'))
        {
            $categoryId = $request->category;
        }

        $keyword = null;
        if($request->filled('keyword'))
        {
            $keyword = $request->keyword;
        }

        $cityId = null;
        if($request->filled('city'))
        {
            $cityId = $request->city;
        }

        $dateStart = null;
        $dateEnd = null;
        if($request->filled('dateRange'))
        {
            $dateRange =  explode(' - ', $request->dateRange);

            $dateStart = Carbon::parse($dateRange[0])->copy()->toDateString();
            $dateEnd = Carbon::parse($dateRange[1])->copy()->toDateString();
        }

        $status = null;
        if($request->filled('status'))
        {
            $status = $request->status;
        }

        $rows = DB::select(
            'call projects_created (?, ?, ?, ?, ?, ?)',
            [
                $categoryId,
                $keyword,
                $cityId,
                $dateStart,
                $dateEnd,
                $status
            ]
        );

        $projects = collect($rows);

        return $projects;
    }

    public function projectByCity(Request $request)
    {

        $categoryId = null;
        if($request->filled('category'))
        {
            $categoryId = $request->category;
        }

        $dateStart = null;
        $dateEnd = null;
        if($request->filled('dateRange'))
        {
            $dateRange =  explode(' - ', $request->dateRange);

            $dateStart = Carbon::parse($dateRange[0])->copy()->toDateString();
            $dateEnd = Carbon::parse($dateRange[1])->copy()->toDateString();
        }

        $cityId = null;
        if($request->filled('city'))
        {
            $cityId = $request->city;
        }

        $rows = DB::select(
            'call projects_per_city (?, ?, ?, ?)',
            [
                $categoryId,
                $dateStart,
                $dateEnd,
                $cityId
            ]
        );

        $cities = collect($rows);

        return $cities;
    }

    public function projectByStatus(Request $request)
    {
        $status = null;
        if($request->filled('status'))
        {
            $status = $request->status;
        }

        $dateStart = null;
        $dateEnd = null;
        if($request->filled('dateRange'))
        {
            $dateRange =  explode(' - ', $request->dateRange);

            $dateStart = Carbon::parse($dateRange[0])->copy()->toDateString();
            $dateEnd = Carbon::parse($dateRange[1])->copy()->toDateString();
        }

        $rows = DB::select(
            'call projects_per_status (?, ?, ?)',
            [
                $status,
                $dateStart,
                $dateEnd
            ]
        );

        $data = collect($rows);

        return $data;
    }
}