<form method="{{ $method }}" role="form" enctype="multipart/form-data" class="col-12">
    <div class="row">
        <div class="col-12 col-md-3">
            <input type="text" class="form-control" name="search_key" value="{{ Request()->search_key }}" placeholder="Search Client" autocomplete="off" />
        </div>
        <div class="col-12 col-md-3">
            <select class="form-control chzn-select" name="permit" tabindex="2">
                <option selected value="">Show All</option>
                @forelse( $permits as $key => $permit )
                    <option value="{{ $permit->id }}" {{ (Request()->permit == $permit->id) ? 'selected' : '' }}>{{ $permit->name }}</option>
                @empty
                @endforelse
            </select>
        </div>
        <div class="col-12 col-md-2 {{(Request()->date_type == "") ? 'col-md-2' : 'col-md-5'}}" id="master-main-dr">
            <ul class="mc-date-range">
                <li class="left {{(Request()->date_type == "") ?: 'show'}}" id="master-dr-left">
                    <select name="date_type" id="master_date_range" class="form-control">
                        <option value="">Select Date</option>
                        @if( $dateTypes )
                            @foreach( $dateTypes as $key => $dateType )
                                <option value="{{ $key }}" {{ ($key != Request()->date_type) ?: 'selected' }}>{{ $dateType }}</option>
                            @endforeach
                        @endif
                    </select>
                </li>
                <li class="right {{(Request()->date_type != "") ?: 'd-none'}}" id="master-dr-wrap">
                     <input type="text" name="date_range" id="dateRange" class="form-control" value="{{ Request()->date_range }}" autocomplete="off">
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-1">
            <button class="btn btn-primary form-control" type="submit">
                Filter
            </button>
        </div>
    </div>
</form>