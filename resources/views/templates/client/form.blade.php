@inject('clientRepo', 'PermitExpress\Client\ClientRepository')

<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="card">
                <div class="card-header dark-blue">
                    {{ $cardLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 input_field_sections">
                            <h5>Name <span class="required">*</span>
                                @if($errors->has('name'))
                                    <span class="error-notif">{{ $errors->first('name') }}</span>
                                @endif
                            </h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control {{ ($errors->has('name')) ? 'error' : '' }}" name="name" value="{{ old('name') ?: $client->name }}" required />
                            </div>
                            
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Address</h5>
                            <textarea id="text4" class="form-control" name="address" cols="50" rows="2">{{ old('address') ?: $client->address }}</textarea>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Type <span class="required d-none">*</span>
                                @if($errors->has('type'))
                                    <span class="error-notif">{{ $errors->first('type') }}</span>
                                @endif
                            </h5>
                            <input type="text" class="form-control {{ ($errors->has('type')) ? 'error' : '' }}" name="type" value="{{ old('type') ?: $client->type }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    {{ $contactLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>First Name <span class="required">*</span></h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" name="cp_first_name" value="{{ old('cp_first_name') ?: $client->cp_first_name }}" required/>
                            </div>
                            
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Last Name <span class="required d-none">*</span></h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" name="cp_last_name" value="{{ old('cp_last_name') ?: $client->cp_last_name }}"/>
                            </div>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Mobile #</h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-mobile"></i>
                                </span>
                                <input type="text" class="form-control mobile-phone" name="cp_number" value="{{ old('cp_number') ?: $client->cp_number }}" />
                            </div>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Secondary Contact #</h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-phone"></i>
                                </span>
                                <input type="text" class="form-control" name="cp_number_2" value="{{ old('cp_number_2') ?: $client->cp_number_2 }}"/>
                            </div>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Primary Email <span class="required">*</span>
                                @if($errors->has('email'))
                                    <span class="error-notif">{{ $errors->first('email') }}</span>
                                @endif
                            </h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="email" class="form-control {{ ($errors->has('email')) ? 'error' : '' }}" name="email" value="{{ old('email') ?: $client->email }}" required />
                            </div>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Secondary Email <span class="required d-none">*</span>
                                @if($errors->has('second_email'))
                                    <span class="error-notif">{{ $errors->first('second_email') }}</span>
                                @endif
                            </h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="email" class="form-control {{ ($errors->has('second_email')) ? 'error' : '' }}" name="second_email" value="{{ old('second_email') ?: $client->second_email }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    {{ $credsLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 input_field_sections">
                            <h5>Username <span class="required">*</span>
                                @if($errors->has('username'))
                                    <span class="error-notif">{{ $errors->first('username') }}</span>
                                @endif
                            </h5>
                            @php
                                $uname = isset($client->user->username) ? $client->user->username : '';
                            @endphp
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control {{ ($errors->has('username')) ? 'error' : '' }}" name="username" value="{{ old('username') ?: $uname }}" required/>
                            </div>
                        </div>
                        <div class="col-4 input_field_sections">
                            <h5>Password
                                @if( !isset($client->user) )
                                    <span class="required">*</span>
                                @endif
                                @if($errors->has('password'))
                                    <span class="error-notif">{{ $errors->first('password') }}</span>
                                @endif
                            </h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-lock"></i>
                                </span>
                                <input type="password" class="form-control {{ ($errors->has('password')) ? 'error' : '' }}" name="password" value="{{ old('password') }}" {{ isset($client->user) ?: 'required' }} />
                            </div>
                        </div>
                        <div class="col-4 input_field_sections">
                            <h5>Confirm Password
                                @if( !isset($client->user) )
                                    <span class="required">*</span>
                                @endif
                                @if($errors->has('password'))
                                    <span class="error-notif">{{ $errors->first('password') }}</span>
                                @endif
                            </h5>
                            <div class="input-group input-group-prepend">
                                <span class="input-group-text border-right-0 rounded-left">
                                    <i class="fa fa-lock"></i>
                                </span>
                                <input type="password" class="form-control {{ ($errors->has('password_confirmation')) ? 'error' : '' }}" name="password_confirmation" value="{{ old('password_confirmation') }}" {{ isset($client->user) ?: 'required' }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 p-l-0">
            <div class="card" id="sticky" style="margin-bottom: 15px;">
                <div class="card-header dark-blue">
                    Actions
                </div>
                <div class="card-body">
                    <div class="checkbox">
                        <div class="col-lg-6 px-0">
                            <div class="input-group">
                                <label for="active" class="custom-control custom-checkbox">
                                    @php
                                        $checkActive = isset($client->user->active) && ( $client->user->active == 1 ) ? 'checked' : '';
                                    @endphp
                                    <input type="checkbox" id="active" name="active" class="custom-control-input" value=1 {{ (!$client->exists) ? 'checked' :$checkActive }}>
                                    <span class="custom-control-label"></span>
                                    <h5 class="custom-control-description">Active</h5>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 px-0">
                            <p class="notice {{($client->projects->count() > 0) ? 'd-block' : 'd-none'}}">Can't convert to Master because this account already has a Project</p>
                            <div class="input-group {{($client->projects->count() > 0) ? 'd-none' : 'd-block'}}">
                                <label for="master" class="custom-control custom-checkbox">
                                    @php
                                        $isMaster = '';
                                        if( $client->exists )
                                        {
                                            $isMaster = ( $client->is_master != 1 ) ?: 'checked';
                                            if(old('is_master'))
                                            {
                                                $isMaster = 'checked';
                                            }
                                        }
                                        else
                                        {
                                            $isMaster = !(old('is_master')) ?: 'checked';
                                        }
                                    @endphp
                                    <input type="checkbox" id="master" name="is_master" class="custom-control-input" value=1 {{ $isMaster }}>
                                    <span class="custom-control-label"></span>
                                    <h5 class="custom-control-description">Master</h5>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="all-clients {{ ($client->is_master == 1) ? 'd-block' : 'd-none' }} {{(old('is_master')) ? 'd-block' : 'd-none'}} " id="all-clients">
                        <div class="col-lg input_field_sections multi-client px-0">
                            <h5>Clients</h5>
                            @if( $client->exists )
                                <select size="3" multiple class="form-control chzn-select"
                                        id="select-clients"
                                        name="clients[]"
                                        tabindex="8"
                                        data-url="../client/ajax"
                                        {{ ($client->is_master != 1) ?: 'required'}}>
                                    @foreach( $tmpClients as $ckey => $tmpClient )
                                        <option value="{{ $tmpClient['id'] }}" {{ ($tmpClient['selected']) ? 'selected' : '' }}>{{ $tmpClient['name'] }}</option>
                                    @endforeach
                                </select>
                            @else
                                <select size="3" multiple class="form-control chzn-select"
                                        id="select-clients"
                                        name="clients[]"
                                        tabindex="8"
                                        data-url="client/ajax">
                                    @forelse( $clients as $ckey => $tmpClient )
                                        @if(old('clients'))
                                            @php
                                                $oldBool = $clientRepo->selectedOldClient(old('clients'), $tmpClient);
                                            @endphp
                                            <option value="{{ $tmpClient->id }}" {{ (!$oldBool) ?: 'selected' }} >{{ $tmpClient->name }}</option>
                                        @else
                                            <option value="{{ $tmpClient->id }}">{{ $tmpClient->name }}</option>
                                        @endif
                                    @empty
                                    @endforelse
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="all-projects {{ ($client->is_master == 1 && $client->clientMasterProjects->count() > 0) ? '' : 'd-none' }}" id="all-projects">
                        <div class="col-lg input_field_sections multi-client px-0">
                            <h5>Projects</h5>
                            @if( $client->exists && isset($client->clientMasterProjects) )
                                <select size="3" multiple class="form-control chzn-select" id="client-projects"
                                        name="projects[]" tabindex="8">
                                    @forelse( $tmpProjects as $cmKey => $tmpProject )
                                        <optgroup label="{{ $tmpProject['client']['name'] }}" data-id="{{ $tmpProject['client']['id'] }}">
                                        @if($tmpProject['projects'])
                                            @foreach($tmpProject['projects'] as $tmpData)
                                                @php
                                                    $tmpArr = array('client_id' => $tmpProject['client']['id'],
                                                                    'project_id' => $tmpData['id']
                                                                    );
                                                    $tmpArr = json_encode($tmpArr);
                                                @endphp
                                                <option value="{{ $tmpArr }}" {{ !isset($tmpData['selected']) ?: 'selected' }}>{{ $tmpData['name'] }}</option>
                                            @endforeach
                                        @endif
                                    @empty
                                    @endforelse
                                </select>
                            @else
                                <select size="3" multiple class="form-control chzn-select" id="client-projects"
                                        name="projects[]" tabindex="8">
                                </select>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                    <button class="btn btn-success form-control" type="submit">
                        <i class="fa fa-save"></i> &nbsp;
                        {{ $actionLabel }}
                    </button>
                </div>
            </div>
            <div class="card" id="sticky" style="margin-bottom:10px;">
                <div class="card-header dark-blue">
                    Additional Contact #
                </div>
                <div class="card-body">
                    <div class="ac-wrapper" id="additional-contacts">
                        @if( isset($client->additional_contacts) )
                            @if( old('additional_contacts') )
                                @foreach( old('additional_contacts') as $oldkey => $oldAC )
                                    <div class="ac-wrap">
                                        <ul>
                                            <li class="left">
                                                <input type="text" class="form-control" name="additional_contacts[]" value="{{ old('additional_contacts')[$oldkey] }}" placeholder="Contact Number" required/>
                                            </li>
                                            <li class="right">
                                                <a href="#" class="btn btn-danger form-control">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @else
                                @foreach( $client->additional_contacts as $key => $additionalContact )
                                    <div class="ac-wrap">
                                        <ul>
                                            <li class="left">
                                                <input type="text" class="form-control" name="additional_contacts[]" value="{{ $additionalContact }}" placeholder="Contact Number" required/>
                                            </li>
                                            <li class="right">
                                                <a href="#" class="btn btn-danger form-control">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @endif
                        @else
                           @if( old('additional_contacts') )
                                @foreach( old('additional_contacts') as $oldkey => $oldAC )
                                    <div class="ac-wrap">
                                        <ul>
                                            <li class="left">
                                                <input type="text" class="form-control" name="additional_contacts[]" value="{{ old('additional_contacts')[$oldkey] }}" placeholder="Contact Number" required />
                                            </li>
                                            <li class="right">
                                                <a href="#" class="btn btn-danger form-control">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @else
                                <div class="ac-wrap">
                                    <ul>
                                        <li class="left">
                                            <input type="text" class="form-control" name="additional_contacts[]" value="" placeholder="Contact Number" required/>
                                        </li>
                                        <li class="right">
                                            <a href="#" class="btn btn-danger form-control">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary form-control" id="ac-button">
                        <i class="fa fa-plus"></i> &nbsp;
                        Add Contact
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
