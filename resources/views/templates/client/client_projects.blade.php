@if(isset($clients))
    @forelse( $clients as $client )
        <optgroup label="{{ $client->name }}" data-id="{{ $client->id }}">
            @if(isset($projects))
                @forelse( $projects as $key => $project )
                    @php
                        $tmpArr = array('client_id' => $client->id,
                                        'project_id' => $project->id
                                        );
                        $tmpArr = json_encode($tmpArr);
                    @endphp
                    <option value="{{ $tmpArr }}">{{ $project->name }}</option>
                @empty
                @endforelse
            @endif
        </optgroup>
    @empty
    @endforelse
@endif