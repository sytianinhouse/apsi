<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    <div class="filter-wrap">
        <div class="left">
            <input type="text" class="form-control" name="search_key" value="{{ Request()->search_key }}" placeholder="Search here ..."  />
        </div>
        <div class="right">
            <button class="btn btn-primary form-control" type="submit">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</form>