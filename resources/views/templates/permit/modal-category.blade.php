<div id="permit-category-modal" class="modal permit-modal" role="dialog" aria-labelledby="modalLabel"
             aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.permit_categories.quick-create') }}" method="POST" role="form" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <ul class="mh-wrap">
                        <li class="left">
                            <h4 class="modal-title" id="modalLabel">Add Category</h4>
                        </li>
                        <li class="right">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <h5>Name <span class="required">*</span></h5>
                        <input type="text" class="form-control" name="name" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button 
                        type="submit"
                        class="btn btn-success">
                        SUBMIT
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>