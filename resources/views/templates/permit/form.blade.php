<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="card">
                <div class="card-header dark-blue">
                    {{ $cardLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>Name <span class="required">*</span></h5>
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?: $permit->name }}" required />
                        </div>
                        <div class="col-6 input_field_sections">
                            <ul class="permit-cat">
                                <li class="left">
                                    <h5>Permit Category <span class="required">*</span></h5>
                                    <select class="form-control chzn-select" name="permit_category_id" tabindex="2" required>
                                        <option value="" disabled selected>Select Category</option>
                                        @forelse( $permitCategories as $permitCategory )
                                            <option value="{{ $permitCategory->id }}" {{ ($permit->permit_category_id == $permitCategory->id) ? 'selected' : '' }}>{{ $permitCategory->name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </li>
                                <li class="right">
                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#permit-category-modal"><i class="fa fa-plus"></i> ADD</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-t-30">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header dark-blue">
                                {{ $reqLabel }}
                            </div>
                            <div class="card-body">
                                <div class="req-main-wrapper">
                                    <div class="row">
                                        @if( $permit->exists && $permit->prerequisitePermits )
                                            @foreach( $permit->prerequisitePermits as $key => $preReq )
                                                <div class="col-12 input_field_sections">
                                                    <h5>Name</h5>
                                                    <div class="req-wrap">
                                                        <div class="wrap left">
                                                            <input type="hidden" class="form-control" name="pre_permits[{{ $key }}][id]" value="{{ $preReq->id }}" />
                                                            <input type="text" class="form-control" name="pre_permits[{{ $key }}][name]" value="{{ $preReq->name }}" />
                                                        </div>
                                                        <div class="wrap right">
                                                            <a href="#" class="btn btn-danger form-control" data-id="{{ $key }}">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-12 input_field_sections">
                                                <h5>Name</h5>
                                                <div class="req-wrap">
                                                    <div class="wrap left">
                                                        <input type="text" class="form-control" name="pre_permits[0][name]" value="" />
                                                    </div>
                                                    <div class="wrap right">
                                                        <a href="#" class="btn btn-danger form-control" data-id="0">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="add-req-wrap">
                                    <a href="#" class="btn btn-success form-control" id="add-req">
                                        <i class="fa fa-plus"></i> ADD PRE-REQUISITE
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 p-l-0">
            <div class="card" id="sticky-sidebar" style="margin-bottom:10px;">
                <div class="card-header dark-blue">
                    Actions
                </div>
                <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                    <button class="btn btn-success form-control" type="submit">
                        <i class="fa fa-save"></i> &nbsp;
                        {{ $actionLabel }}
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

@include('templates.permit.modal-category')
