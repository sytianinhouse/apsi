<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    <div class="proj-created-filter">
        <div class="pcf-wrap second">
            <select name="status" id="" class="form-control chzn-select">
                <option value="" {{ (Request()->status == "") ? 'selected' : '' }}>Show All</option>
                @foreach(PermitExpress\Permit\Permit::$statuses as $keys => $status)
                    <option value="{{ $keys }}" {{ ($keys == Request()->status) ? 'selected' : '' }}>{{ $status }}</option>
                @endforeach
            </select>
        </div>
        <div class="pcf-wrap third">
            <input type="text" name="dateRange" id="dateRange" class="form-control" value="{{ Request()->dateRange }}" placeholder="Date Range" autocomplete="off">
        </div>
        <div class="pcf-wrap fourth">
            <button class="btn btn-primary form-control" type="submit">
                Filter
            </button>
        </div>
    </div>
</form>