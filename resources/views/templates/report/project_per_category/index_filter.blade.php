<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    <div class="proj-created-filter">
        <div class="pcf-wrap second">
            <select class="form-control chzn-select" name="city" tabindex="2">
                <option selected value="">Show All City</option>
                @forelse($cities as $city)
                    <option value="{{ $city->id }}" {{ (Request()->city == $city->id) ? 'selected' : '' }}>{{ $city->name }}</option>
                @empty
                @endforelse
            </select>
        </div>
        <div class="pcf-wrap third">
            <input type="text" name="dateRange" id="dateRange" class="form-control" value="{{ Request()->dateRange }}" placeholder="Date Range" autocomplete="off">
        </div>
        <div class="pcf-wrap fourth">
            <button class="btn btn-primary form-control" type="submit">
                Filter
            </button>
        </div>
    </div>
</form>