@if( isset($permits) && !empty($permits) )
    @foreach( $permits as $key => $permit )
        @php
            $key = $key + $permitCount
        @endphp
        <div class="permit-content-wrapper" data-id="{{$permit->id}}" id="perm-{{$permit->id . $key}}" data-count="{{ $key }}">
            <input type="hidden" name="Permit[{{$key}}][permit_id]" value="{{$permit->id}}">
            <input type="hidden" name="Permit[{{$key}}][name]" value="{{$permit->name}}">
            <div class="parent-permit">
                <a href="#" class="close x-parent" data-key="{{$permit->id . $key}}"><i class="fa fa-times"></i></a>
                <ul class="permit-head">
                    <li><h4>{{ $permit->name }}</h4></li>
                    <li id="permit-view-file-{{$permit->id . $key}}">
                        <a href="#" 
                            class="btn btn-primary btn-vfile"
                            data-toggle="modal" 
                            data-target="#permit-file-modal-{{$permit->id . $key}}">
                            <i class="fa fa-paperclip"></i> FILE ATTACHMENTS
                        </a>
                    </li>
                    @if( $permitCount > 0 )
                        {{-- <li><a href="#" class="btn btn-success">RENEW</a></li> --}}
                        {{-- <li><a href="#" class="btn btn-success">SEND NOTIFICATION</a></li> --}}
                    @endif
                </ul>
                <ul class="other-field">
                    <li class="of-wrap">
                        <h5>Name</h5>
                        <input type="text" class="form-control" name="Permit[{{$key}}][branch_name]" value="{{ old('branch_name') ?: $permit->branch_name }}" />
                    </li>
                    <li class="of-wrap">
                        <h5>City <span class="required">*</span></h5>
                        <select class="form-control chzn-select" name="Permit[{{$key}}][city]" tabindex="2" autocomplete="off" required>
                            <option value="" disabled selected>Select City</option>
                            @forelse( $cities as $city )
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @empty
                            @endforelse
                        </select>
                    </li>
                    <li class="of-wrap">
                        <h5>Status</h5>
                        <select name="Permit[{{$key}}][status]" id="" class="form-control">
                            @foreach(PermitExpress\Permit\Permit::$statuses as $keys => $status)
                                <option value="{{ $keys }}">{{ $status }}</option>
                            @endforeach
                        </select>
                    </li>
                </ul>
                <ul class="permit-fields">
                    <li>
                        <h5>Due Date</h5>
                        <input type="text" name="Permit[{{$key}}][date_due]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                           data-date-format="mm/dd/yyyy" autocomplete="off">
                    </li>
                    <li>
                        <h5>Date Filed</h5>
                        <input type="text" name="Permit[{{$key}}][date_filed]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                           data-date-format="mm/dd/yyyy" autocomplete="off">
                    </li>
                    <li>
                        <h5>Date Acquired</h5>
                        <input type="text" name="Permit[{{$key}}][date_acquired]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                           data-date-format="mm/dd/yyyy" autocomplete="off">
                    </li>
                    <li>
                        <h5>Date Expiry</h5>
                        <input type="text" name="Permit[{{$key}}][date_expiry]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                           data-date-format="mm/dd/yyyy" autocomplete="off">
                    </li>
                </ul>

                {{-- PERMIT FILE MODAL | START --}}
                <div id="permit-file-modal-{{$permit->id . $key}}" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <ul class="mh-wrap">
                                    <li class="left">
                                        <h4 class="modal-title">File Attachments</h4>
                                    </li>
                                    <li class="right">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal-body">
                                <div class="permit-file-wrapper" id="permit-file-{{$permit->id . $key}}">
                                    <a href="#" class="file-close d-none"><i class="fa fa-times"></i></a>
                                    <input  name="Permit[{{$key}}][uploads][]" type="file" multiple class="file-loading mult-permit-file" data-id="{{$permit->id . $key}}">

                                    <div class="existing-file-wrapper d-none" id="existing-{{$permit->id . $key}}"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button 
                                    type="button"
                                    class="btn btn-success"
                                    data-dismiss="modal">
                                    CONFIRM
                                </button>
                                <button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CANCEL</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- PERMIT FILE MODAL | END --}}

                <div class="add-notes-wrap">
                    <div class="notes-btn-wrap">
                        <input type="hidden" id="chcknote-{{$permit->id . $key}}" name="Permit[{{$key}}][check_note]" value="0">
                        <a href="#" class="btn-permit-notes" data-id="{{$permit->id . $key}}"><i class="fa fa-plus"></i> ADD NOTES</a>
                    </div>
                    <div class="notes-wrap d-none" id="pnote-{{$permit->id . $key}}">
                        <h5>Notes</h5>
                        <textarea class="form-control" name="Permit[{{$key}}][notes]" cols="50" rows="5"></textarea>
                    </div>
                </div>
            </div>

            <div class="prereq-permit" id="pre-permit-{{$key}}">
                @forelse($permit->prerequisitePermits as $prekey => $pre_permit)
                    <div class="prereq-wrap">
                        <a href="#" class="close x-child" data-pkey="{{$key}}" data-prekey="{{$prekey}}"><i class="fa fa-times"></i></a>
                        <div class="upper-fields">
                            <div class="uf-wrap left">
                                <h5>Title</h5>
                                <input type="text" class="form-control" name="Permit[{{$key}}][Pre][{{$prekey}}][name]" value="{{ $pre_permit->name }}" />
                            </div>
                            <div class="uf-wrap right">
                                <ul class="btn-wrap">
                                    <li id="pre-view-file-{{$key . $prekey}}" class="middle">
                                        <a href="#" class="btn btn-primary btn-vfile" data-toggle="modal" data-target="#prereq-file-modal-{{$key . $prekey}}"><i class="fa fa-paperclip"></i> FILE ATTACHMENTS</a>
                                    </li>
                                    <li class="right">
                                        <h5>Status</h5>
                                        <input type="checkbox" name="Permit[{{$key}}][Pre][{{$prekey}}][status]" class="make-switch-radio" data-on-text="OPEN" data-off-text="CLOSE" data-on-color="primary" data-off-color="danger" checked />
                                        {{-- <span class="radio_switchery_padding">Status</span> --}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <ul class="date-fields">
                            <li>
                                <h5>Due Date</h5>
                                <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_due]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                   data-date-format="mm/dd/yyyy" autocomplete="off">
                            </li>
                            <li>
                                <h5>Date Filed</h5>
                                <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_filed]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                   data-date-format="mm/dd/yyyy" autocomplete="off">
                            </li>
                            <li>
                                <h5>Date Acquired</h5>
                                <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_acquired]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                   data-date-format="mm/dd/yyyy" autocomplete="off">
                            </li>
                            <li>
                                <h5>Date Expiry</h5>
                                <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_expiry]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                   data-date-format="mm/dd/yyyy" autocomplete="off">
                            </li>
                        </ul>
                        {{-- PRE-REQUISITE PERMIT FILE MODAL | START --}}
                        <div id="prereq-file-modal-{{$key . $prekey}}" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel"
                                     aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <ul class="mh-wrap">
                                            <li class="left">
                                                <h4 class="modal-title">File Attachments</h4>
                                            </li>
                                            <li class="right">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="modal-body">
                                        <div class="permit-file-wrapper" id="permit-file-{{$key . $prekey}}">
                                            <a href="#" class="file-close d-none"><i class="fa fa-times"></i></a>
                                            <input  name="Permit[{{$key}}][Pre][{{$prekey}}][uploads][]" type="file" multiple class="file-loading mult-permit-file" data-id="{{'prereq-' . $key . $prekey}}">

                                            <div class="existing-file-wrapper d-none" id="existing-{{'prereq-' . $key . $prekey}}"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success" data-dismiss="modal">CONFIRM</button>
                                        <button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CANCEL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- PRE-REQUISITE PERMIT FILE MODAL | END --}}
                        <div class="add-notes-wrap padding-top">
                            <div class="notes-btn-wrap">
                                <input type="hidden" id="prechcknote-{{$key . $prekey}}" name="Permit[{{$key}}][Pre][{{$prekey}}][check_note]" value="0">
                                <a href="#" class="btn-prereq-notes" data-id="{{$key . $prekey}}"><i class="fa fa-plus"></i> ADD NOTES</a>
                            </div>
                            <div class="notes-wrap d-none" id="prenote-{{$key . $prekey}}">
                                <h5>Notes</h5>
                                <textarea class="form-control" name="Permit[{{$key}}][Pre][{{$prekey}}][notes]" cols="50" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                @empty
                @endforelse

            </div>
            <div class="add-prereq-btn">
                <a href="#" class="btn btn-success" data-count={{$key}}><i class="fa fa-plus"></i> ADD PRE-REQUISITE</a>
            </div>
        </div>
    @endforeach
@endif

