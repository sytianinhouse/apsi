<div id="permit-modal" class="modal permit-modal" role="dialog" aria-labelledby="modalLabel"
             aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="mh-wrap">
                    <li class="left">
                        <h4 class="modal-title" id="modalLabel"></h4>
                    </li>
                    <li class="right">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="p-wrap left">
                    <h5>Permits <span class="required">*</span> <span class="required d-none" id="slctError" style="font-size: 13px;"> This field is required</span></h5>
                    <select size="3" multiple class="form-control chzn-select"
                            name="test_me_form[]" tabindex="8" id="select-permit" required>
                        @forelse( $permits as $tmpPermit )
                            <option value="{{ $tmpPermit->id }}">{{ $tmpPermit->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button 
                    id="add-permit"
                    type="button"
                    class="btn btn-success"
                    data-url="{{ ($project->exists) ? '../ajax' : (isset($client_id)) ? '../ajax' : 'ajax' }}">
                    CONFIRM
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>