<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    <div class="project-filter-wrap">
        <div class="pf-wrap left">
            <input type="text" class="form-control" name="search_key" value="{{ Request()->search_key }}" placeholder="Search" autocomplete="off" />
        </div>
        <div class="pf-wrap middle">
            <input type="text" name="date_range" id="dateRange" class="form-control" value="{{ Request()->date_range }}" placeholder="Date Range" autocomplete="off">
        </div>
        <div class="pf-wrap right">
            <button class="btn btn-primary form-control" type="submit">
                Filter
            </button>
        </div>
    </div>
</form>