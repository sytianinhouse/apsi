<form method="{{ $method }}" role="form" enctype="multipart/form-data" id="project-form">
    @csrf
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="card">
                <div class="card-header dark-blue">
                    {{ $cardLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>Client <span class="required">*</span></h5>
                            <select class="form-control chzn-select" name="client" tabindex="2" required>
                                <option value="" disabled selected>Select Client</option>
                                @forelse( $clients as $client )
                                    <option value="{{ $client->id }}"
                                        {{ (old('client') ==  $client->id) ? 'selected' : ($project->client_id == $client->id) ? 'selected' : (isset($client_id) && $client_id == $client->id ) ? 'selected' : '' }}>
                                        {{ $client->name }}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Project Name <span class="required">*</span></h5>
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?: $project->name }}" required />
                        </div>
                        <div class="col-4 input_field_sections">
                            <h5>City <span class="required">*</span></h5>
                            <select class="form-control chzn-select" name="project_city" tabindex="2" required>
                                <option value="" disabled selected>Select City</option>
                                @forelse( $cities as $city )
                                    <option value="{{ $city->id }}"
                                        {{ (old('project_city') ==  $city->id) ? 'selected' : ($project->city_id == $city->id) ? 'selected' : '' }}>
                                        {{ $city->name }}
                                    </option>
                                @empty
                                @endforelse
                            </select>
                        </div>
                        <div class="col-4 input_field_sections">
                            <h5>Barangay</h5>
                            <input type="text" class="form-control" name="barangay" value="{{ old('barangay') ?: $project->barangay }}" />
                        </div>
                        <div class="col-4 input_field_sections">
                            <h5>Type</h5>
                            <input type="text" class="form-control" name="project_type" value="{{ old('project_type') ?: $project->type }}" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    {{ $permitLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 m-t-15">
                            <a href="#" class="btn btn-success btn-add-permit" data-toggle="modal" data-target="#permit-modal"><i class="fa fa-plus"></i> ADD PERMIT</a>
                        </div>
                        <div class="col-12 m-t-15" id="display-permits">
                            @if( $project->exists )
                                <input type="hidden" id="current-permit-count" value="{{ $project->projectPermits->count() }}">
                                @forelse( $project->projectPermits as $key => $permit )
                                    <input type="hidden" name="Permit[{{$key}}][id]" value="{{$permit->id}}">
                                    <div class="permit-content-wrapper" data-id="{{$permit->permit_id}}" id="perm-{{$permit->permit_id . $key}}" data-count="{{ $key }}">
                                        <input type="hidden" name="Permit[{{$key}}][permit_id]" value="{{$permit->permit_id}}">
                                        <input type="hidden" name="Permit[{{$key}}][name]" value="{{$permit->name}}">
                                        <div class="parent-permit">
                                            <a href="#" class="close x-parent" data-key="{{$permit->permit_id . $key}}"><i class="fa fa-times"></i></a>
                                            <ul class="permit-head">
                                                <li><h4>{{ $permit->name }}</h4></li>
                                                <li id="permit-view-file-{{$permit->permit_id . $key}}">
                                                    <a href="#" 
                                                        class="btn btn-primary btn-vfile" 
                                                        data-toggle="modal" 
                                                        data-target="#permit-file-modal-{{$permit->permit_id . $key}}"
                                                        >
                                                        <i class="fa fa-paperclip"></i> FILE ATTACHMENTS
                                                    </a>
                                                </li>
                                                {{-- <li><a href="#" class="btn btn-success">RENEW</a></li> --}}
                                                <li><a href="#" class="btn btn-success btn-sendnotif" data-permit="{{$permit->id}}" data-client="{{$project->client_id}}"><i class="fa fa-envelope"></i> SEND VIA EMAIL</a></li>
                                            </ul>

                                            <ul class="other-field">
                                                <li class="of-wrap">
                                                    <h5>Name</h5>
                                                    <input type="text" class="form-control" name="Permit[{{$key}}][branch_name]" value="{{ old('branch_name') ?: $permit->branch_name }}" />
                                                </li>
                                                <li class="of-wrap">
                                                    <h5>City <span class="required">*</span></h5>
                                                    <select class="form-control chzn-select" name="Permit[{{$key}}][city]" tabindex="2" autocomplete="off" required>
                                                        <option value="" disabled selected>Select City</option>
                                                        @forelse( $cities as $city )
                                                            <option value="{{ $city->id }}" {{ ($city->id == $permit->city_id) ? 'selected' : '' }}>{{ $city->name }}</option>
                                                        @empty
                                                        @endforelse
                                                    </select>
                                                </li>
                                                <li class="of-wrap">
                                                    <h5>Status</h5>
                                                    <select name="Permit[{{$key}}][status]" id="" class="form-control">
                                                        @foreach(PermitExpress\Permit\Permit::$statuses as $keys => $status)
                                                            <option value="{{ $keys }}" {{ ($keys == $permit->status) ? 'selected' : '' }}>{{ $status }}</option>
                                                        @endforeach
                                                    </select>
                                                </li>
                                            </ul>

                                            <ul class="permit-fields">
                                                <li>
                                                    <h5>Due Date</h5>
                                                    <input type="text" name="Permit[{{$key}}][date_due]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                    data-date-format="mm/dd/yyyy"
                                                    value="{{ !($permit->date_due) ? '' : date('m/d/Y', strtotime($permit->date_due)) }}" autocomplete="off">
                                                </li>
                                                <li>
                                                    <h5>Date Filed</h5>
                                                    <input type="text" name="Permit[{{$key}}][date_filed]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                    data-date-format="mm/dd/yyyy"
                                                    value="{{ !($permit->date_filed) ? '' : date('m/d/Y', strtotime($permit->date_filed)) }}" autocomplete="off">
                                                </li>
                                                <li>
                                                    <h5>Date Acquired</h5>
                                                    <input type="text" name="Permit[{{$key}}][date_acquired]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                    data-date-format="mm/dd/yyyy"
                                                    value="{{ !($permit->date_acquired) ? '' : date('m/d/Y', strtotime($permit->date_acquired)) }}" autocomplete="off">
                                                </li>
                                                <li>
                                                    <h5>Date Expiry</h5>
                                                    <input type="text" name="Permit[{{$key}}][date_expiry]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                    data-date-format="mm/dd/yyyy"
                                                    value="{{ !($permit->date_expiry) ? '' : date('m/d/Y', strtotime($permit->date_expiry)) }}" autocomplete="off">
                                                </li>
                                            </ul>

                                            {{-- PERMIT FILE MODAL | START --}}
                                            <div id="permit-file-modal-{{$permit->permit_id . $key}}" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel"
                                                         aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <ul class="mh-wrap">
                                                                <li class="left">
                                                                    <h4 class="modal-title">File Attachments</h4>
                                                                </li>
                                                                <li class="right">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="permit-file-wrapper" id="permit-file-{{$permit->permit_id . $key}}">
                                                                <a href="#" class="file-close d-none"><i class="fa fa-times"></i></a>
                                                                <input  name="Permit[{{$key}}][uploads][]" type="file" multiple class="file-loading mult-permit-file" data-id="{{$permit->permit_id . $key}}">

                                                                <div class="existing-file-wrapper d-none" id="existing-{{$permit->permit_id . $key}}">
                                                                    @forelse($permit->uploads as $pUpload)
                                                                        @if($pUpload->key == 'permit-attachment')
                                                                            <input type="hidden" value="{{asset($pUpload->path)}}" class="exists-path" data-id="{{$pUpload->id}}">
                                                                        @endif
                                                                    @empty
                                                                    @endforelse
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button 
                                                                type="button"
                                                                class="btn btn-success"
                                                                data-dismiss="modal">
                                                                CONFIRM
                                                            </button>
                                                            <button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CANCEL</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- PERMIT FILE MODAL | END --}}

                                            <div class="add-notes-wrap">
                                                <div class="notes-btn-wrap">
                                                    <input type="hidden" id="chcknote-{{$permit->permit_id . $key}}" name="Permit[{{$key}}][check_note]" value="{{ ($permit->notes) ? 1 : 0 }}">
                                                    <a href="#" class="btn-permit-notes {{ !($permit->notes) ?: 'active' }}" data-id="{{$permit->permit_id . $key}}">
                                                        @if($permit->notes)
                                                            <i class="fa fa-minus"></i> REMOVE NOTES
                                                        @else
                                                            <i class="fa fa-plus"></i> ADD NOTES
                                                        @endif
                                                    </a>
                                                </div>
                                                <div class="notes-wrap {{ ($permit->notes) ? 'd-block' : 'd-none' }}" id="pnote-{{$permit->permit_id . $key}}">
                                                    <h5>Notes</h5>
                                                    <textarea class="form-control" name="Permit[{{$key}}][notes]" cols="50" rows="5">{{$permit->notes}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="prereq-permit" id="pre-permit-{{$key}}">
                                            @forelse($permit->projectPrerequisitePermits as $prekey => $pre_permit)
                                                <input type="hidden" class="form-control" name="Permit[{{$key}}][Pre][{{$prekey}}][id]" value="{{ $pre_permit->id }}" />
                                                <div class="prereq-wrap">
                                                    <a href="#" class="close x-child" data-pkey="{{$key}}" data-prekey="{{$prekey}}"><i class="fa fa-times"></i></a>
                                                    <div class="upper-fields">
                                                        <div class="uf-wrap left">
                                                            <h5>Title</h5>
                                                            <input type="text" class="form-control" name="Permit[{{$key}}][Pre][{{$prekey}}][name]" value="{{ $pre_permit->name }}" />
                                                        </div>
                                                        <div class="uf-wrap right">
                                                            <ul class="btn-wrap">
                                                                <li id="pre-view-file-{{$key . $prekey}}" class="middle">
                                                                    <a href="#" class="btn btn-primary btn-vfile" data-toggle="modal" data-target="#prereq-file-modal-{{$key . $prekey}}"><i class="fa fa-paperclip"></i> FILE ATTACHMENTS</a>
                                                                </li>
                                                                <li class="right">
                                                                    <h5>Status</h5>
                                                                    <input type="checkbox" name="Permit[{{$key}}][Pre][{{$prekey}}][status]" class="make-switch-radio" data-on-text="OPEN" data-off-text="CLOSE" data-on-color="primary" data-off-color="danger" value="1" {{ ( $pre_permit->status != 1 ) ?: 'checked' }} />
                                                                    {{-- <span class="radio_switchery_padding">Status</span> --}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <ul class="date-fields">
                                                        <li>
                                                            <h5>Due Date</h5>
                                                            <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_due]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                               data-date-format="mm/dd/yyyy"
                                                               value="{{ !($pre_permit->date_due) ? '' : date('m/d/Y', strtotime($pre_permit->date_due)) }}" autocomplete="off">
                                                        </li>
                                                        <li>
                                                            <h5>Date Filed</h5>
                                                            <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_filed]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                               data-date-format="mm/dd/yyyy"
                                                               value="{{ !($pre_permit->date_filed) ? '' : date('m/d/Y', strtotime($pre_permit->date_filed)) }}" autocomplete="off">
                                                        </li>
                                                        <li>
                                                            <h5>Date Acquired</h5>
                                                            <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_acquired]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                               data-date-format="mm/dd/yyyy"
                                                               value="{{ !($pre_permit->date_acquired) ? '' : date('m/d/Y', strtotime($pre_permit->date_acquired)) }}" autocomplete="off">
                                                        </li>
                                                        <li>
                                                            <h5>Date Expiry</h5>
                                                            <input type="text" name="Permit[{{$key}}][Pre][{{$prekey}}][date_expiry]" class="form-control datepicker" placeholder="mm/dd/yyyy"
                                                               data-date-format="mm/dd/yyyy"
                                                               value="{{ !($pre_permit->date_expiry) ? '' : date('m/d/Y', strtotime($pre_permit->date_expiry)) }}" autocomplete="off">
                                                        </li>
                                                    </ul>

                                                    {{-- PRE-REQUISITE PERMIT FILE MODAL | START --}}
                                                    <div id="prereq-file-modal-{{$key . $prekey}}" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel"
                                                                 aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <ul class="mh-wrap">
                                                                        <li class="left">
                                                                            <h4 class="modal-title">File Attachments</h4>
                                                                        </li>
                                                                        <li class="right">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">×</span>
                                                                            </button>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="permit-file-wrapper" id="permit-file-{{$key . $prekey}}">
                                                                        <a href="#" class="file-close d-none"><i class="fa fa-times"></i></a>
                                                                        <input  name="Permit[{{$key}}][Pre][{{$prekey}}][uploads][]" type="file" multiple class="file-loading mult-permit-file" data-id="{{'prereq-' . $key . $prekey}}">

                                                                        <div class="existing-file-wrapper d-none" id="existing-{{'prereq-' . $key . $prekey}}">
                                                                            @forelse($pre_permit->uploads as $pUpload)
                                                                                @if($pUpload->key == 'prereq-attachment')
                                                                                    <input type="hidden" value="{{asset($pUpload->path)}}" class="exists-path" data-id="{{$pUpload->id}}">
                                                                                @endif
                                                                            @empty
                                                                            @endforelse
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-success" data-dismiss="modal">CONFIRM</button>
                                                                    <button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CANCEL</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- PRE-REQUISITE PERMIT FILE MODAL | END --}}

                                                    <div class="add-notes-wrap padding-top">
                                                        <div class="notes-btn-wrap">
                                                            <input type="hidden" id="prechcknote-{{$key . $prekey}}" name="Permit[{{$key}}][Pre][{{$prekey}}][check_note]" value="{{ ($pre_permit->notes) ? 1 : 0 }}">
                                                            <a href="#" class="btn-prereq-notes {{ !($pre_permit->notes) ?: 'active' }}" data-id="{{$key . $prekey}}">
                                                                @if($pre_permit->notes)
                                                                    <i class="fa fa-minus"></i> REMOVE NOTES
                                                                @else
                                                                    <i class="fa fa-plus"></i> ADD NOTES
                                                                @endif
                                                            </a>
                                                        </div>
                                                        <div class="notes-wrap {{ ($pre_permit->notes) ? 'd-block' : 'd-none' }}" id="prenote-{{$key . $prekey}}">
                                                            <h5>Notes</h5>
                                                            <textarea class="form-control" name="Permit[{{$key}}][Pre][{{$prekey}}][notes]" cols="50" rows="1">{{$pre_permit->notes}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                            @endforelse

                                        </div>
                                        <div class="add-prereq-btn">
                                            <a href="#" class="btn btn-success" data-count={{$key}}><i class="fa fa-plus"></i> ADD PRE-REQUISITE</a>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 p-l-0">
            <div id="sticky-sidebar" style="margin-bottom:10px;">
                <div class="card">
                    <div class="card-header dark-blue">
                        Actions
                    </div>
                    <div class="card-body">
                        <div class="assign-master">
                            <h5>Assign Master Client</h5>
                            @if($project->exists)
                                <select size="3" multiple class="form-control chzn-select" name="master[]" tabindex="8">
                                    @foreach( $tmpMasters as $tmpMaster )
                                        <option value="{{ $tmpMaster['id'] }}" {{ ($tmpMaster['selected']) ? 'selected' : '' }}>
                                            {{ $tmpMaster['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                            @else
                                <select size="3" multiple class="form-control chzn-select" name="master[]" tabindex="8">
                                    @forelse( $masters as $master )
                                        <option value="{{ $master->id }}">
                                            {{ $master->name }}
                                        </option>
                                    @empty
                                    @endforelse
                                </select>
                            @endif
                        </div>
                        <div class="checkbox">
                            <div class="form-check row">
                                <div class="col-lg-12 px-0">
                                    <div class="input-group">
                                        <label for="active" class="custom-control custom-checkbox">
                                            @php
                                                $checkActive = isset($project->show_to_client) && ( $project->show_to_client ) ? 'checked' : '';
                                            @endphp
                                            <input type="checkbox" id="active" name="show_to_client" class="custom-control-input" value=1 {{ (!$project->exists) ? 'checked' :$checkActive }}>
                                            <span class="custom-control-label"></span>
                                            <h5 class="custom-control-description">Show to client</h5>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($project->exists)
                            <div class="project-date-details">
                                <p>
                                    <em>{{$project->created_at->format('F d, Y')}}</em>
                                    <sub>Date Created:</sub>
                                </p>
                                <p>
                                    <em>{{$project->updated_at->format('F d, Y')}}</em>
                                    <sub>Last Updated:</sub>
                                </p>
                            </div>
                        @endif
                    </div>
                    <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                        <button class="btn btn-success form-control" type="submit">
                            <i class="fa fa-save"></i> &nbsp;
                            {{ $actionLabel }}
                        </button>
                    </div>
                </div>
                @if( $project->exists )
                    <div class="card m-t-30">
                        <div class="card-header dark-blue">
                            SMS
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-success form-control" type="button" data-toggle="modal" data-target="#sms-modal">
                                <i class="fa fa-envelope"></i> &nbsp;
                                Send SMS
                            </button>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</form>

@include('templates.project.modal-permit')
@if( $project->exists )
    @include('templates.project.modal-sms')
@endif
