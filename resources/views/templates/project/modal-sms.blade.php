<div id="sms-modal" class="modal permit-modal" role="dialog" aria-labelledby="modalLabel"
             aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="mh-wrap">
                    <li class="left">
                        <h4 class="modal-title">SEND SMS</h4>
                    </li>
                    <li class="right">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </li>
                </ul>
            </div>
            <div class="modal-body">
                <p id="sms-alert"></p>
                @php
                    $ph_number = preg_replace("/[^+0-9]/", "", $project->client->cp_number);
                @endphp
                <input type="hidden" name="security_key" id="security_key" value="e8be2fd79be494d">
                <input type="hidden" name="app_key" id="app_key" value="26306700ac">
                <input type="hidden" name="number" id="number" value="{{$ph_number}}">
                <input type="hidden" name="name" id="name" value="{{$project->client->name}}">
                <textarea name="message" id="message" class="form-control" cols="30" rows="10">Hi, there is an update for your project '{{$project->name}}'. you can login to http://dashboard.apsi.com.ph to check your project progress</textarea>
            </div>
            <div class="modal-footer">
                <button 
                    id="send-sms"
                    type="button"
                    class="btn btn-success">
                    SEND
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="send-cancel">CANCEL</button>
            </div>
        </div>
    </div>
</div>