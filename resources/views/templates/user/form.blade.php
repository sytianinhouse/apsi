<form method="{{ $method }}" role="form" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="card">
                <div class="card-header dark-blue">
                    {{ $cardLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>First Name</h5>
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') ?: $user->first_name }}" />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Last Name</h5>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') ?: $user->last_name }}" />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Email <span class="required">*</span>
                                @if($errors->has('email'))
                                    <span class="error-notif">{{ $errors->first('email') }}</span>
                                @endif
                            </h5>
                            <input type="email" class="form-control {{ ($errors->has('email')) ? 'error' : '' }}" name="email" value="{{ old('email') ?: $user->email }}" requried />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Username <span class="required">*</span>
                                @if($errors->has('username'))
                                    <span class="error-notif">{{ $errors->first('username') }}</span>
                                @endif
                            </h5>
                            <input type="text" class="form-control {{ ($errors->has('username')) ? 'error' : '' }}" name="username" value="{{ old('username') ?: $user->username }}" required />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Password
                                @if( !$user->exists )
                                    <span class="required">*</span>
                                @endif
                                @if($errors->has('password'))
                                    <span class="error-notif">{{ $errors->first('password') }}</span>
                                @endif
                            </h5>
                            <input type="password" class="form-control {{ ($errors->has('password')) ? 'error' : '' }}" name="password" value="{{ old('password') ?: '' }}" {{ $user->exists ?: 'required' }} />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Confirm Password
                                @if( !$user->exists )
                                    <span class="required">*</span>
                                @endif
                                @if($errors->has('password'))
                                    <span class="error-notif">{{ $errors->first('password') }}</span>
                                @endif
                            </h5>
                            <input type="password" class="form-control {{ ($errors->has('password')) ? 'error' : '' }}" name="password_confirmation" value="{{ old('password_confirmation') ?: '' }}" {{ $user->exists ?: 'required' }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 p-l-0">
            <div class="card" id="sticky" style="margin-bottom:10px;">
                <div class="card-header dark-blue">
                    Actions
                </div>
                @if( $user->id != Auth::id() )
                    <div class="card-body">
                        <div class="checkbox">
                            <div class="form-check row">
                                <div class="col-lg-6 px-0">
                                    <div class="input-group">
                                        <label for="active" class="custom-control custom-checkbox">
                                            @php
                                                $checkActive = isset($user->active) && ( $user->active == 1 ) ? 'checked' : '';
                                            @endphp
                                            <input type="checkbox" id="active" name="active" class="custom-control-input" value=1 {{ (!$user->exists) ? 'checked' :$checkActive }}>
                                            <span class="custom-control-label"></span>
                                            <h5 class="custom-control-description">Active</h5>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                    <button class="btn btn-success form-control" type="submit">
                        <i class="fa fa-save"></i> &nbsp;
                        {{ $actionLabel }}
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
