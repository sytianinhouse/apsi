<form action="{{ $action }}" method="{{ $method }}" role="form" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="card">
                <div class="card-header dark-blue">
                    {{ $cardLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>Site Name <span class="required">*</span></h5>
                            <input type="text" class="form-control" name="site_name" value="{{ $setting->site_name }}" required />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Site Email <span class="required">*</span></h5>
                            <input type="email" class="form-control" name="site_email" value="{{ $setting->site_email }}" required />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 p-l-0">
            <div class="card" id="sticky" style="margin-bottom:10px;">
                <div class="card-header dark-blue">
                    Actions
                </div>
                <div class="card-body">
                    <div class="checkbox">
                        <h5>Maintenance Mode</h5>
                        <input type="checkbox" name="under_maintenance" class="make-switch-radio" data-on-text="ENABLED" data-off-text="DISABLED" data-on-color="primary" data-off-color="danger" {{ !( $setting->under_maintenance ) ?: 'checked' }} />
                    </div>
                    @if($setting->exists)
                        <div class="project-date-details">
                            <p>
                                <em>{{$setting->updated_at->format('F d, Y')}}</em>
                                <sub>Last Updated:</sub>
                            </p>
                        </div>
                    @endif
                </div>
                <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                    <button class="btn btn-success form-control" type="submit">
                        <i class="fa fa-save"></i> &nbsp;
                        {{ $actionLabel }}
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
