@component('mail::message')
<h2 style="font-size: 26px;">{{ $projectPermit->name }}</h2>
<ul>
<li><b>City: </b>{{ $projectPermit->city->name }}</li>
<li><b>Due Date: </b>{{ ($projectPermit->date_due) ? date('m/d/Y', strtotime($projectPermit->date_due)) : '--/--/---' }}</li>
<li><b>Date Filed: </b>{{ ($projectPermit->date_filed) ? date('m/d/Y', strtotime($projectPermit->date_filed)) : '--/--/---' }}</li>
<li><b>Date Acquired: </b>{{ ($projectPermit->date_acquired) ? date('m/d/Y', strtotime($projectPermit->date_acquired)) : '--/--/---' }}</li>
<li><b>Date Expiry: </b>{{ ($projectPermit->date_expiry) ? date('m/d/Y', strtotime($projectPermit->date_expiry)) : '--/--/---' }}</li>
@foreach( $statuses as $keys => $status )
@if( $keys == $projectPermit->status )
<li><b>Status: </b>{{ $status }}</li>
@endif
@endforeach
</ul>

<h2 style="font-size: 22px;">Pre-Requisite</h2>

@foreach( $projectPermit->projectPrerequisitePermits as $preReqPermit )
<h3 style="font-size: 18px;">{{ $preReqPermit->name }}</h3>
<ul>
<li><b>Due Date: </b>{{ ($preReqPermit->date_due) ? date('m/d/Y', strtotime($preReqPermit->date_due)) : '--/--/---' }}</li>
<li><b>Date Filed: </b>{{ ($preReqPermit->date_filed) ? date('m/d/Y', strtotime($preReqPermit->date_filed)) : '--/--/---' }}</li>
<li><b>Date Acquired: </b>{{ ($preReqPermit->date_acquired) ? date('m/d/Y', strtotime($preReqPermit->date_acquired)) : '--/--/---' }}</li>
<li><b>Date Expiry: </b>{{ ($preReqPermit->date_expiry) ? date('m/d/Y', strtotime($preReqPermit->date_expiry)) : '--/--/---' }}</li>
<li><b>Status: </b>{{ ($preReqPermit->status == 1) ? 'Active' : 'Inactive' }}</li>
</ul>
@endforeach
@endcomponent