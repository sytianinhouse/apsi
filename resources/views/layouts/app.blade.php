<!DOCTYPE html>
<html>
<head>
    <title>APSI | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="{{asset('assets/img/logo1.ico')}}"/>
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/all-plugins.css') }}?v=0.0"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}?v=0.0" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/login1.css')}}"/>
</head>
<body>
<div class="preloader">
    <div class="preloader-img">
        <img src="{{asset('assets/img/loader.gif')}}" alt="loading...">
    </div>
</div>
@yield('content')
<!-- global js -->
<script type="text/javascript" src="{{asset('assets/js/all-plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin.js')}}"></script>

</body>
</html>