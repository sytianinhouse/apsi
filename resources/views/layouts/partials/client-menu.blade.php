<div class="client-menu-top">
    <div class="container">
        <ul class="client-menu">
            <li>
                <a href="{{ route('client.projects.index') }}">
                    <i class="fa fa-archive"></i> Projects
                </a>
            </li>
        </ul>
    </div>
</div>


{{-- <li class="nav-item">
    <a class="nav-link text-white" href="{{ route('client.projects.index') }}">
        <i class="fa fa-archive"></i> <span class="quick_text">Projects</span>
    </a>
</li> --}}

{{-- <div id="left" class="fixed">
    <div class="menu_scroll left_scrolled">
        <ul id="menu" class="bg-blue dker">
            <li>
                <a href="{{ route('client.projects.index') }}">
                    <i class="fa fa-archive"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Projects</span>
                </a>
            </li>
        </ul>
    </div>
</div> --}}
