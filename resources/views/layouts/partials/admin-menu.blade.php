<div id="left" class="fixed">
    <div class="menu_scroll left_scrolled">
        <ul id="menu" class="bg-blue dker">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Dashboard</span>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-group" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Clients</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.clients.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;All Clients</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.clients.create') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Add New</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Permits</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.permits.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;All Permits</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.permits.create') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Add New</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.permit_categories.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Categories</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-archive" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Projects</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.projects.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;All Projects</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.projects.create') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Add New</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-building-o" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Cities</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.cities.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;All Cities</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.cities.create') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Add New</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Users</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;All Users</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.users.create') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Add New</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Reports</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.reports.projPerCategory') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Projects Per Permit Category</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.reports.projPerCity') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Projects Per City</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.reports.projPerStatus') }}">
                            <i class="fa fa-angle-right"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Projects Per Status</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Settings</span>

                    <span class="fa arrow menu_hide"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin.settings') }}">
                            <i class="fa fa-cog"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;General</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
