<div class="client-menu-top">
    <div class="container">
        <ul class="client-menu">
            <li>
                <a href="{{ route('master_client.dashboard') }}">
                    <i class="fa fa-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="{{ route('master_client.clients') }}">
                    <i class="fa fa-archive"></i> Clients
                </a>
            </li>
        </ul>
    </div>
</div>


{{-- <li class="nav-item">
    <a class="nav-link text-white" href="{{ route('master_client.dashboard') }}">
        <i class="fa fa-home"></i> <span class="quick_text">Dashboard</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link text-white" href="{{ route('master_client.clients') }}">
        <i class="fa fa-users"></i> <span class="quick_text">Clients</span>
    </a>
</li> --}}



{{-- <div id="left" class="fixed">
    <div class="menu_scroll left_scrolled">
        <ul id="menu" class="bg-blue dker">
            <li>
                <a href="{{ route('master_client.dashboard') }}">
                    <i class="fa fa-home"></i>
                    <span class="link-title menu_hide">&nbsp;&nbsp;Dashboard</span>
                </a>
            </li>
        </ul>
    </div>
</div> --}}
