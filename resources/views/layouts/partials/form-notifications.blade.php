<script type="text/javascript">
  $(document).ready(function(){
    var success = '{!! session()->has('success') ? json_encode(session()->get('success')) : 'null' !!}';
    var errors = {!! session()->has('errors') ? json_encode($errors->all()) : 'null' !!};
        
    if (success != 'null') {

      setTimeout(function(){
        var notif = new PNotify({
          title: false,
          text: success.toString().replace(/"/g, ""),
          shadow: true,
          type: 'success',
          addclass: 'alert-success text-white b_r_5',
          delay: 2500,
          minHeight: '20px',
          closer: true,
          animate: {
            animate: true,
            in_class: 'rotateIn',
            out_class: 'rollOut'
          }
        }).click(function(){ notif.pnotify_remove(); });
      }, 1000);
    }

    if (errors) {

         var errorsHtml = '<p><strong>Check errors below:</strong></p><ul class="list-unstyled">';
         $.each(errors, function(i, err) {
             let errorString = '<li> * ' + err.toString().replace(/"/g, "") + '</li>';
             errorsHtml += errorString;
         });
         errorsHtml += '</ul>';

        setTimeout(function(){
            var notif = new PNotify({
                title: false,
                text: errorsHtml ? errorsHtml : 'Please check the following errors!',
                shadow: true,
                type: 'error',
                addclass: 'alert-danger text-white',
                delay: 2500,
                minHeight: '20px',
                closer: true,
                animate: {
                    animate: true,
                    in_class: 'rotateIn',
                    out_class: 'rollOut'
                }
            }).click(function(){ notif.pnotify_remove(); });
        }, 1000);
    }
  });
</script>