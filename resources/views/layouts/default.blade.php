<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"> <!-- CSRF Token -->
    <meta name="base-url" content="{{ url('/') }}">
    <meta name="robots" content="noindex">

    @inject('settingRepo', 'PermitExpress\Setting\SettingRepository')
    <title>{{ isset($settingRepo->getSiteName()[0]) ? $settingRepo->getSiteName()[0] : 'APSI' }} | @yield('title')</title>

    <meta name="description" content="description">
    <meta name="keywords" content="keywords">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('images/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ff0000">

    {{-- <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/base.css') }}?v=0.0"/> --}}
    <link type="text/css" rel="stylesheet" href="{{asset('../resources/assets/vendors/fileinput/css/fileinput.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('../resources/assets/css/pages/file_upload.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/all-plugins.css') }}?v=0.0"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}?v=0.0" />

    <!-- end of global styles-->

    @yield('header_styles')

    @yield('header_scripts')

</head>

<body @yield('body-id') class="fixed_header @yield('body-class')">

<div class="preloader">
    <div class="preloader-img">
        <img src="{{asset('assets/img/loader.gif')}}" alt="loading...">
    </div>
</div>

<div id="wrap">
    <div id="top" class="fixed">

        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0 position-relative">
                <div class="cs-logo-main-wrapper">
                    <a class="navbar-brand mr-0 cs-logo-wrapper" href="#">
                        <h4 class="text-white">
                            <img src="{{asset('assets/img/site-logo2.png')}}" class="img-fluid" alt="logo">
                        </h4>
                    </a>
                </div>
                <div class="menu mr-sm-auto admin">
                        <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars text-white"></i>
                    </span>
                </div>

                <div class="navbar-toggleable-sm m-lg-auto d-none d-lg-flex top_menu" id="nav-content">
                    <ul class="nav navbar-nav flex-row top_menubar">
                        {{-- @section('heading-navbar-center') --}}
                    </ul>
                </div>

                <div class="topnav dropdown-menu-right ml-auto">

                    {{-- @include('layouts.partials.heading-notifications', [
                        'display' => false
                    ]) --}}

                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn cs-avatar" id="cs_trigger">
                                <div class="profile-pic" style="background-image:url({{ asset('assets/img/user-placeholder.jpg') }})"></div>
                            </button>
                            <div class="cs-dropdown superadmin">
                                <div class="head-dropdown">
                                    <ul>
                                        <li>
                                            <div class="profile-pic" style="background-image:url({{ asset('assets/img/user-placeholder.jpg') }})"></div>
                                        </li>
                                        <li>
                                            {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}
                                            <ul>
                                                <li>
                                                    <span class="email">{{ Auth::user()->email }}</span>
                                                </li>
                                                <li>
                                                    @if( Auth::user()->isSuperAdmin() )
                                                        <a href="{{ route('admin.users.edit', Auth::user()) }}">Profile</a>
                                                    @elseif( Auth::user()->isClient() )
                                                        <a href="{{ route('client.profile') }}">Profile</a>
                                                    @elseif( Auth::user()->isMasterClient() )
                                                        <a href="{{ route('master_client.profile') }}">Profile</a>
                                                    @endif
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="foot-dropdown">
                                    <a class="cs-logout" href="{{ route('logout') }}">
                                        <i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>

    <!-- /#top -->
    <div class="wrapper {{!Auth::user()->isSuperAdmin() ?: 'fixedNav_top'}}">
        @if( Auth::user()->isSuperAdmin() )
            @include('layouts.partials.admin-menu')
        @endif

    <!-- /#left -->
        <div id="content" class="bg-container @yield('content-class')">
            <!-- Content -->
            <div class="outer">
            <div class="inner bg-container">
                @if(Auth::user()->isMasterClient())
                    @include('layouts.partials.master-client-menu')
                @elseif( Auth::user()->isClient() )
                    @include('layouts.partials.client-menu')
                @endif
                    <div class="main-bar stretch-wrapper">
                        <div class="row no-gutters">
                            <div class="col">
                                <ol class="breadcrumb">
                                    @if( Auth::user()->isSuperAdmin() )
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('admin.dashboard') }}">
                                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                                            </a>
                                        </li>
                                    @elseif( Auth::user()->isMasterClient() )
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('master_client.dashboard') }}">
                                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                                            </a>
                                        </li>
                                    @endif
                                    @yield('crumbs')
                                </ol>
                            </div>
                        </div>
                    </div>
                    @yield('content')
                </div>
            </div>
            <!-- Content end -->
        </div>

    </div>

    {{-- @include('partials.form-success-notify') --}}
    {{-- @include('layouts.right_sidebar') --}}

</div>

@yield('before_global_scripts')

<!-- global scripts-->

<!-- end of global scripts-->
<script type="text/javascript" src="{{asset('assets/js/all-plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin.js')}}"></script>
<script type="text/javascript" src="{{asset('../resources/assets/vendors/fileinput/js/fileinput.min.js')}}"></script>
<script type="text/javascript" src="{{asset('../resources/assets/vendors/fileinput/js/theme.js')}}"></script>
{{-- <script type="text/javascript" src="{{asset('../resources/assets/js/pages/file_upload.js')}}"></script> --}}
<!-- page level js -->
@yield('footer_scripts')
@yield('scripts')

@include('layouts.partials.form-notifications')

<!-- end page level js -->

</body>
</html>
