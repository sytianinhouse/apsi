@extends('layouts.app')

@section('content')

<div class="container login-wrapper">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 login_top_bottom">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-8 col-sm-12">
                    <div class="login_logo login_border_radius1">
                        <h3 class="text-center">
                            <img src="{{asset('assets/img/site-logo-white.png')}}" alt="permit logo" class="admire_logo"><span class="text-white"><br/>
                                Log In</span>
                        </h3>
                    </div>
                    <div class="bg-white login_content login_border_radius">

                        @if ($errors->count() > 0)
                            <div class="text-center">
                                <span class="help-block text-danger">
                                    <strong>{!! $errors->first() !!}</strong>
                                </span>
                            </div>
                        @endif

                        <form action="{{ route('login') }}" id="login_validator" method="POST" class="login_validator">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="username" class="col-form-label"> Username</label>

                                <div class="input-group input-group-prepend">
                                    <span class="input-group-text border-right-0 rounded-left"><i
                                                class="fa fa-envelope text-primary"></i></span>
                                    <input type="text" class="form-control form-control-md {{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" name="username" placeholder="Username" value="{{ old('username') }}" required>
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!--</h3>-->
                            <div class="form-group">
                                <label for="password" class="col-form-label">Password</label>
                                <div class="input-group input-group-prepend">
                                    <span class="input-group-text border-right-0 rounded-left addon_password"><i
                                                class="fa fa-lock text-primary"></i></span>
                                    <input type="password" class="form-control form-control-md {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" placeholder="Password" required>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="submit" value="Log In" class="btn btn-primary btn-block login_button">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                </div>
                                <div class="col-6 text-right forgot_pwd">
                                    @if (Route::has('password.request'))
                                        <a class="custom-control-description forgottxt_clr" href="{{ route('password.request') }}">
                                            {{ __('Forgot Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection