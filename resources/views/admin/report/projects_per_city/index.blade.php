@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Projects Per Permit City
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Projects Per Permit City</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Projects Per Permit City</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                @include('templates.report.project_per_city.index', [
                                    'method' => 'GET',
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table" data-sortindex="1" data-sortby="desc" data-disableCol="3">
                            <thead class="flip-content">
                            <tr>
                                <th>CITY NAME</th>
                                <th>Number Of Projects</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $data as $city )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.reports.projCreated','city='.$city->id) }}" class="tbl-title-link">
                                                {{ $city->name }}
                                            </a>
                                        </td>
                                        <td>{{ $city->projects }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.reports.projCreated','city='.$city->id) }}" class="btn btn-success btn-action">VIEW</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Cities Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection