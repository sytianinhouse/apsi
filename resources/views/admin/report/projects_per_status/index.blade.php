@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Projects Created Per Status
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Projects Created Per Permit Status</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Projects Created Per Permit Status</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-10">
                                @include('templates.report.project_per_status.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table" data-sortindex="1" data-sortby="desc" data-disableCol="3">
                            <thead class="flip-content">
                            <tr>
                                <th>Status</th>
                                <th>Number Of Projects</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $projectsPerStatus as $projectPerStatus )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.reports.projCreated','status='.$projectPerStatus->status) }}" class="tbl-title-link">
                                                @foreach(PermitExpress\Permit\Permit::$statuses as $keys => $status)
                                                    @if($keys == $projectPerStatus->status)
                                                        {{ $status }}
                                                    @endif
                                                @endforeach
                                            </a>
                                        </td>
                                        <td>{{ $projectPerStatus->projects }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.reports.projCreated','status='.$projectPerStatus->status) }}" class="btn btn-success btn-action">VIEW</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Permit Status Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection