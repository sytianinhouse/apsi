@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Projects Created
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Projects Created</li>
@stop

@inject('projectRepo', 'PermitExpress\Project\ProjectRepository')

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Projects Created</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12">
                                @include('templates.report.project_per_category.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table" data-sortindex="1" data-sortby="desc" data-disableCol="3">
                            <thead class="flip-content">
                            <tr>
                                <th>Project Name</th>
                                <th>Number Of Permits</th>
                                <th>Date Created</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $projects as $project )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.projects.edit', $projectRepo->getById($project->id)) }}" class="tbl-title-link">
                                                {{ $project->name }}
                                            </a>
                                        </td>
                                        <td>{{ $project->permits }}</td>
                                        <td>{{ ($project->created_at) ? date('m/d/y', strtotime($project->created_at)) : '--/--/--' }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No Projects Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection