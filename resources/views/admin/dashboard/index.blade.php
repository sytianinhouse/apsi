@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Admin Dashboard
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title">
            <h1>Dashboard</h1>
        </div>
    </div>
    <div class="col-md-6 m-t-30">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header dark-blue">
                        Last 10 Created Projects
                    </div>
                    <div class="card-body">
                        <div class="table-responsive m-t-35">
                            <table class="table table-striped dash-table">
                                <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Client Name</th>
                                    <th>City</th>
                                    <th>Date Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($projects as $project)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.projects.edit', $project) }}">{{ $project->name }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.clients.edit', $project->client) }}">{{ $project->client->name }}</a>
                                            </td>
                                            <td>{{ !($project->city) ? '' : $project->city->name }}</td>
                                            <td>{{ $project->created_at->format('F d, Y') }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No project(s)</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 m-t-30">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header dark-blue">
                        Permits for Renewal in 30 Days
                    </div>
                    <div class="card-body">
                        <div class="table-responsive m-t-35">
                            <table class="table table-striped dash-table">
                                <thead>
                                <tr>
                                    <th>Permit Name</th>
                                    <th>Client Name</th>
                                    <th>Project Name</th>
                                    <th>Expiry Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($beforeExpirations as $beforeExpiration)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.projects.edit', $beforeExpiration->project) }}">
                                                    {{ $beforeExpiration->name }}
                                                </a>
                                            </td>
                                            <td>{{ $beforeExpiration->project->client->name }}</td>
                                            <td>{{ $beforeExpiration->project->name }}</td>
                                            <td>{{ date('F d, Y', strtotime($beforeExpiration->date_expiry)) }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No permit(s)</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card m-t-30">
                    <div class="card-header dark-blue">
                        Permits In Temporary Status
                    </div>
                    <div class="card-body">
                        <div class="table-responsive m-t-35">
                            <table class="table table-striped dash-table">
                                <thead>
                                <tr>
                                    <th>Permit Name</th>
                                    <th>Client Name</th>
                                    <th>Project Name</th>
                                    <th>Expiry Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($temporaryStatus as $tmpStatus)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.projects.edit', $tmpStatus->project) }}">
                                                    {{ $tmpStatus->name }}
                                                </a>
                                            </td>
                                            <td>{{ $tmpStatus->project->client->name }}</td>
                                            <td>{{ $tmpStatus->project->name }}</td>
                                            <td>{{ !($tmpStatus->date_expiry) ? '--/--/----' : date('F d, Y', strtotime($tmpStatus->date_expiry)) }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No permit(s)</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card m-t-30">
                    <div class="card-header dark-blue">
                        Permits Due In 2 Weeks
                    </div>
                    <div class="card-body">
                        <div class="table-responsive m-t-35">
                            <table class="table table-striped dash-table">
                                <thead>
                                <tr>
                                    <th>Permit Name</th>
                                    <th>Client Name</th>
                                    <th>Project Name</th>
                                    <th>Due Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($beforeDueDate as $dueDate)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.projects.edit', $dueDate->project) }}">
                                                    {{ $dueDate->name }}
                                                </a>
                                            </td>
                                            <td>{{ $dueDate->project->client->name }}</td>
                                            <td>{{ $dueDate->project->name }}</td>
                                            <td>{{ !($dueDate->date_due) ? '--/--/----' : date('F d, Y', strtotime($dueDate->date_due)) }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No permit(s)</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
