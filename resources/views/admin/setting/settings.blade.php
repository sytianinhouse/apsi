@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Settings
@stop

@section('crumbs')
    <li class="breadcrumb-item active">Settings</li>
@stop

@section('content')

    @if($siteSetting)
        @include('templates.setting.form', [
            'action' => route('admin.update_settings', $siteSetting),
            'setting' =>  $siteSetting,
            'method' => 'POST',
            'actionLabel' => 'Save Changes',
            'cardLabel' => 'General'
        ])
    @else
        @include('templates.setting.form', [
            'action' => route('admin.store_settings'),
            'setting' =>  new PermitExpress\Setting\SiteSetting(),
            'method' => 'POST',
            'actionLabel' => 'Save Changes',
            'cardLabel' => 'General'
        ])
    @endif


@endsection