@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Edit Client
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.clients.index') }}">Clients</a>
    </li>
    <li class="breadcrumb-item active">Update Client</li>
@stop

@section('content')

    @include('templates.client.form', [
        'client' =>  $client,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Company Details',
        'credsLabel' => 'Login Details',
        'contactLabel' => 'Contact Person Details',
    ])

@endsection