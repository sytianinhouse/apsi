@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Add Client
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.clients.index') }}">Clients</a>
    </li>
    <li class="breadcrumb-item active">Add Client</li>
@stop

@section('content')

    @include('templates.client.form', [
        'client' =>  new PermitExpress\Client\Client(),
        'method' => 'POST',
        'actionLabel' => 'Save Client',
        'cardLabel' => 'Company Details',
        'credsLabel' => 'Login Details',
        'contactLabel' => 'Contact Person Details',
    ])

@endsection