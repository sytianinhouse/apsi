@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Clients
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Clients</li>
@stop

@inject('clientRepo', 'PermitExpress\Client\ClientRepository')

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Clients</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                @include('templates.client.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="add-wrap">
                                    <a href="{{ route('admin.clients.create') }}" class="btn btn-success btn-add"><i class="fa fa-user-plus"></i> Add Client</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped" id="index-table" data-sortindex="3" data-sortby="desc" data-disableCol="7">
                            <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Contact Name</th>
                                    <th>Contact Number</th>
                                    <th>Date Created</th>
                                    <th>Projects Created</th>
                                    <th>Access Level</th>
                                    <th class="center-action">Status</th>
                                    <th class="center-action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse( $clients as $client )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.clients.edit', $client) }}" class="tbl-title-link">
                                                {{ $client->name }}
                                            </a>
                                        </td>
                                        <td>{{ $client->cp_first_name . ' ' . $client->cp_last_name }}</td>
                                        <td>{{ $client->cp_number ? $client->cp_number : $client->cp_number_2 }}</td>
                                        <td>{{ $client->created_at->format('F d, Y') }}</td>
                                        <td>{{ $client->projects->count() }}</td>
                                        <td>{{ ($client->is_master) ? 'Master' : 'Client' }}</td>
                                        <td class="center-action"><span class="status {{ $client->user->statusColor }}">{{ $client->user->status }}</span></td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.clients.edit', $client) }}" class="btn btn-success btn-action">EDIT</a>
                                            <a href="{{ route('admin.projects.byClient', $client) }}" class="btn btn-primary btn-action {{ (!$client->is_master) ?: 'disabled' }}">PROJECTS</a>
                                            <a href="{{ route('admin.clients.destroy', $client) }}" class="btn btn-danger btn-action btn-delete"
                                                data-projCount="{{ $clientRepo->hasProjects($client) }}">DELETE</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="8">No clients found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection