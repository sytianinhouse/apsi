@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Add Project
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.projects.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item active">Add Project</li>
@stop

@section('content')

    @include('templates.project.form', [
        'project' =>  new PermitExpress\Project\Project(),
        'clients' => $clients,
        'permits' => $permits,
        'cities' => $cities,
        'method' => 'POST',
        'actionLabel' => 'Save Project',
        'cardLabel' => 'Details',
        'permitLabel' => 'Permits'
    ])

@endsection