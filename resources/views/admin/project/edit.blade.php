@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Edit Project
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.projects.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item active">Edit Project</li>
@stop

@section('content')

    @include('templates.project.form', [
        'project' =>  $project,
        'clients' => $clients,
        'permits' => $permits,
        'cities' => $cities,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Details',
        'permitLabel' => 'Permits'
    ])

@endsection