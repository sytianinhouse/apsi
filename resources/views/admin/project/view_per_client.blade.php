@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Projects
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Projects</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Projects for {{ $client->name }}</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                @include('templates.project.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="add-wrap">
                                    <a href="{{ route('admin.projects.createByClient', $client) }}" class="btn btn-success btn-add"><i class="fa fa-plus"></i> Add Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table" data-sortindex="2" data-sortby="desc" data-disableCol="4">
                            <thead class="flip-content">
                            <tr>
                                <th>Project Name</th>
                                <th>Client Name</th>
                                <th>City</th>
                                <th>Date Created</th>
                                <th>Permits</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $projects as $project )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.projects.edit', $project) }}" class="tbl-title-link">
                                                {{ $project->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.clients.edit', $project->client) }}" class="tbl-title-link">
                                                {{ $project->client->name }}
                                            </a>
                                        </td>
                                        <td>{{ ($project->city) ? $project->city->name : '' }}</td>
                                        <td>{{ $project->created_at->format('F d, Y') }}</td>
                                        <td>{{ $project->permit_counts }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.projects.edit', $project) }}" class="btn btn-primary btn-action">EDIT</a>
                                            <a href="{{ route('admin.projects.destroy', $project) }}" class="btn btn-danger btn-action btn-delete">DELETE</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">No projects found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection