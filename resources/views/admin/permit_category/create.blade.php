@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Add Permit Category
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.permits.index') }}">Permits</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.permit_categories.index') }}">Permit Categories</a>
    </li>
    <li class="breadcrumb-item active">Add Category</li>
@stop

@section('content')

    @include('templates.permit_category.form', [
        'permitCategory' =>  new PermitExpress\PermitCategory\PermitCategory(),
        'method' => 'POST',
        'actionLabel' => 'Save Category',
        'cardLabel' => 'Details'
    ])

@endsection