@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Permit Categories
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.cities.index') }}">Permits</a>
    </li>
    <li class="breadcrumb-item active">Permit Categories</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Permit Categories</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                {{-- @include('templates.permit_category.filter', [
                                    'method' => 'POST',
                                ]) --}}
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="add-wrap">
                                    <a href="{{ route('admin.permit_categories.create') }}" class="btn btn-success btn-add"><i class="fa fa-plus"></i> Add Category</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table" data-sortindex="0" data-sortby="asc" data-disableCol="2">
                            <thead class="flip-content">
                            <tr>
                                <th>Name</th>
                                <th>Date Created</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $permitCategories as $permitCategory )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.permit_categories.edit', $permitCategory) }}" class="tbl-title-link">
                                                {{ $permitCategory->name }}
                                            </a>
                                        </td>
                                        <td>{{ $permitCategory->created_at->format('F d, Y') }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.permit_categories.edit', $permitCategory) }}" class="btn btn-primary btn-action">EDIT</a>
                                            <a href="{{ route('admin.permit_categories.destroy', $permitCategory) }}" class="btn btn-danger btn-action btn-delete">DELETE</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No permit categories found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection