@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Edit Permit Category
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.permits.index') }}">Permits</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.permit_categories.index') }}">Permit Categories</a>
    </li>
    <li class="breadcrumb-item active">Update Category</li>
@stop

@section('content')

    @include('templates.permit_category.form', [
        'permitCategory' =>  $permitCategory,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Details'
    ])

@endsection