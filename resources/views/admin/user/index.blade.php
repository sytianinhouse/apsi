@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Users
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Users</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Users</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                @include('templates.user.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="add-wrap">
                                    <a href="{{ route('admin.users.create') }}" class="btn btn-success btn-add"><i class="fa fa-plus"></i> Add User</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table" data-sortindex="3" data-sortby="desc" data-disableCol="4">
                            <thead class="flip-content">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Date Created</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $users as $user )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.users.edit', $user) }}" class="tbl-title-link">
                                                {{ $user->first_name . ' ' . $user->last_name }}
                                            </a>
                                        </td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ ($user->created_at) ? $user->created_at->format('F d, Y') : '-/--/--' }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary btn-action">EDIT</a>
                                            <a href="{{ route('admin.users.destroy', $user) }}" class="btn btn-danger btn-action btn-delete">DELETE</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No users found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection