@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Edit User
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.users.index') }}">Users</a>
    </li>
    <li class="breadcrumb-item active">Update User</li>
@stop

@section('content')

    @include('templates.user.form', [
        'user' =>  $user,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Details'
    ])

@endsection