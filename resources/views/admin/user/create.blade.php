@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Add User
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.users.index') }}">Users</a>
    </li>
    <li class="breadcrumb-item active">Add User</li>
@stop

@section('content')

    @include('templates.user.form', [
        'user' =>  new PermitExpress\User\User(),
        'method' => 'POST',
        'actionLabel' => 'Save User',
        'cardLabel' => 'Details'
    ])

@endsection