@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Add City
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.cities.index') }}">Cities</a>
    </li>
    <li class="breadcrumb-item active">Add City</li>
@stop

@section('content')

    @include('templates.city.form', [
        'city' =>  new PermitExpress\City\City(),
        'method' => 'POST',
        'actionLabel' => 'Save City',
        'cardLabel' => 'Details'
    ])

@endsection