@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Edit City
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.cities.index') }}">Cities</a>
    </li>
    <li class="breadcrumb-item active">Update City</li>
@stop

@section('content')

    @include('templates.city.form', [
        'city' =>  $city,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Details'
    ])

@endsection