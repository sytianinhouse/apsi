@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Permit Express | Permits
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Permits</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Permits</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                @include('templates.permit.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="add-wrap">
                                    <a href="{{ route('admin.permits.create') }}" class="btn btn-success btn-add"><i class="fa fa-plus"></i> Add Permit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="display table table-bordered table-striped" id="index-table" data-sortindex="1" data-sortby="desc" data-disableCol="2">
                            <thead>
                            <tr>
                                <th>Permit Name</th>
                                <th>Date Created</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $permits as $permit )
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.permits.edit', $permit) }}" class="tbl-title-link">
                                                {{ $permit->name }}
                                            </a>
                                        </td>
                                        <td>{{ $permit->created_at->format('F d, Y') }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('admin.permits.edit', $permit) }}" class="btn btn-primary btn-action">EDIT</a>
                                            <a href="{{ route('admin.permits.destroy', $permit) }}" class="btn btn-danger btn-action btn-delete">DELETE</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No permits found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection