@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Permit Express | Add Permit
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.permits.index') }}">Permits</a>
    </li>
    <li class="breadcrumb-item active">Add Permit</li>
@stop

@section('content')

    @include('templates.permit.form', [
        'permit' =>  new PermitExpress\Permit\Permit(),
        'permitCategories' => $permitCategories,
        'method' => 'POST',
        'actionLabel' => 'Save Permit',
        'cardLabel' => 'Details',
        'reqLabel' => 'Pre-Requisite Permit',
    ])

@endsection