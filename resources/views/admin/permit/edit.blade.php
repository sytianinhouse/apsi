@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Permit Express | Edit Permit
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.permits.index') }}">Permits</a>
    </li>
    <li class="breadcrumb-item active">Add Permit</li>
@stop

@section('content')

    @include('templates.permit.form', [
        'permit' =>  $permit,
        'permitCategories' => $permitCategories,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Details',
        'reqLabel' => 'Pre-Requisite Permit',
    ])

@endsection