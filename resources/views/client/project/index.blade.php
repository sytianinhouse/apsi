@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Projects
@stop

{{-- Page Breadcrumbs --}}
@section('crumbs')
    <li class="breadcrumb-item active">Projects</li>
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Projects</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                @include('templates.project.filter', [
                                    'method' => 'GET',
                                ])
                            </div>
                            <div class="col-12 col-md-6"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table">
                            <thead class="flip-content">
                            <tr>
                                <th>Project Name</th>
                                <th>Date Created</th>
                                <th class="center-action">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse( $projects as $project )
                                    <tr>
                                        <td>
                                            <a href="{{ route('client.projects.view', $project) }}" class="ref-color tbl-title-link">
                                                {{ $project->name }}
                                            </a>
                                        </td>
                                        <td>{{ $project->created_at->format('F d, Y') }}</td>
                                        <td class="center-action">
                                            <a href="{{ route('client.projects.view', $project) }}" class="btn btn-primary btn-action">VIEW</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No projects found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection