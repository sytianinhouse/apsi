@extends('layouts.default')

{{-- Page title --}}
@section('title')
    View Project
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('client.projects.index') }}">Projects</a>
    </li>
    <li class="breadcrumb-item active">Add Project</li>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body client-project">
                    <div class="row align-items-center">
                        <div class="col-9">
                            <h3 style="margin-bottom: 0;">{{ $project->name }}</h3>
                        </div>
                        <div class="col-3">
                            <span class="proj-dc"><b>Date Created: </b> {{ date('F d, Y', strtotime($project->created_at)) }}</span>
                        </div>
                        <div class="col-12 m-t-15">
                            <p><b>City:</b> {{ $project->city->name }}</p>
                        </div>
                        <div class="col-12">
                            <p><b>Barangay:</b> {{ $project->barangay }}</p>
                        </div>
                        <div class="col-12">
                            <p><b>Type:</b> {{ $project->type }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    PERMITS
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @forelse( $project->projectPermits as $key => $projPermit )
                                <div class="card m-t-30 bg-dirty-white">
                                    <div class="card-header border-top-green bg-dirty-white">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-md-10">
                                                <h4 class="permit-title">{{ $projPermit->name }} {{ !($projPermit->branch_name) ? '' : '(' . $projPermit->branch_name .')' }}</h4>
                                            </div>
                                            @if($projPermit->file_path)
                                                <div class="col-12 col-md-2">
                                                    <div class="wrap" style="text-align: right;">
                                                        <a href="#" class="btn btn-primary btn-vfile"  
                                                        data-toggle="modal" 
                                                        data-target="#permit-file-modal-{{$projPermit->id . $key}}"
                                                        style="display: block;">
                                                            <i class="fa fa-paperclip"></i> VIEW ATTACHMENTS
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- PERMIT MODAL | START--}}
                                    <div id="permit-file-modal-{{$projPermit->id . $key}}" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel"
                                                 aria-hidden="true">
                                        <div class="modal-dialog client">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <ul class="mh-wrap">
                                                        <li class="left">
                                                            <h4 class="modal-title">File Attachments ({{$projPermit->name}})</h4>
                                                        </li>
                                                        <li class="right">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive m-t-35">
                                                        <table class="table table-striped table-bordered table-advance table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>
                                                                    <i class="fa fa-briefcase"></i> File Name
                                                                </th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse($projPermit->uploads as $ups)
                                                                    <tr>
                                                                        <td style="width: 62%; vertical-align: middle;">{{$ups->filename}}</td>
                                                                        <td style="width: 38%; vertical-align: middle;">
                                                                            <a href="{{asset($ups->path)}}" class="btn btn-primary btn-xs purple" target="_blank">
                                                                                <i class="fa fa-eye"></i> PREVIEW
                                                                             </a>
                                                                            <a href="{{asset($ups->path)}}" class="btn btn-success btn-xs purple" download>
                                                                                <i class="fa fa-download"></i> DOWNLOAD
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                @endforelse
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CLOSE</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- PERMIT MODAL | END --}}
                                    <div class="card-body">
                                        <div class="card-info-wrap">
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <p><b>City: </b> {{ $projPermit->city->name }}</p>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <p><b>Status: </b>
                                                        @foreach(PermitExpress\Permit\Permit::$statuses as $keys => $status)
                                                            @if( $keys == $projPermit->status )
                                                                {{ $status }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3">
                                                    <p><b>Due Date: </b> {{ !($projPermit->date_due) ? '-/--/--' : date('m/d/y', strtotime($projPermit->date_due)) }}</p>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3">
                                                    <p><b>Date Filed: </b> {{ !($projPermit->date_filed) ? '-/--/--' : date('m/d/y', strtotime($projPermit->date_filed)) }}</p>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3">
                                                    <p><b>Date Acquired: </b> {{ !($projPermit->date_acquired) ? '-/--/--' : date('m/d/y', strtotime($projPermit->date_acquired)) }}</p>
                                                </div>
                                                <div class="col-12 col-md-6 col-lg-3">
                                                    <p><b>Date Expiry: </b> {{ !($projPermit->date_expiry) ? '-/--/--' : date('m/d/y', strtotime($projPermit->date_expiry)) }}</p>
                                                </div>
                                                <div class="col-12">
                                                    <p><b>Notes: </b> {{$projPermit->notes}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center">
                                            @forelse($projPermit->projectPrerequisitePermits as $key2 => $preReqPermit)
                                                <div class="col-12 col-xl-6 m-t-15">
                                                    <div class="card">
                                                        <div class="card-header {{($preReqPermit->status) ? 'prereq-head-open' : 'prereq-head-close'}}">
                                                            <div class="row align-items-center">
                                                                <div class="col-12 col-md-9">
                                                                    <h5 class="permit-title">{{ $preReqPermit->name }}</h5>
                                                                </div>
                                                                @if($preReqPermit->file_path)
                                                                    <div class="col-12 col-md-3">
                                                                        <ul class="cp-btn-wrapper">
                                                                            <li style="width:100%;">
                                                                                <a href="#" class="btn btn-primary"
                                                                                data-toggle="modal" 
                                                                                data-target="#prereq-file-modal-{{$preReqPermit->id . $key2}}"><i class="fa fa-paperclip">
                                                                                    </i> VIEW ATTACHMENTS
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        {{-- PRE-REQ PERMIT MODAL | START--}}
                                                        <div id="prereq-file-modal-{{$preReqPermit->id . $key2}}" class="modal permit-file-modal" role="dialog" aria-labelledby="modalLabel"
                                                                     aria-hidden="true">
                                                            <div class="modal-dialog client">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <ul class="mh-wrap">
                                                                            <li class="left">
                                                                                <h4 class="modal-title">File Attachments ({{$preReqPermit->name}})</h4>
                                                                            </li>
                                                                            <li class="right">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">×</span>
                                                                                </button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="table-responsive m-t-35">
                                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        <i class="fa fa-briefcase"></i> File Name
                                                                                    </th>
                                                                                    <th>Actions</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @forelse($preReqPermit->uploads as $ups)
                                                                                        <tr>
                                                                                            <td style="width: 62%; vertical-align: middle;">{{$ups->filename}}</td>
                                                                                            <td style="width: 38%; vertical-align: middle;">
                                                                                                <a href="{{asset($ups->path)}}" class="btn btn-primary btn-xs purple" target="_blank">
                                                                                                    <i class="fa fa-eye"></i> PREVIEW
                                                                                                </a>
                                                                                                <a href="{{asset($ups->path)}}" class="btn btn-success btn-xs purple" download>
                                                                                                    <i class="fa fa-download"></i> DOWNLOAD
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    @empty
                                                                                    @endforelse
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-danger permit-modal-cancel" data-dismiss="modal">CLOSE</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{-- PRE-REQ PERMIT MODAL | END --}}
                                                        <div class="card-body {{($preReqPermit->status) ? 'prereq-body-open' : 'prereq-body-close'}}">
                                                            <div class="row align-items-center">
                                                                <div class="col-6">
                                                                    <p><b>Due Date: </b> {{ !($preReqPermit->date_due) ? '-/--/--' : date('m/d/y', strtotime($preReqPermit->date_due)) }}</p>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p><b>Date Filed: </b> {{ !($preReqPermit->date_filed) ? '-/--/--' : date('m/d/y', strtotime($preReqPermit->date_filed)) }}</p>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p><b>Date Acquired: </b> {{ !($preReqPermit->date_acquired) ? '-/--/--' : date('m/d/y', strtotime($preReqPermit->date_acquired)) }}</p>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p><b>Date Expiry: </b> {{ !($preReqPermit->date_expiry) ? '-/--/--' : date('m/d/y', strtotime($preReqPermit->date_expiry)) }}</p>
                                                                </div>
                                                                <div class="col-6">
                                                                    <p><b>Status: </b> {{ ($preReqPermit->status) ? 'Open' : 'Close' }}</p>
                                                                </div>
                                                                <div class="col-12">
                                                                    <p><b>Notes:</b> {{$preReqPermit->notes}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection