@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Master Client Dashboard
@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    Permits for Renewal in 30 Days
                </div>
                <div class="card-body">
                    <div class="table-responsive m-t-35">
                        <table class="table table-striped dash-table">
                            <thead>
                            <tr>
                                <th>Permit Name</th>
                                <th>Client Name</th>
                                <th>Project Name</th>
                                <th>Expiry Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($beforeExpirations as $beforeExpiration)
                                    <tr>
                                        <td>{{ $beforeExpiration->name }}</td>
                                        <td>{{ $beforeExpiration->project->client->name }}</td>
                                        <td>{{ $beforeExpiration->project->name }}</td>
                                        <td>{{ date('F d, Y', strtotime($beforeExpiration->date_expiry)) }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">No permit(s)</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
