@inject('clientRepo', 'PermitExpress\Client\ClientRepository')

<form action="{{route('master_client.update', $client)}}" method="{{ $method }}" role="form" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 col-sm-9">
            <div class="card">
                <div class="card-header dark-blue">
                    {{ $cardLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 input_field_sections">
                            <h5>Name <span class="required">*</span>
                                @if($errors->has('name'))
                                    <span class="error-notif">{{ $errors->first('name') }}</span>
                                @endif
                            </h5>
                            <input type="text" class="form-control {{ ($errors->has('name')) ? 'error' : '' }}" name="name" value="{{ old('name') ?: $client->name }}" required />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Address</h5>
                            <textarea id="text4" class="form-control" name="address" cols="50" rows="2">{{ old('address') ?: $client->address }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    {{ $contactLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>First Name</h5>
                            <input type="text" class="form-control" name="cp_first_name" value="{{ old('cp_first_name') ?: $client->cp_first_name }}"/>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Last Name</h5>
                            <input type="text" class="form-control" name="cp_last_name" value="{{ old('cp_last_name') ?: $client->cp_last_name }}"/>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Mobile #</h5>
                            <input type="text" class="form-control mobile-phone" name="cp_number" value="{{ old('cp_number') ?: $client->cp_number }}"/>
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Secondary Contact #</h5>
                            <input type="text" class="form-control" name="cp_number_2" value="{{ old('cp_number_2') ?: $client->cp_number_2 }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-30">
                <div class="card-header dark-blue">
                    {{ $credsLabel }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 input_field_sections">
                            <h5>Password
                                @if( !isset($client->user) )
                                    <span class="required">*</span>
                                @endif
                                @if($errors->has('password'))
                                    <span class="error-notif">{{ $errors->first('password') }}</span>
                                @endif
                            </h5>
                            <input type="password" class="form-control {{ ($errors->has('password')) ? 'error' : '' }}" name="password" value="{{ old('password') }}" {{ isset($client->user) ?: 'required' }} />
                        </div>
                        <div class="col-6 input_field_sections">
                            <h5>Confirm Password
                                @if( !isset($client->user) )
                                    <span class="required">*</span>
                                @endif
                                @if($errors->has('password'))
                                    <span class="error-notif">{{ $errors->first('password') }}</span>
                                @endif
                            </h5>
                            <input type="password" class="form-control {{ ($errors->has('password_confirmation')) ? 'error' : '' }}" name="password_confirmation" value="{{ old('password_confirmation') }}" {{ isset($client->user) ?: 'required' }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 p-l-0">
            <div class="card" id="sticky" style="margin-bottom: 15px;">
                <div class="card-header dark-blue">
                    Actions
                </div>
                <div class="card-footer" @if(!isset($activation)) style="border-radius: 3px;border-top: none" @endif>
                    <button class="btn btn-success form-control" type="submit">
                        <i class="fa fa-save"></i> &nbsp;
                        {{ $actionLabel }}
                    </button>
                </div>
            </div>
            <div class="card" id="sticky" style="margin-bottom:10px;">
                <div class="card-header dark-blue">
                    Additional Contacts
                </div>
                <div class="card-body">
                    <div class="ac-wrapper" id="additional-contacts">
                        @if( isset($client->additional_contacts) )
                            @if( old('additional_contacts') )
                                @foreach( old('additional_contacts') as $oldkey => $oldAC )
                                    <div class="ac-wrap">
                                        <ul>
                                            <li class="left">
                                                <input type="text" class="form-control" name="additional_contacts[]" value="{{ old('additional_contacts')[$oldkey] }}" placeholder="Contact Number" />
                                            </li>
                                            <li class="right">
                                                <a href="#" class="btn btn-danger form-control">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @else
                                @foreach( $client->additional_contacts as $key => $additionalContact )
                                    <div class="ac-wrap">
                                        <ul>
                                            <li class="left">
                                                <input type="text" class="form-control" name="additional_contacts[]" value="{{ $additionalContact }}" placeholder="Contact Number" />
                                            </li>
                                            <li class="right">
                                                <a href="#" class="btn btn-danger form-control">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @endif
                        @else
                           @if( old('additional_contacts') )
                                @foreach( old('additional_contacts') as $oldkey => $oldAC )
                                    <div class="ac-wrap">
                                        <ul>
                                            <li class="left">
                                                <input type="text" class="form-control" name="additional_contacts[]" value="{{ old('additional_contacts')[$oldkey] }}" placeholder="Contact Number" />
                                            </li>
                                            <li class="right">
                                                <a href="#" class="btn btn-danger form-control">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            @else
                                <div class="ac-wrap">
                                    <ul>
                                        <li class="left">
                                            <input type="text" class="form-control" name="additional_contacts[]" value="" placeholder="Contact Number" />
                                        </li>
                                        <li class="right">
                                            <a href="#" class="btn btn-danger form-control">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary form-control" id="ac-button">
                        <i class="fa fa-plus"></i> &nbsp;
                        Add Contact
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
