@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Profile
@stop

@section('crumbs')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.clients.index') }}">Home</a>
    </li>
    <li class="breadcrumb-item active">Profile</li>
@stop

@section('content')

    @include('master_client.user.form', [
        'client' =>  Auth::user()->client,
        'method' => 'POST',
        'actionLabel' => 'Save Changes',
        'cardLabel' => 'Details',
        'credsLabel' => 'Credentials',
        'contactLabel' => 'Contact Person Details',
    ])

@endsection