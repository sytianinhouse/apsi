@extends('layouts.default')

{{-- Page title --}}
@section('title')
    Master Clients
@stop

@inject('projPermitRepo', 'PermitExpress\ProjectPermit\ProjectPermitRepository')

@section('content')
    <div class="row">
        <div class="col">
            <div class="page-title">
                <h1>Master Clients</h1>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="filter-and-button-wrapper">
                        <div class="row">
                                @include('templates.client.master_filter', [
                                    'method' => 'GET',
                                ])
                        </div>
                    </div>
                </div>
            </div>
            <div class="card m-t-15">
                <div class="card-body flip-scroll">
                    <div class="m-t-0">
                        <table class="table table-bordered table-striped flip-content" id="index-table">
                            <thead class="flip-content">
                            <tr>
                                <th>Client Name</th>
                                <th>Project Name</th>
                                <th>Permits</th>
                                <th>Pre-Requisite</th>
                                <th>Due Date</th>
                                <th>Date Filed</th>
                                <th>Date Acquired</th>
                                <th>Expiry Date</th>
                                <th>Notes</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if( $newClients )
                                    @foreach( $newClients['client'] as $key => $data )
                                        @if($data['display'])
                                            <tr>
                                                <td>{{$data['name']}}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @if( isset($data['project']) && !empty($data['project']) )
                                                @foreach( $data['project'] as $pKey => $project )
                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $project['name'] }}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    @if( isset($project['permit']) )
                                                        @foreach( $project['permit'] as $permKey => $permit )
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <p style="margin-bottom: 0;">{{ $permit['name'] }}</p>
                                                                    @if( $permit['branch_name'] )
                                                                        <p style="padding-left: 15px;">({{ $permit['branch_name'] }})</p>
                                                                    @endif
                                                                </td>
                                                                <td></td>
                                                                <td>{{ !($permit['date_due']) ? '-/--/--' : date('m/d/y', strtotime($permit['date_due'])) }}</td>
                                                                <td>{{ !($permit['date_filed']) ? '-/--/--' : date('m/d/y', strtotime($permit['date_filed'])) }}</td>
                                                                <td>{{ !($permit['date_acquired']) ? '-/--/--' : date('m/d/y', strtotime($permit['date_acquired'])) }}</td>
                                                                <td {{ !($projPermitRepo->dateChecker($permit['date_expiry'])) ?: 'class=add-red' }}>
                                                                    {{ !($permit['date_expiry']) ? '-/--/--' : date('m/d/y', strtotime($permit['date_expiry'])) }}
                                                                </td>

                                                                <td>{{$permit['notes']}}</td>
                                                            </tr>
                                                            @if( isset($permit['prereq']) )
                                                                @foreach( $permit['prereq'] as $preKey => $prereq )
                                                                    <tr>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>{{ $prereq['name'] }}</td>
                                                                        <td>{{ !($prereq['date_due']) ? '-/--/--' : date('m/d/y', strtotime($prereq['date_due'])) }}</td>
                                                                        <td>{{ !($prereq['date_filed']) ? '-/--/--' : date('m/d/y', strtotime($prereq['date_filed'])) }}</td>
                                                                        <td>{{ !($prereq['date_acquired']) ? '-/--/--' : date('m/d/y', strtotime($prereq['date_acquired'])) }}</td>
                                                                        <td {{ !($projPermitRepo->dateChecker($prereq['date_expiry'])) ?: 'class=add-red' }}>
                                                                            {{ !($prereq['date_expiry']) ? '-/--/--' : date('m/d/y', strtotime($prereq['date_expiry'])) }}
                                                                        </td>
                                                                        <td>{{$prereq['notes']}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
