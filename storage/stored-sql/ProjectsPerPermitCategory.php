<?php

$projects_per_category = "
    SELECT
        `category`.`id`,
        `category`.`name`,
        `city`.`name` city,
        COUNT(DISTINCT `projPerm`.`project_id`) projects,
        `category`.`created_at`
    FROM `projects` proj
    RIGHT JOIN `project_permits` projPerm ON `proj`.`id` = `projPerm`.`project_id`
    RIGHT JOIN `cities` city ON `proj`.`city_id` = `city`.`id`
    RIGHT JOIN `permits` permit ON `projPerm`.`permit_id` = `permit`.`id`
    RIGHT JOIN `permit_categories` category ON `permit`.`permit_category_id` = `category`.`id`
    WHERE (
        (categoryId IS NOT NULL AND
            `category`.`id` = categoryId
        )
        OR (categoryId IS NULL)
    )
    AND (
        (dateStart IS NOT NULL AND dateEnd IS NOT NULL AND
            `proj`.`created_at` BETWEEN dateStart AND dateEnd
        )
        OR (dateStart IS NULL OR dateEnd IS NULL)
    )
    AND (
        (cityId IS NOT NULL AND
            `proj`.`city_id` = cityId
        )
        OR (cityId IS NULL)
    )
    GROUP BY `category`.`id`
";

$projects_created = "
    SELECT
        `proj`.`id`,
        `proj`.`name`,
        COUNT(`projPerm`.`id`) +
        (SELECT
            COUNT(`projPreReq`.`id`)
            FROM `project_permits` projPermit
            JOIN `project_prerequisite_permits` projPreReq ON `projPermit`.`id` = `projPreReq`.`project_permits_id`
            WHERE `project_id` IN (`proj`.`id`))
        
        AS permits,
        `proj`.`created_at`
    FROM `projects` proj
    LEFT JOIN `project_permits` projPerm ON `proj`.`id` = `projPerm`.`project_id`
    LEFT JOIN `cities` city ON `proj`.`city_id` = `city`.`id`
    LEFT JOIN `permits` permit ON `projPerm`.`permit_id` = `permit`.`id`
    WHERE (
        (categoryId IS NOT NULL AND
            `permit`.`permit_category_id` = categoryId
        )
        OR (categoryId IS NULL)
    )
    AND (
        (keyword IS NOT NULL AND
            `proj`.`name` LIKE CONCAT('%',keyword,'%')
        )
        OR (keyword IS NULL)
    )
    AND (
        (cityId IS NOT NULL AND
            `proj`.`city_id` = cityId
        )
        OR (cityId IS NULL)
    )
    AND (
        (dateStart IS NOT NULL AND dateEnd IS NOT NULL AND
            `proj`.`created_at` BETWEEN dateStart AND dateEnd
        )
        OR (dateStart IS NULL OR dateEnd IS NULL)
    )
    AND (
        (permit_status IS NOT NULL AND
            `projPerm`.`status` = permit_status
        )
        OR (permit_status IS NULL)
    )
    GROUP BY `projPerm`.`project_id`
";

$projects_per_city = "
    SELECT
        `city`.`id`,
        `city`.`name`,
        COUNT(DISTINCT `projPerm`.`project_id`) projects,
        `city`.`created_at`
    FROM `projects` proj
    LEFT JOIN `project_permits` projPerm ON `proj`.`id` = `projPerm`.`project_id`
    RIGHT JOIN `cities` city ON `proj`.`city_id` = `city`.`id`
    LEFT JOIN `permits` permit ON `projPerm`.`permit_id` = `permit`.`id`
    LEFT JOIN `permit_categories` category ON `permit`.`permit_category_id` = `category`.`id`
    WHERE (
        (categoryId IS NOT NULL AND
            `category`.`id` = categoryId
        )
        OR (categoryId IS NULL)
    )
    AND (
        (dateStart IS NOT NULL AND dateEnd IS NOT NULL AND
            `proj`.`created_at` BETWEEN dateStart AND dateEnd
        )
        OR (dateStart IS NULL OR dateEnd IS NULL)
    )
    AND (
        (cityId IS NOT NULL AND
            `proj`.`city_id` = cityId
        )
        OR (cityId IS NULL)
    )
    GROUP BY `city`.`id`
";

$projects_per_status = "
    SELECT 
        `projPerm`.`status` status,
        COUNT(DISTINCT `projPerm`.`project_id`) projects,
        `proj`.`created_at`
    FROM `projects` proj
    JOIN `project_permits` projPerm ON `proj`.`id` = `projPerm`.`project_id`
    WHERE (
        (status IS NOT NULL AND
            `projPerm`.`status` = status
        )
        OR (status IS NULL)
    )
    AND (
        (dateStart IS NOT NULL AND dateEnd IS NOT NULL AND
            `proj`.`created_at` BETWEEN dateStart AND dateEnd
        )
        OR (dateStart IS NULL OR dateEnd IS NULL)
    )
    GROUP BY `projPerm`.`status`
";