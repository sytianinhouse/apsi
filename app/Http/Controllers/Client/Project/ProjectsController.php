<?php

namespace App\Http\Controllers\Client\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use PermitExpress\Project\Project;
use PermitExpress\Project\ProjectRepository;
use PermitExpress\Validation\ValidationException;

class ProjectsController extends Controller
{
    //
    public function __construct(
        ProjectRepository $projectRepo
    )
    {
        $this->projectRepo = $projectRepo;
    }

    public function index( Request $request )
    {
        $request['client_id'] = Auth::user()->client->id;
        $request['show_to_client'] = true;
        $projects = $this->projectRepo->displayProjects($request);

        return view('client.project.index', compact('projects'));
    }

    public function viewProject(Project $project)
    {
        if($project->client_id != Auth::user()->client->id)
        {
            return redirect()->route('client.projects.index')
                ->withErrors(__('auth.account.unauthorized'));
        }
        return view('client.project.view', compact('project'));
    }

    public function dashboard(Request $request)
    {
        return redirect()->route('client.projects.index');
    }
}
