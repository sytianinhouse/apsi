<?php

namespace App\Http\Controllers\Client\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\Client\Client;
use PermitExpress\Client\ClientRepository;
use PermitExpress\User\User;
use PermitExpress\User\UserRepository;
use PermitExpress\Validation\ValidationException;

class ProfileController extends Controller
{
    //
    public function __construct(
        ClientRepository $clientRepo
    )
    {
        $this->clientRepo = $clientRepo;
    }

    public function edit()
    {
        return view('client.user.profile');
    }

    public function update(Request $request, Client $client)
    {
        try
        {
            $client = $this->clientRepo->updateProfile($request, $client);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'Client']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }
}
