<?php

namespace App\Http\Controllers\MasterClient\Dashboard;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\ClientMasterProject\ClientMasterProjectRepository;
use PermitExpress\ProjectPermit\ProjectPermitRepository;
use PermitExpress\Permit\PermitRepository;

class DashboardController extends Controller
{
    //
    public function __construct(
        ClientMasterProjectRepository $clientMasterRepo,
        ProjectPermitRepository $projPermitRepo,
        PermitRepository $permitRepo
    )
    {
        $this->clientMasterRepo = $clientMasterRepo;
        $this->projPermitRepo = $projPermitRepo;
        $this->permitRepo = $permitRepo;
    }

    public function index(Request $request)
    {
        $clients = $this->clientMasterRepo->getByMaster(Auth::user()->client->id);
        $permits = $this->permitRepo->getAll();
        $newClients = $this->buildClients($clients, $request);
        $dateTypes = $this->dateTypes();


        return view('master_client.user.index', compact('newClients', 'permits', 'dateTypes'));
    }

    public function dashboard(Request $request)
    {
        $projects = $this->clientMasterRepo->getMastersProject(Auth::user()->client->id);
        $beforeExpirations = $this->projPermitRepo->getExpiredByMaster($projects);

        return view('master_client.dashboard.index', compact('beforeExpirations'));
    }

    public function dashRedirect(Request $request)
    {
        return redirect()->route('master_client.dashboard');
    }

    private function buildClients( $clients, $request )
    {
        $tmpData = array();
        $keyword = $request->search_key;
        $permitFilter = $request->permit;
        $dateType = $request->date_type;
        if($request->date_range)
        {
            $dateRange = explode(' - ',$request->date_range);
            $newDate = [0 => Carbon::parse($dateRange[0])->copy()->toDateString(),
                        1 => Carbon::parse($dateRange[1])->copy()->toDateString()];
        }

        foreach($clients as $cKey => $client)
        {
            $tmpClient = $client->client->where('id', $client->client_id);

            $tmpClient = $tmpClient->where(function($q) use($keyword) {
                $q->where('name', 'LIKE', "%{$keyword}%");
            });

            $tmpClient = $tmpClient->get();

            if( $tmpClient->count() > 0 )
            {
                $tmpData['client'][$client->client->id]['name'] = $client->client->name;
                $tmpData['client'][$client->client->id]['display'] = true;

                if( $client->project )
                {
                    $tmpProject = $client->project->where('id', $client->project_id);

                    // $tmpProject = $tmpProject->where(function($q) use($keyword) {
                    //     $q->where('name', 'LIKE', "%{$keyword}%");
                    // });

                    $tmpProject = $tmpProject->get();

                    if( $tmpProject->count() > 0 )
                    {
                        $tmpData['client'][$client->client->id]['project'][$cKey]['name'] = $client->project->name;

                        if( $client->project->projectPermits )
                        {
                            $tmpProjPermit = $client->project->projectPermits()->where('project_id', $client->project_id);

                            if($permitFilter)
                            {
                                $tmpProjPermit = $tmpProjPermit->where('permit_id', $permitFilter);
                            }

                            if($request->date_range && $dateType)
                            {
                                $tmpProjPermit = $tmpProjPermit->whereBetween($dateType, $newDate);
                                // $tmpProjPermit = $tmpProjPermit->whereHas('projectPrerequisitePermits', function($q) use($dateType, $newDate) {
                                //                                 $q->whereBetween($dateType, $newDate);
                                //                             });
                            }

                            $tmpProjPermit = $tmpProjPermit->get();

                            if( $tmpProjPermit->count() > 0 )
                            {
                                foreach( $tmpProjPermit as $pKey => $projPermit )
                                {
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['name'] = $projPermit->name;
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['branch_name'] = $projPermit->branch_name;
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['date_due'] = $projPermit->date_due;
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['date_filed'] = $projPermit->date_filed;
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['date_acquired'] = $projPermit->date_acquired;
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['date_expiry'] = $projPermit->date_expiry;
                                    $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['notes'] = $projPermit->notes;

                                    $tmpPreReq = $projPermit->projectPrerequisitePermits();

                                    if($request->date_range && $dateType)
                                    {
                                        $tmpPreReq = $tmpPreReq->whereBetween($dateType, $newDate);
                                    }

                                    $tmpPreReq = $tmpPreReq->get();

                                    if($tmpPreReq->count() > 0)
                                    {
                                        foreach($tmpPreReq as $prKey => $preReqPermit)
                                        {
                                                $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['prereq'][$prKey]['name'] = $preReqPermit->name;
                                                $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['prereq'][$prKey]['date_due'] = $preReqPermit->date_due;
                                                $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['prereq'][$prKey]['date_filed'] = $preReqPermit->date_filed;
                                                $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['prereq'][$prKey]['date_acquired'] = $preReqPermit->date_acquired;
                                                $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['prereq'][$prKey]['date_expiry'] = $preReqPermit->date_expiry;
                                                $tmpData['client'][$client->client->id]['project'][$cKey]['permit'][$pKey]['prereq'][$prKey]['notes'] = $preReqPermit->notes;
                                        }
                                    }
                                    else
                                    {
                                        //$tmpData['client'][$client->client->id]['display'] = false;
                                    }
                                }
                            }
                            else
                            {
                                $tmpData['client'][$client->client->id]['display'] = false;
                            }
                        }
                    }
                    else
                    {
                        //$tmpData['client'][$client->client->id]['display'] = false;
                    }
                }
                else
                {
                    $tmpData['client'][$client->client->id]['project'] = array();
                }
            }
        }

        return $tmpData;
    }

    public function getMasterData($request)
    {
        // $query = new ProjectPrerequisitePermit();

        // $clientsIds = [1,2];
        // $projectsIds = [1,2];

        // $query->where('created_at', $request->from_date);

        // $query->whereHas('projectPermit', function($q) use($request) {
        //     $q->whereHas('project'm function($q) use($request){
        //         $q->where('name', $request->name)
        //           ->whereHas('client', function($q) use($request) {
        //             $q->whereIn('id', $clientsIds)->where('name', $request->name);
        //           })
        //     });
        // })->with('projectPermit.project', 'projectPermit.project.client');
    }

    public function dateTypes()
    {
        return [
                'date_due' => 'Due Date',
                'date_filed' => 'Date Filed',
                'date_acquired' => 'Date Acquired',
                'date_expiry' => 'Expiry Date'
            ];
    }
}
