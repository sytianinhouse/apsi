<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use PermitExpress\Validation\ValidationException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    const PER_PAGE = 50;
    const DISPLAY_ALL = 'all';

    public function redirectFormRequest(ValidationException $e, $ajax = false)
    {
    if ($ajax) {
      return response()->json($e->getErrors()); //422
    }

    return redirect()->back()
      ->withInput()
      ->withErrors($e->getErrors())
      ->withError("Please check required fields.");
    }
}
