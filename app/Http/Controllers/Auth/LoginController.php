<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use PermitExpress\User\User;
use PermitExpress\User\UserRepository;
use PermitExpress\Setting\SiteSetting;
use PermitExpress\Setting\SettingRepository;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $username = 'username';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo, SettingRepository $settingRepo)
    {
        $this->userRepo = $userRepo;
        $this->settingRepo = $settingRepo;
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, User $user)
    {
        $isActive = $this->checkIfActive($user);

        if(!$isActive)
        {
            Auth::logout();
            return redirect()->back()
                        ->withErrors(__('auth.account.inactive'));
        }

        $user->last_login = new \DateTime;
        $user->save();

        if( $user->isSuperAdmin() )
        {
            return redirect()->route('admin.dashboard');
        }
        elseif( $user->isClient() )
        {
            if($this->settingRepo->isMaintenance())
            {
                Auth::logout();
                return redirect()->back()
                            ->withErrors(__('auth.account.undermaintenance'));
            }
            return redirect()->route('client.projects.index');
        }
        elseif( $user->isMasterClient() )
        {
            if($this->settingRepo->isMaintenance())
            {
                Auth::logout();
                return redirect()->back()
                            ->withErrors(__('auth.account.undermaintenance'));
            }
            return redirect()->route('master_client.dashboard');
        }

        return 'access denied!!!';
    }

    public function manageRedirect(Request $request)
    {
        $user = $request->user();

        if( $user )
        {
            if( $user->isSuperAdmin() )
            {
                return redirect()->route('admin.dashboard');
            }
            elseif( $user->isClient() )
            {
                return redirect()->route('client.projects.index');
            }
            elseif( $user->isMasterClient() )
            {
                return redirect()->route('master_client.dashboard');
            }
        }

        return redirect('login');
    }

    public function username()
    {
        return 'username';
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required',
            'password' => 'required',
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    private function checkIfActive(User $user)
    {
        return ($user->active) ? true : false;
    }
}
