<?php

namespace App\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PermitExpress\Project\ProjectRepository;
use PermitExpress\ProjectPermit\ProjectPermitRepository;

class DashboardController extends Controller
{
    //
    public function __construct(
        ProjectRepository $projRepo,
        ProjectPermitRepository $projPermitRepo
    )
    {
        $this->projRepo = $projRepo;
        $this->projPermitRepo = $projPermitRepo;
    }


    public function index(Request $request)
    {
        $request['limit'] = 10;

        $projects = $this->projRepo->displayProjects($request);
        $beforeExpirations = $this->projPermitRepo->beforeExpiration();
        $temporaryStatus = $this->projPermitRepo->temporaryStatus();
        $beforeDueDate = $this->projPermitRepo->beforeDueDate();

        return view('admin.dashboard.index', compact('projects', 'beforeExpirations', 'temporaryStatus', 'beforeDueDate'));
    }

    public function dashboard(Request $request)
    {
        return redirect()->route('admin.dashboard');
    }

}
