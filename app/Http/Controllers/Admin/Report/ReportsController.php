<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PermitExpress\City\City;
use PermitExpress\City\CityRepository;
use PermitExpress\PermitCategory\PermitCategoryRepository;
use PermitExpress\StoredProcedure\Report\ReportProcedure;
use PermitExpress\Validation\ValidationException;

class ReportsController extends Controller
{
    //
    public function __construct(
        ReportProcedure $reportProcedure,
        CityRepository $cityRepo,
        PermitCategoryRepository $categoryRepo
    )
    {
        $this->reportProcedure = $reportProcedure;
        $this->cityRepo = $cityRepo;
        $this->categoryRepo = $categoryRepo;
    }

    public function projectsPerCategory(Request $request)
    {
        $cities = $this->cityRepo->getAll();
        $permitCategories = $this->reportProcedure->projectByCategory($request);

        return view('admin.report.projects_per_category.index', compact('permitCategories', 'cities'));
    }

    public function viewProjectsCreated(Request $request)
    {
        $cities = $this->cityRepo->getAll();
        $categories = $this->categoryRepo->getAll();
        $projects = $this->reportProcedure->projectsCreated($request);

        return view('admin.report.projects_per_category.projects', compact('projects', 'cities', 'categories'));
    }

    public function projectsPerCity(Request $request)
    {
        $cities = $this->cityRepo->getAll();
        $categories = $this->categoryRepo->getAll();
        $data = $this->reportProcedure->projectByCity($request);

        return view('admin.report.projects_per_city.index', compact('data', 'cities', 'categories'));
    }

    public function projectsPerStatus(Request $request)
    {
        $cities = $this->cityRepo->getAll();
        $categories = $this->categoryRepo->getAll();
        $projectsPerStatus = $this->reportProcedure->projectByStatus($request);

        return view('admin.report.projects_per_status.index', compact('projectsPerStatus'));
    }
}
