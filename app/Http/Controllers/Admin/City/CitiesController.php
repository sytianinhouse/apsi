<?php

namespace App\Http\Controllers\Admin\City;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\City\City;
use PermitExpress\City\CityRepository;
use PermitExpress\Validation\ValidationException;

class CitiesController extends Controller
{
    //
    public function __construct(
        CityRepository $cityRepo
    )
    {
        $this->cityRepo = $cityRepo;
    }

    public function index(Request $request)
    {
        $request['sort_by_name'] = true;
        $cities = $this->cityRepo->displayCities($request);

        return view('admin.city.index', compact('cities'));
    }

    public function create()
    {
        return view('admin.city.create');
    }

    public function edit(City $city)
    {
        return view('admin.city.edit', compact('city'));
    }

    public function store(Request $request)
    {
        try
        {
            $city = $this->cityRepo->createCity($request);

            return redirect()->route('admin.cities.index')
                ->withSuccess(__('validation.request.create', ['prefix' => 'City']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, City $city)
    {
        try
        {
            $city = $this->cityRepo->updateCity($request, $city);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'City']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function destroy(City $city)
    {
        try
        {
            $city = $this->cityRepo->deleteCity($city);

            return redirect()->back()
                ->withSuccess(__('validation.request.deleted', ['prefix' => 'City']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

}
