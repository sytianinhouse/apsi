<?php

namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\User\User;
use PermitExpress\User\UserRepository;
use PermitExpress\Validation\ValidationException;

class UsersController extends Controller
{
    //
    public function __construct(
        UserRepository $userRepo
    )
    {
        $this->userRepo = $userRepo;
    }

    public function index(Request $request)
    {
        $request['except_auth'] = true;
        $users = $this->userRepo->displayUsers($request);

        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function edit(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    public function store(Request $request)
    {
        try
        {
            $user = $this->userRepo->createUser($request);

            return redirect()->route('admin.users.index')
                ->withSuccess(__('validation.request.create', ['prefix' => 'User']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, User $user)
    {
        try
        {
            $user = $this->userRepo->updateUser($request, $user);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'User']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function destroy(User $user)
    {
        try
        {
            $user = $this->userRepo->deleteUser($user);

            return redirect()->back()
                ->withSuccess(__('validation.request.deleted', ['prefix' => 'User']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }
}
