<?php

namespace App\Http\Controllers\Admin\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\Client\Client;
use PermitExpress\Client\ClientRepository;
use PermitExpress\ClientMasterProject\ClientMasterProject;
use PermitExpress\ClientMasterProject\ClientMasterProjectRepository;
use PermitExpress\User\User;
use PermitExpress\User\UserRepository;
use PermitExpress\Project\Project;
use PermitExpress\Project\ProjectRepository;
use PermitExpress\Validation\ValidationException;

class ClientsController extends Controller
{
    //
    public function __construct(
        ClientRepository $clientRepo,
        ProjectRepository $projectRepo,
        ClientMasterProjectRepository $masterProjRepo
    )
    {
        $this->clientRepo = $clientRepo;
        $this->projectRepo = $projectRepo;
        $this->masterProjRepo = $masterProjRepo;
    }

    public function index(Request $request)
    {
        //dd($request->search_key);
        $clients = $this->clientRepo->displayClients($request);

        return view('admin.client.index', compact('clients'));
    }

    public function create()
    {
        $clients = $this->clientRepo->getAllExceptMaster();

        return view('admin.client.create', compact('clients'));
    }

    public function edit(Client $client)
    {
        $clients = $this->clientRepo->getAllExceptAndNoMaster($client->id);
        $tmpClients = $this->getTempClients($client, $clients);
        $tmpProjects = $this->getTempProjects($client);

        return view('admin.client.edit', compact('client', 'clients', 'tmpClients', 'tmpProjects'));
    }

    public function store(Request $request)
    {
        try
        {
            $client = $this->clientRepo->createClient($request);

            return redirect()->route('admin.clients.index')
                ->withSuccess(__('validation.clients.create', ['prefix' => 'Client']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, Client $client)
    {
        try
        {
            $client = $this->clientRepo->updateClient($request, $client);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'Client']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function destroy(Client $client)
    {
        try
        {

            $client = $this->clientRepo->deleteClient($client);

            return redirect()->back()
                ->withSuccess(__('validation.request.deleted', ['prefix' => 'Client']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function clientAjax(Request $request)
    {
        //$projects = $this->projectRepo->getByClient($client_ids);
        $projects = Project::where('client_id', $request->client_ids)->get();
        $clients = $this->clientRepo->getByID($request->client_ids);

        $view = view('templates.client.client_projects', compact('projects','clients'))->render();

        return response()->json(['html'=>$view]);
    }

    public function getTempClients($client, $selectClients)
    {
        $tmpClients = array();

        if( $client->exists )
        {
            foreach( $client->clientMasterProjects as $clientMaster )
            {
                $tmpClients[$clientMaster->client_id]['id'] = $clientMaster->client_id;
                $tmpClients[$clientMaster->client_id]['name'] = $clientMaster->client->name;
                $tmpClients[$clientMaster->client_id]['selected'] = true;
            }

            foreach ( $selectClients as $pkey => $selectClient )
            {
                if( !array_key_exists($selectClient->id, $tmpClients) )
                {
                    $tmpClients[$selectClient->id]['id'] = $selectClient->id;
                    $tmpClients[$selectClient->id]['name'] = $selectClient->name;
                    $tmpClients[$selectClient->id]['selected'] = false;
                }
            }
        }

        return $tmpClients;
    }

    public function getTempProjects($client)
    {
        $tmpArr = array();

        if( $client->exists )
        {
            $selectedClientID = array();
            foreach( $client->clientMasterProjects as $clientMaster )
            {
                array_push($selectedClientID, $clientMaster->client_id);
            }

            $clients = $this->clientRepo->getByID($selectedClientID);

            foreach( $clients as $key => $dataClient )
            {
                $projects = $this->projectRepo->getByClient($dataClient->id);

                if( $projects->count() > 0 )
                {
                    $tmpArr[$key]['client']['id'] = $dataClient->id;
                    $tmpArr[$key]['client']['name'] = $dataClient->name;

                    foreach( $projects as $key2 => $project )
                    {
                        $tmpArr[$key]['projects'][$key2]['id'] = $project->id;
                        $tmpArr[$key]['projects'][$key2]['name'] = $project->name;
                        $queriedProj = $this->masterProjRepo->getByMasterProject($project->id, $client->id);

                        if( count($queriedProj) > 0 )
                        {
                            $tmpArr[$key]['projects'][$key2]['selected'] = true;
                        }
                    }
                }
            }
        }

        return $tmpArr;
    }
}
