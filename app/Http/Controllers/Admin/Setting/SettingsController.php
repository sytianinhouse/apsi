<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\Setting\SiteSetting;
use PermitExpress\Setting\SettingRepository;
use PermitExpress\Validation\ValidationException;

class SettingsController extends Controller
{
    //
    public function __construct(
        SettingRepository $settingRepo
    )
    {
        $this->settingRepo = $settingRepo;
    }

    public function index(Request $request)
    {
        $siteSetting = $this->settingRepo->getSettings();

        return view('admin.setting.settings', compact('siteSetting'));
    }

    public function store(Request $request)
    {
        try
        {
            $siteSetting = new SiteSetting;
            $setting = $this->settingRepo->saveSetting($request, $siteSetting);

            return redirect()->back()
                ->withSuccess(__('validation.request.create', ['prefix' => 'Settings']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, SiteSetting $siteSetting)
    {
        try
        {
            $setting = $this->settingRepo->saveSetting($request, $siteSetting);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'Settings']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }
}
