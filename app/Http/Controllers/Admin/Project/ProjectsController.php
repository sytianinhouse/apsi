<?php

namespace App\Http\Controllers\Admin\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\City\City;
use PermitExpress\City\CityRepository;
use PermitExpress\Client\Client;
use PermitExpress\Client\ClientRepository;
use PermitExpress\Permit\Permit;
use PermitExpress\Permit\PermitRepository;
use PermitExpress\Project\Project;
use PermitExpress\Project\ProjectRepository;
use PermitExpress\Uploadable\Uploadable;
use PermitExpress\Uploadable\UploadableRepository;
use PermitExpress\Validation\ValidationException;

class ProjectsController extends Controller
{
    //
    public function __construct(
        ProjectRepository $projectRepo,
        ClientRepository $clientRepo,
        PermitRepository $permitRepo,
        CityRepository $cityoRepo,
        UploadableRepository $uploadRepo
    )
    {
        $this->projectRepo = $projectRepo;
        $this->clientRepo = $clientRepo;
        $this->permitRepo = $permitRepo;
        $this->cityoRepo = $cityoRepo;
        $this->uploadRepo = $uploadRepo;
    }

    public function index(Request $request)
    {
        $projects = $this->projectRepo->displayProjects($request);

        return view('admin.project.index', compact('projects'));
    }

    public function viewByClient(Request $request, Client $client)
    {

        $request['client_id'] = $client->id;
        $projects = $this->projectRepo->displayProjects($request);

        return view('admin.project.view_per_client', compact('projects', 'client'));
    }

    public function create()
    {
        $clients = $this->clientRepo->getAllExceptMaster();
        $permits = $this->permitRepo->getAll();
        $cities = $this->cityoRepo->getAll();
        $masters = $this->clientRepo->getMasters();

        return view('admin.project.create', compact('clients', 'permits', 'cities', 'masters'));
    }

    public function createByClient(Client $client)
    {
        $clients = $this->clientRepo->getAllExceptMaster();
        $masters = $this->clientRepo->getMasters();
        $permits = $this->permitRepo->getAll();
        $cities = $this->cityoRepo->getAll();
        $client_id = $client->id;

        return view('admin.project.create', compact('clients', 'permits', 'cities', 'client_id', 'masters'));
    }

    public function edit(Project $project)
    {
        $clients = $this->clientRepo->getAllExceptMaster();
        $masters = $this->clientRepo->getMasters();
        $permits = $this->permitRepo->getAll();
        $cities = $this->cityoRepo->getAll();
        $tmpPermits = $this->getTempPermit($project, $permits);
        $tmpMasters = $this->getTmpMasters($masters, $project);

        return view('admin.project.edit', compact('project', 'clients', 'permits', 'cities', 'tmpPermits', 'masters', 'tmpMasters'));
    }

    public function store(Request $request)
    {
        try
        {
            $projects = $this->projectRepo->createProject($request);

            return redirect()->route('admin.projects.index')
                ->withSuccess(__('validation.request.create', ['prefix' => 'Project']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function storeByClient(Client $client, Request $request)
    {
        try
        {
            $projects = $this->projectRepo->createProject($request);

            return redirect()->route('admin.projects.byClient', $client)
                ->withSuccess(__('validation.request.create', ['prefix' => 'Project']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, Project $project)
    {
        try
        {
            $project = $this->projectRepo->updateProject($request, $project);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'Project']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function destroy(Project $project)
    {
        try
        {
            $hasPermits = $this->projectRepo->hasPermits($project);
            if($hasPermits == 0)
            {
                $project = $this->projectRepo->deleteProject($project);
            }
            else
            {
                return redirect()->back()
                    ->withErrors(__('validation.categories.failed', ['prefix' => 'Project']));
            }

            return redirect()->back()
                ->withSuccess(__('validation.request.deleted', ['prefix' => 'Project']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function permitAjax(Request $request, Project $project)
    {
        $permits = $this->permitRepo->getById($request->permit_ids);
        $cities = $this->cityoRepo->getAll();
        $permitCount = $request->permitCount;

        $view = view('templates.project.permit', compact('permits', 'cities', 'permitCount'))->render();

        return response()->json(['html'=>$view]);
    }

    public function ajaxSendNotification(Request $request)
    {

        $sent = $this->projectRepo->makePermitEmail($request);

        return response()->json(['data'=>$sent]);
    }

    public function ajaxDeleteFile(Request $request)
    {
        $uploadable = Uploadable::find($request->id);

        $this->uploadRepo->deleteThisUploadable($uploadable);
        $uploadable->delete();

        $arr = ['deleted'=>true];
        return json_encode($arr);
    }

    public function getTempPermit($project, $permits)
    {
        $tmpPermits = array();

        if( $project->exists )
        {
            foreach( $project->projectPermits as $projPerm )
            {
                $tmpPermits[$projPerm->permit_id]['id'] = $projPerm->permit_id;
                $tmpPermits[$projPerm->permit_id]['name'] = $projPerm->name;
                $tmpPermits[$projPerm->permit_id]['selected'] = true;
            }

            foreach ( $permits as $pkey => $permit )
            {
                if( !array_key_exists($permit->id, $tmpPermits) )
                {
                    $tmpPermits[$permit->id]['id'] = $permit->id;
                    $tmpPermits[$permit->id]['name'] = $permit->name;
                    $tmpPermits[$permit->id]['selected'] = false;
                }
            }
        }

        return $tmpPermits;
    }

    public function getTmpMasters($masters, $project)
    {
        $tmpMasters = array();

        if( $project->exists )
        {
            foreach( $project->clientMasterProjects as $projectMaster )
            {
                $tmpMasters[$projectMaster->master_id]['id'] = $projectMaster->master_id;
                $tmpMasters[$projectMaster->master_id]['name'] = $this->clientRepo->getByID($projectMaster->master_id)->name;
                $tmpMasters[$projectMaster->master_id]['selected'] = true;
            }

            foreach ( $masters as $pkey => $master )
            {
                if( !array_key_exists($master->id, $tmpMasters) )
                {
                    $tmpMasters[$master->id]['id'] = $master->id;
                    $tmpMasters[$master->id]['name'] = $master->name;
                    $tmpMasters[$master->id]['selected'] = false;
                }
            }

        }

        return $tmpMasters;
    }
}
