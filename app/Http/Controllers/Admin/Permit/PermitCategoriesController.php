<?php

namespace App\Http\Controllers\Admin\Permit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\PermitCategory\PermitCategory;
use PermitExpress\PermitCategory\PermitCategoryRepository;
use PermitExpress\Permit\Permit;
use PermitExpress\Permit\PermitRepository;
use PermitExpress\Validation\ValidationException;

class PermitCategoriesController extends Controller
{
    //
    public function __construct(
        PermitCategoryRepository $permitCategoryRepo,
        PermitRepository $permitRepo
    )
    {
        $this->permitCategoryRepo = $permitCategoryRepo;
        $this->permitRepo = $permitRepo;
    }

    public function index(Request $request)
    {
        $permitCategories = $this->permitCategoryRepo->displayPermitCategories($request);

        return view('admin.permit_category.index', compact('permitCategories'));
    }

    public function create()
    {
        return view('admin.permit_category.create');
    }

    public function edit(PermitCategory $permitCategory)
    {
        return view('admin.permit_category.edit', compact('permitCategory'));
    }

    public function store(Request $request)
    {
        try
        {
            $permitCategory = $this->permitCategoryRepo->createPermitCategory($request);

            return redirect()->route('admin.permit_categories.index')
                ->withSuccess(__('validation.request.create', ['prefix' => 'Permit Category']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, PermitCategory $permitCategory)
    {
        try
        {
            $permitCategory = $this->permitCategoryRepo->updatePermitCategory($request, $permitCategory);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'Permit Category']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function destroy(PermitCategory $permitCategory)
    {
        try
        {
            if( $this->permitRepo->hasCategory($permitCategory) == 0 )
            {
                $permitCategory = $this->permitCategoryRepo->deletePermitCategory($permitCategory);

                return redirect()->back()
                    ->withSuccess(__('validation.request.deleted', ['prefix' => 'Permit Category']));
            }
            else
            {
                return redirect()->back()
                    ->withErrors(__('validation.categories.failed', ['prefix' => 'category']));
            }

        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function quickCreate(Request $request)
    {
        try
        {
            $permitCategory = $this->permitCategoryRepo->createPermitCategory($request);

            return redirect()->back()
                ->withSuccess(__('validation.request.create', ['prefix' => 'Permit Category']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }
}
