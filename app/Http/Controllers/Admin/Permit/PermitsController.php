<?php

namespace App\Http\Controllers\Admin\Permit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use PermitExpress\Permit\Permit;
use PermitExpress\Permit\PermitRepository;
use PermitExpress\PermitCategory\PermitCategory;
use PermitExpress\PermitCategory\PermitCategoryRepository;
use PermitExpress\Validation\ValidationException;

class PermitsController extends Controller
{
    //
    public function __construct(
        PermitRepository $permitRepo,
        PermitCategoryRepository $permitCategoryRepo
    )
    {
        $this->permitRepo = $permitRepo;
        $this->permitCategoryRepo = $permitCategoryRepo;
    }

    public function index(Request $request)
    {
        $permits = $this->permitRepo->displayPermits($request);

        return view('admin.permit.index', compact('permits'));
    }

    public function create()
    {
        $permitCategories = $this->permitCategoryRepo->getAll();

        return view('admin.permit.create', compact('permitCategories'));
    }

    public function edit(Permit $permit)
    {
        $permitCategories = $this->permitCategoryRepo->getAll();
        return view('admin.permit.edit', compact('permit', 'permitCategories'));
    }

    public function store(Request $request)
    {
        try
        {
            $permits = $this->permitRepo->createPermit($request);

            return redirect()->route('admin.permits.index')
                ->withSuccess(__('validation.request.create', ['prefix' => 'Permit']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function update(Request $request, Permit $permit)
    {
        try
        {
            $permit = $this->permitRepo->updatePermit($request, $permit);

            return redirect()->back()
                ->withSuccess(__('validation.request.update', ['prefix' => 'Permit']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }

    public function destroy(Permit $permit)
    {
        try
        {
            $hasCategory = $this->permitRepo->checkCategories($permit);

            if($hasCategory > 0)
            {
                return redirect()->back()
                    ->withErrors(__('validation.categories.failed', ['prefix' => 'Permit']));
            }
            else
            {
                $permit = $this->permitRepo->deletePermit($permit);
            }

            return redirect()->back()
                ->withSuccess(__('validation.request.deleted', ['prefix' => 'Permit']));
        }
        catch (ValidationException $e)
        {
            return $this->redirectFormRequest($e);
        }
    }
}
