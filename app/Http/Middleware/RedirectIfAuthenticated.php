<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            //return redirect('/home');
            $user = Auth::guard($guard)->user();
            if( $user->isSuperAdmin() )
            {
                return redirect()->route('admin.dashboard');
            }
            elseif( $user->isClient() )
            {
                return redirect()->route('client.projects.index');
            }
            elseif( $user->isMasterClient() )
            {
                return redirect()->route('master_client.dashboard');
            }
        }

        return $next($request);
    }
}
