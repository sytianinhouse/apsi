<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PermitExpress\Permit\Permit;
use PermitExpress\City\City;

class ProjectPermitNotif extends Mailable
{
    use Queueable, SerializesModels;

    public $projectPermit;
    public $client;
    public $statuses;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($projectPermit, $client)
    {
        $this->projectPermit = $projectPermit;
        $this->client = $client;
        $this->statuses = Permit::$statuses;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message = $this->subject('Project Permit');
        $message->markdown('emails.project-permit');

        if($this->projectPermit->uploads)
        {
            foreach($this->projectPermit->uploads as $pUpload)
            {
                if($pUpload->key == 'permit-attachment')
                {
                    $message->attach($pUpload->path);
                }
            }
        }

        // if($this->projectPermit->file_path)
        // {
        //     $message->attach($this->projectPermit->file_path);
        // }

        foreach( $this->projectPermit->projectPrerequisitePermits as $preReqPermit )
        {
            foreach($preReqPermit->uploads as $pUpload2)
            {
                if($pUpload2->key == 'prereq-attachment')
                {
                    $message->attach($pUpload2->path);
                }
            }
            // if($preReqPermit->file_path)
            // {
            //     $message->attach($preReqPermit->file_path);
            // }
        }
        return $message;
    }
}
